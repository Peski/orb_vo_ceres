/**
* This file is part of ORB-SLAM.
*
* Copyright (C) 2014 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <http://webdiis.unizar.es/~raulmur/orbslam/>
*
* ORB-SLAM is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OPTIMIZER_H
#define OPTIMIZER_H

#include "Map.h"
#include "MapPoint.h"
#include "KeyFrame.h"
//#include "LoopClosing.h"
#include "Frame.h"
#include "SE3Quat.h"
#include "INS.h"

//#include "Thirdparty/g2o/g2o/types/types_seven_dof_expmap.h"

namespace ORB_SLAM
{

class LoopClosing;

class Optimizer
{
public:
    void static GlobalBundleAdjustemnt(Map* pMap, int nIterations=20, bool *pbStopFlag=NULL);
    void static BundleAdjustment(const std::vector<KeyFrame*> &vpKF, const std::vector<MapPoint*> &vpMP, int nIterations = 5, bool *pbStopFlag=NULL);
    int static PoseOptimization(Frame* pFrame);
    bool static LocalBundleAdjustment(KeyFrame* pKF, const std::vector<INS::InertialFactor>& vImuData, bool *pbStopFlag=NULL, long unsigned int* nLocalIdStart=nullptr);
    bool static LocalBundleAdjustmentVisual(KeyFrame* pKF, bool *pbStopFlag=NULL, long unsigned int* nLocalIdStart=nullptr);
    //bool static LocalBundleAdjustmentInertial(KeyFrame *pKF, const std::vector<INS::InertialFactor>& vImuData, bool *pbStopFlag=nullptr, long unsigned int *nLocalIdStart=nullptr);

    //void static BundleAdjustment(const std::vector<KeyFrame*> &vpKF, const std::map<KeyFrame*, pose_t> &mpKFPose, const std::vector<MapPoint*> &vpMP, int nIterations = 5, bool *pbStopFlag=NULL);
    //void static LocalBundleAdjustment(KeyFrame* pKF, const std::map<KeyFrame*, SE3Quat> &mpKFPose, bool *pbStopFlag=NULL);
    //void static Refinement(KeyFrame* pKF, const std::map<KeyFrame*, SE3Quat> &mpKFPose, bool *pbStopFlag=NULL);

/*
    void static OptimizeEssentialGraph(Map* pMap, KeyFrame* pLoopKF, KeyFrame* pCurKF, g2o::Sim3 &Scurw,
                                       LoopClosing::KeyFrameAndPose &NonCorrectedSim3,
                                       LoopClosing::KeyFrameAndPose &CorrectedSim3,
                                       std::map<KeyFrame*, set<KeyFrame*> > &LoopConnections);


    static int OptimizeSim3(KeyFrame* pKF1, KeyFrame* pKF2, std::vector<MapPoint *> &vpMatches1, g2o::Sim3 &g2oS12, float th2 = 10);
*/
};

} //namespace ORB_SLAM

#endif // OPTIMIZER_H
