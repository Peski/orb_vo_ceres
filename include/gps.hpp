
#ifndef GPS_HPP_
#define GPS_HPP_

#include <cmath>
#include <cstdint>
#include <fstream>
#include <istream>
#include <string>
#include <utility>
#include <vector>

#include <Eigen/Core>

namespace gps {

inline Eigen::Vector3d geodetic2Ecef(const double latitude, const double longitude, const double altitude) {
  const double sLat = std::sin(latitude * 0.01745329252);
  const double sLon = std::sin(longitude * 0.01745329252);
  const double cLat = std::cos(latitude * 0.01745329252);
  const double cLon = std::cos(longitude * 0.01745329252);
  
  // Convert geodetic coordinates to ECEF (WGS84)
  double xi = std::sqrt(1.0 - (6.69437999014 * 0.001) * sLat * sLat);
  
  Eigen::Vector3d ecef;
  ecef.x() = (6378137.0 / xi + altitude) * cLat * cLon;
  ecef.y() = (6378137.0 / xi + altitude) * cLat * sLon;
  ecef.z() = (6378137.0 / xi * (1.0 - (6.69437999014 * 0.001)) + altitude) * sLat;
  return ecef;
}

inline Eigen::Matrix3d nRe(const double latitude, const double longitude) {
  const double sLat = std::sin(latitude * 0.01745329252);
  const double sLon = std::sin(longitude * 0.01745329252);
  const double cLat = std::cos(latitude * 0.01745329252);
  const double cLon = std::cos(longitude * 0.01745329252);

  Eigen::Matrix3d R;
  R(0, 0) = -sLat * cLon;
  R(0, 1) = -sLat * sLon;
  R(0, 2) = cLat;
  R(1, 0) = -sLon;
  R(1, 1) = cLon;
  R(1, 2) = 0.0;
  R(2, 0) = -cLat * cLon;
  R(2, 1) = -cLat * sLon;
  R(2, 2) = -sLat;

  return R;
}

/*
void RelativeNEDCoordinates(const double latitude_0, const double longitude_0, const double altitude_0,
                            const double latitude, const double longitude, const double altitude,
                            double* x, double* y, double* z) {
    const double sLat = std::sin((latitude_0 / 180.0) * EIGEN_PI);
    const double sLon = std::sin((longitude_0 / 180.0) * EIGEN_PI);
    const double cLat = std::cos((latitude_0 / 180.0) * EIGEN_PI);
    const double cLon = std::cos((longitude_0 / 180.0) * EIGEN_PI);

    Eigen::Matrix3d nRe;
    nRe(0, 0) = -sLat * cLon;
    nRe(0, 1) = -sLat * sLon;
    nRe(0, 2) = cLat;
    nRe(1, 0) = -sLon;
    nRe(1, 1) = cLon;
    nRe(1, 2) = 0.0;
    nRe(2, 0) = -cLat * cLon;
    nRe(2, 1) = -cLat * sLon;
    nRe(2, 2) = -sLat;
    
    double ecef_x0, ecef_y0, ecef_z0;
    geodetic2Ecef(latitude_0, longitude_0, altitude_0, &ecef_x0, &ecef_y0, &ecef_z0);
    
    double ecef_x, ecef_y, ecef_z;
    geodetic2Ecef(latitude, longitude, altitude, &ecef_x, &ecef_y, &ecef_z);
    
    Eigen::Vector3d ned = nRe * Eigen::Vector3d(ecef_x - ecef_x0, ecef_y - ecef_y0, ecef_z - ecef_z0);
    
    *x = ned.x();
    *y = ned.y();
    *z = ned.z();
}
*/
} // namesapce gps

#endif // GPS_HPP_
