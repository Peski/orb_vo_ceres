#ifndef GEODETIC_CONVERTER_H_
#define GEODETIC_CONVERTER_H_

// https://github.com/ethz-asl/geodetic_utils

#include <Eigen/Core>

class GeodeticConverter
{
public:

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  GeodeticConverter();

  ~GeodeticConverter();

  // Default copy constructor and assignment operator are OK.

  bool isInitialised() const;

  void getReference(double* latitude, double* longitude, double* altitude) const;

  void initialiseReference(const double latitude, const double longitude, const double altitude);

  void geodetic2Ecef(const double latitude, const double longitude, const double altitude, double* x,
                     double* y, double* z) const;

  void ecef2Geodetic(const double x, const double y, const double z, double* latitude,
                     double* longitude, double* altitude) const;

  void ecef2Ned(const double x, const double y, const double z, double* north, double* east,
                double* down) const;

  void ned2Ecef(const double north, const double east, const double down, double* x, double* y,
                double* z) const;

  void geodetic2Ned(const double latitude, const double longitude, const double altitude,
                    double* north, double* east, double* down) const;

  void ned2Geodetic(const double north, const double east, const double down, double* latitude,
                    double* longitude, double* altitude) const;

  void geodetic2Enu(const double latitude, const double longitude, const double altitude,
                    double* east, double* north, double* up) const;

  void enu2Geodetic(const double east, const double north, const double up, double* latitude,
                    double* longitude, double* altitude) const;

private:

  double initial_latitude_;
  double initial_longitude_;
  double initial_altitude_;

  double initial_ecef_x_;
  double initial_ecef_y_;
  double initial_ecef_z_;

  Eigen::Matrix3d ecef_to_ned_matrix_;
  Eigen::Matrix3d ned_to_ecef_matrix_;

  bool haveReference_;
  
  static Eigen::Matrix3d nRe(const double lat_radians, const double lon_radians);
  
  static double rad2Deg(const double radians);
  
  static double deg2Rad(const double degrees);
  
  // Geodetic system parameters
  static const double kSemimajorAxis;
  static const double kSemiminorAxis;
  static const double kFirstEccentricitySquared;
  static const double kSecondEccentricitySquared;
  static const double kFlattening;

}; // class GeodeticConverter

#endif // GEODETIC_CONVERTER_H_
