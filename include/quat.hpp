
// STL
#include <cmath>

inline void fromYPR(const double ypr[3], double q[4])
{
  const double cy = std::cos(0.5*ypr[0]), sy = std::sin(0.5*ypr[0]);
	const double cp = std::cos(0.5*ypr[1]), sp = std::sin(0.5*ypr[1]);
	const double cr = std::cos(0.5*ypr[2]), sr = std::sin(0.5*ypr[2]);
	
	const double ccc = cr*cp*cy;
	const double ccs = cr*cp*sy;
	const double css = cr*sp*sy;
	const double sss = sr*sp*sy;
	const double scc = sr*cp*cy;
	const double ssc = sr*sp*cy;
	const double csc = cr*sp*cy;
	const double scs = sr*cp*sy;
	
	q[0] = ccc + sss;
	q[1] = scc - css;
	q[2] = csc + scs;
	q[3] = ccs - ssc;
}

void toYPR(const double q[4], double ypr[3]) {
  const double d = q[0]*q[2] - q[1]*q[3];
  
  if (d > 0.49999) {
    ypr[0] = -2.0*std::atan2(q[1], q[0]);
    ypr[1] = 0.5 * M_PI;
    ypr[2] = 0.0;
  } else if (d < -0.49999) {
    ypr[0] = 2.0*std::atan2(q[1], q[0]);
    ypr[1] = -0.5 * M_PI;
    ypr[2] = 0.0;
  } else { // General case
    ypr[0] = std::atan2(2.0*(q[0]*q[3] + q[1]*q[2]), 1.0 - 2.0*(q[2]*q[2] + q[3]*q[3]));
    ypr[1] = std::asin(2.0*d);
    ypr[2] = std::atan2(2.0*(q[0]*q[1] + q[2]*q[3]), 1.0 - 2.0*(q[1]*q[1] + q[2]*q[2]));
  }
}

double qnorm(const double q[4]) {
  return std::sqrt(q[0]*q[0] + q[1]*q[1] + q[2]*q[2] + q[3]*q[3]);
}

void qnormalize(double q[4]) {
  
  double norm = (q[0] < 0.0) ? -1.0 : 1.0;
  norm /= std::sqrt(q[0]*q[0] + q[1]*q[1] + q[2]*q[2] + q[3]*q[3]);
  
  q[0] *= norm;
  q[1] *= norm;
  q[2] *= norm;
  q[3] *= norm;
}

void qinverse(const double q[4], double qinv[4]) {
  qinv[0] =  q[0];
  qinv[1] = -q[1];
  qinv[2] = -q[2];
  qinv[3] = -q[3];
}

void qproduct(const double p[4], const double q[4], double pq[4]) {
  pq[0] = p[0]*q[0] - p[1]*q[1] - p[2]*q[2] - p[3]*q[3];
  pq[1] = p[0]*q[1] + p[1]*q[0] + p[2]*q[3] - p[3]*q[2];
  pq[2] = p[0]*q[2] - p[1]*q[3] + p[2]*q[0] + p[3]*q[1];
  pq[3] = p[0]*q[3] + p[1]*q[2] - p[2]*q[1] + p[3]*q[0];
}

void qrotate(const double q[4], const double v[3], double result[3]) {
  const double t2 =  q[0] * q[1];
  const double t3 =  q[0] * q[2];
  const double t4 =  q[0] * q[3];
  const double t5 = -q[1] * q[1];
  const double t6 =  q[1] * q[2];
  const double t7 =  q[1] * q[3];
  const double t8 = -q[2] * q[2];
  const double t9 =  q[2] * q[3];
  const double t1 = -q[3] * q[3];
  
  result[0] = 2.0 * ((t8 + t1) * v[0] + (t6 - t4) * v[1] + (t3 + t7) * v[2]) + v[0];
  result[1] = 2.0 * ((t4 + t6) * v[0] + (t5 + t1) * v[1] + (t9 - t2) * v[2]) + v[1];
  result[2] = 2.0 * ((t7 - t3) * v[0] + (t2 + t9) * v[1] + (t5 + t8) * v[2]) + v[2];
}
