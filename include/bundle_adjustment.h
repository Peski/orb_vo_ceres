
#ifndef BUNDLE_ADJUSTMENT_H
#define BUNDLE_ADJUSTMENT_H

#include <cstdint>
#include <memory>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

// Ceres
#include <ceres/ceres.h>

// Eigen
#include <Eigen/Core>

// Colmap
#include <colmap/util/alignment.h>

#include "eigen.hpp"
#include "SE3Quat.h"

namespace std {

// Hash function specialization for uint32_t pairs, e.g., ORB_SLAM::id_t.
template <>
struct hash<std::pair<uint32_t, uint32_t>> {
  std::size_t operator()(const std::pair<uint32_t, uint32_t>& p) const {
    const uint64_t s = (static_cast<uint64_t>(p.first) << 32) +
                       static_cast<uint64_t>(p.second);
    return std::hash<uint64_t>()(s);
  }
};

} // namespace std

namespace ORB_SLAM {

class Point3D;

using id_t = std::uint32_t;
using pair_id_t = std::pair<id_t, id_t>;

bool HasPointPositiveDepth(const Point3D& point, const SE3Quat& pose);

class Point3D {
public:

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  Point3D();

  Point3D(const Eigen::Vector3d& xyz);

  // Access 3d point vector as (x, y, z) specifying the position of the
  // 3D point defined in world coordinates.
  const Eigen::Vector3d& XYZ() const;
  Eigen::Vector3d& XYZ();
  double XYZ(const size_t idx) const;
  double& XYZ(const size_t idx);
  void SetXYZ(const Eigen::Vector3d& xyz);

private:

  Eigen::Vector3d xyz_;
};

class ProjectionConstraint {
public:

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  ProjectionConstraint();
  ProjectionConstraint(const id_t point_id, const id_t pose_id);

  id_t GetPointId() const;
  id_t GetPoseId() const;

  int& Level();
  const int& Level() const;
  void SetLevel(const int level);

  double& Chi2();
  const double& Chi2() const;

  void UpdateChi2(const Point3D& point, const SE3Quat& pose);
  
  Eigen::Vector2d Weight() const;
  void SetWeight(const Eigen::Vector2d& weight);

  Eigen::Vector2d Measurement() const;
  void SetMeasurement(const Eigen::Vector2d& measurement);

  bool Check() const;

  double& fx();
  const double& fx() const;

  double& fy();
  const double& fy() const;

  double& cx();
  const double& cx() const;

  double& cy();
  const double& cy() const;

private:

  id_t point_id_, pose_id_; // Optimizable parameters

  int level_;
  double chi2_;

  double fx_, fy_, cx_, cy_; // Fixed parameters
  Eigen::Vector2d measurement_; // Observation (measurement)
  Eigen::Vector2d weight_; // Weight (1 / sqrt(var))
};

class PoseConstraint {
public:

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  PoseConstraint();
  PoseConstraint(const id_t pose_a_id, const id_t pose_b_id);

  id_t GetPoseAId() const;
  id_t GetPoseBId() const;

  int& Level();
  const int& Level() const;
  void SetLevel(const int level);

/*
  double& Chi2();
  const double& Chi2() const;

  void UpdateChi2(const SE3Quat& pose_a, const SE3Quat& pose_b);
*/
  
  Eigen::Matrix6d Weight() const;
  void SetWeight(const Eigen::Matrix6d& weight);

  SE3Quat Measurement() const;
  void SetMeasurement(const SE3Quat& measurement);

  bool Check() const;

private:

  id_t pose_a_id_, pose_b_id_; // Optimizable parameters

  int level_;
  double chi2_;

  SE3Quat relative_pose_; // Observation
  Eigen::Matrix6d weight_; // Weight (1 / sqrt(var), position then orientation)
};

class ConstraintGraph {
public:

  ConstraintGraph();

  void AddPoint(const id_t id, const Point3D& point);
  void AddPose(const id_t id, const SE3Quat& pose);
  void AddPoseConstraint(const id_t pose_a_id, const id_t pose_b_id, const PoseConstraint& constraint);
  void AddPoseConstraint(const pair_id_t pair_id, const PoseConstraint& constraint);
  void AddProjectionConstraint(const id_t point_id, const id_t pose_id, const ProjectionConstraint& constraint);
  void AddProjectionConstraint(const pair_id_t pair_id, const ProjectionConstraint& constraint);

  void RemovePoseConstraint(const id_t pose_a_id, const id_t pose_b_id);
  void RemovePoseConstraint(const pair_id_t pair_id);
  void RemoveProjectionConstraint(const id_t point_id, const id_t pose_id);
  void RemoveProjectionConstraint(const pair_id_t pair_id);

  bool HasPoint(const id_t id) const;
  bool HasPose(const id_t id) const;
  bool HasPoseConstraint(const id_t pose_a_id, const id_t pose_b_id) const;
  bool HasPoseConstraint(const pair_id_t pair_id) const;
  bool HasProjectionConstraint(const id_t point_id, const id_t pose_id) const;
  bool HasProjectionConstraint(const pair_id_t pair_id) const;

  PoseConstraint& GetPoseConstraint(const id_t pose_a_id, const id_t pose_b_id);
  const PoseConstraint& GetPoseConstraint(const id_t point_id, const id_t pose_id) const;
  ProjectionConstraint& GetProjectionConstraint(const id_t point_id, const id_t pose_id);
  const ProjectionConstraint& GetProjectionConstraint(const id_t point_id, const id_t pose_id) const;

  PoseConstraint& GetPoseConstraint(const pair_id_t pair_id);
  const PoseConstraint& GetPoseConstraint(const pair_id_t pair_id) const;
  ProjectionConstraint& GetProjectionConstraint(const pair_id_t pair_id);
  const ProjectionConstraint& GetProjectionConstraint(const pair_id_t pair_id) const;

  Point3D& GetPoint(const id_t id);
  const Point3D& GetPoint(const id_t id) const;

  SE3Quat& GetPose(const id_t id);
  const SE3Quat& GetPose(const id_t id) const;

  const std::unordered_map<id_t, Point3D>& GetPoints() const;
  const std::unordered_map<id_t, SE3Quat>& GetPoses() const;
  const std::unordered_map<pair_id_t, PoseConstraint>& GetPoseConstraints() const;
  const std::unordered_map<pair_id_t, ProjectionConstraint>& GetProjectionConstraints() const;

private:

  std::unordered_map<id_t, Point3D> points_;
  std::unordered_map<id_t, SE3Quat> poses_;
  std::unordered_map<pair_id_t, PoseConstraint> pose_constraints_;
  std::unordered_map<pair_id_t, ProjectionConstraint> projection_constraints_;
};

struct BundleAdjustmentOptions {
  // Loss function types: Trivial (non-robust) and Cauchy (robust) loss.
  enum class LossFunctionType { TRIVIAL, HUBER, SOFT_L1, CAUCHY };
  LossFunctionType pose_loss_type = LossFunctionType::TRIVIAL;
  LossFunctionType projection_loss_type = LossFunctionType::TRIVIAL;

  // Scaling factor determines residual at which robustification takes place.
  double pose_loss_scale = 1.0;
  double projection_loss_scale = 1.0;

  // Whether to refine the focal length parameter group.
  // bool refine_focal_length = true;

  // Whether to refine the principal point parameter group.
  // bool refine_principal_point = false;

  // Whether to refine the extra parameter group.
  // bool refine_extra_params = true;

  // Whether to refine the extrinsic parameter group.
  // bool refine_extrinsics = true;

  // Only include constraints below or at a certain level
  int optimization_level = 0;

  // Whether to compute the final residual error
  bool compute_errors = false;
  
  // Whether to compute the pose covariance
  bool compute_covariance = false;

  // Whether to print a final summary.
  bool print_summary = false;

  // Ceres-Solver options.
  ceres::Solver::Options solver_options;

  BundleAdjustmentOptions() {
    solver_options.function_tolerance = 0.0;
    solver_options.gradient_tolerance = 0.0;
    solver_options.parameter_tolerance = 0.0;
    solver_options.minimizer_progress_to_stdout = false;
    solver_options.max_num_iterations = 100;
    solver_options.max_linear_solver_iterations = 200;
    solver_options.max_num_consecutive_invalid_steps = 10;
    solver_options.max_consecutive_nonmonotonic_steps = 10;
    solver_options.num_threads = -1;
#if CERES_VERSION_MAJOR < 2
    solver_options.num_linear_solver_threads = -1;
#endif  // CERES_VERSION_MAJOR
  }

  // Create a new loss function. 
  // The caller takes ownership of the loss function.
  static ceres::LossFunction* CreateLossFunction(const LossFunctionType loss_function_type,
                                                 const double loss_function_scale = 1.0);

  bool Check() const;
};

// Configuration container to setup bundle adjustment problems.
// (assuming a single camera with fixed parameters)
// This class contains only holds information about what should be kept constant.
class BundleAdjustmentConfig {
 public:
  BundleAdjustmentConfig();

  size_t NumConstantPoses() const;
  size_t NumConstantTvecs() const;
  size_t NumConstantPoints() const;

  // Set the pose of added images as constant. The pose is defined as the
  // rotational and translational part of the projection matrix.
  void SetConstantPose(const id_t id);
  bool HasConstantPose(const id_t id) const;
  void RemoveConstantPose(const id_t id);

  // Set the translational part of the pose, hence the constant pose
  // indices may be in [0, 1, 2] and must be unique. Note that the
  // corresponding images have to be added prior to calling these methods.
  void SetConstantTvec(const id_t id, const std::vector<int>& idxs);
  bool HasConstantTvec(const id_t id) const;
  void RemoveConstantTvec(const id_t id);

  // Add / remove points from the configuration. Note that points can either
  // be variable or constant but not both at the same time.
  void SetConstantPoint(const id_t id);
  bool HasConstantPoint(const id_t id) const;
  void RemoveConstantPoint(const id_t id);

  // Access configuration data.
  const std::unordered_set<id_t>& ConstantPoints() const;
  const std::unordered_set<id_t>& ConstantPoses() const;
  const std::vector<int>& ConstantTvec(const id_t id) const;
  const std::unordered_map<id_t, std::vector<int>>& ConstantTvecs() const;

 private:
  std::unordered_set<id_t> constant_points_;
  std::unordered_set<id_t> constant_poses_;
  std::unordered_map<id_t, std::vector<int>> constant_tvecs_;
};

// Bundle adjustment based on Ceres-Solver. Enables most flexible configurations
// and provides best solution quality.
class BundleAdjuster {
 public:
  BundleAdjuster(const BundleAdjustmentOptions& options,
                 const BundleAdjustmentConfig& config);

  bool Solve(ConstraintGraph* graph);

  // Get the Ceres solver summary for the last call to `Solve`.
  const ceres::Solver::Summary& Summary() const;
  
  // Get solution covariacne of the last pose
  Eigen::MatrixXd Covariance() const;

 private:

  void SetUp(ConstraintGraph* graph);
  void TearDown(ConstraintGraph* graph);

  void ParameterizePoints(ConstraintGraph* graph);
  void ParameterizePoses(ConstraintGraph* graph);
  
  double covariance_xx_[9];
  double covariance_yy_[9];
  double covariance_xy_[9];

  const BundleAdjustmentOptions options_;
  const BundleAdjustmentConfig config_;
  std::unique_ptr<ceres::Problem> problem_;
  ceres::Solver::Summary summary_;
  std::unordered_map<id_t, size_t> poses_num_observations_;
  std::unordered_map<id_t, size_t> points_num_observations_;
  std::unordered_map<ceres::ResidualBlockId, pair_id_t> residuals_;
};

void PrintSolverSummary(const ceres::Solver::Summary& summary);

} // namespace ORB_SLAM

#endif  // BUNDLE_ADJUSTMENT_H
