
/**
* This file is NOT part of ORB-SLAM.
*/

#ifndef SE3QUAT_H
#define SE3QUAT_H

#include <Eigen/Core>
#include <Eigen/Geometry>

namespace ORB_SLAM
{

// SE3 pose with quaternion representation for rotation
//   This structure is used for optimization for ceres and within INS
struct SE3Quat {
    // Translation (x, y, z order) and quaternion (w, x, y, z order)
    double t[3], q[4];

    // Default constructor (identity)
    SE3Quat();

    // Full constructor
    SE3Quat(double tx, double ty, double tz, double qw, double qx, double qy, double qz);

    // t_vec: (tx, ty, tz); q_vec: (qw, qx, qy, qz)
    SE3Quat(const Eigen::Vector3d& t_vec, const Eigen::Vector4d& q_vec);

    // From Eigen Transform (constructor)
    template<typename Scalar, int Type>
    SE3Quat(const Eigen::Transform<Scalar, 3, Type> &T);

    // To Eigen Transform (implicit conversion)
    template<typename Scalar, int Type>
    operator Eigen::Transform<Scalar, 3, Type>() const;

    // Inverse operator
    SE3Quat inverse() const;

    // Concatenation (this * other)
    SE3Quat operator *(const SE3Quat& other) const;

    // Quaternion normalization (to avoid numerical issues)
    void q_normalize();

    // Identity transformation
    static SE3Quat Identity();
};

} //namespace ORB_SLAM

#endif // SE3QUAT_H