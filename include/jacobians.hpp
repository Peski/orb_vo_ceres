
/**
* This file is NOT part of ORB-SLAM.
*/

#ifndef JACOBIANS_H
#define JACOBIANS_H

// STL
#include <cmath>

// Eigen
#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/Geometry>

#include "SE3Quat.h"

namespace ORB_SLAM
{

// TODO Add size checks instead of resizes

// Jacobians of quaternion and quaternion-based pose funcitons, assuming unit quaternions
// All functions have been thoroughly tested with symbolic differentiation tools

// Quaternion Jacobians

// TODO Not tested
/*
inline void normalizationJacobian(const double q[4], Eigen::MatrixXd& J) {
  J.resize(4, 4);
  
  const double n = 1.0 / std::pow(q[0]*q[0] + q[1]*q[1] + q[2]*q[2] + q[3]*q[3], 1.5);
  
  J(0, 0) =  q[1]*q[1] + q[2]*q[2] + q[3]*q[3];
  J(0, 1) = -q[0]*q[1];
  J(0, 2) = -q[0]*q[2];
  J(0, 3) = -q[0]*q[3];

  J(1, 0) = -q[1]*q[0];
  J(1, 1) =  q[0]*q[0] + q[2]*q[2] + q[3]*q[3];
  J(1, 2) = -q[1]*q[2];
  J(1, 3) = -q[1]*q[3];

  J(2, 0) = -q[2]*q[0];
  J(2, 1) = -q[2]*q[1];
  J(2, 2) =  q[0]*q[0] + q[1]*q[1] + q[3]*q[3];
  J(2, 3) = -q[2]*q[3];

  J(3, 0) = -q[3]*q[0];
  J(3, 1) = -q[3]*q[1];
  J(3, 2) = -q[3]*q[2];
  J(3, 3) =  q[0]*q[0] + q[1]*q[1] + q[2]*q[2];
  
  J *= n;
}
*/

inline void fromYPRJacobian(const double ypr[3], Eigen::Ref<Eigen::MatrixXd> J) {
  //J.resize(4, 3);
  
  const double cy = std::cos(0.5*ypr[0]), sy = std::sin(0.5*ypr[0]);
	const double cp = std::cos(0.5*ypr[1]), sp = std::sin(0.5*ypr[1]);
	const double cr = std::cos(0.5*ypr[2]), sr = std::sin(0.5*ypr[2]);
	
	const double ccc = cr*cp*cy;
	const double ccs = cr*cp*sy;
	const double css = cr*sp*sy;
	const double sss = sr*sp*sy;
	const double scc = sr*cp*cy;
	const double ssc = sr*sp*cy;
	const double csc = cr*sp*cy;
	const double scs = sr*cp*sy;
	
	J(0, 0) =  0.5*(ssc - ccs);
	J(0, 1) =  0.5*(scs - csc);
	J(0, 2) =  0.5*(css - scc);
	
	J(1, 0) = -0.5*(csc + scs);
	J(1, 1) = -0.5*(ssc + ccs);
	J(1, 2) =  0.5*(ccc + sss);
	
	J(2, 0) =  0.5*(scc - css);
	J(2, 1) =  0.5*(ccc - sss);
	J(2, 2) =  0.5*(ccs - ssc);
	
	J(3, 0) =  0.5*(ccc + sss);
	J(3, 1) = -0.5*(css + scc);
	J(3, 2) = -0.5*(csc + scs);
}

inline void toYPRJacobian(const double q[4], Eigen::Ref<Eigen::MatrixXd> J) {
  //J.resize(3, 4);
  
  double d = q[0]*q[2] - q[1]*q[3];
  if (d > 0.49999) {
    const double t = 2.0 / (q[0]*q[0] + q[1]*q[1]);
    
    J.setZero();
    J(0, 0) =  q[1]*t;
    J(0, 1) = -q[0]*t;
  } else if (d < -0.49999) {
    const double t = 2.0 / (q[0]*q[0] + q[1]*q[1]);
    
    J.setZero();
    J(0, 0) = -q[1]*t;
    J(0, 1) =  q[0]*t;
  } else { // General case
    const double t1  = (2.0*q[2]*q[2] + 2.0*q[3]*q[3] - 1.0);
    const double t2  = (2.0*q[0]*q[3] + 2.0*q[1]*q[2]);
    const double t3  =  1.0 / (t1*t1 + t2*t2);
    const double t4  = -2.0*t1*t3;
    const double t5  =  4.0*t2*t3;
    
    J(0, 0) = q[3]*t4;
    J(0, 1) = q[2]*t4;
    J(0, 2) = q[1]*t4 + q[2]*t5;
    J(0, 3) = q[0]*t4 + q[3]*t5;
    
    const double t6  =  2.0 / std::sqrt(1.0 - std::pow(2.0*q[0]*q[2] - 2.0*q[1]*q[3], 2));
    
    J(1, 0) =  q[2]*t6;
    J(1, 1) = -q[3]*t6;
    J(1, 2) =  q[0]*t6;
    J(1, 3) = -q[1]*t6;
    
    const double t7  = (2.0*q[1]*q[1] + 2.0*q[2]*q[2] - 1.0);
    const double t8  = (2.0*q[0]*q[1] + 2.0*q[2]*q[3]);
    const double t9  =  1.0 / (t7*t7 + t8*t8);
    const double t10 = -2.0*t7*t9;
    const double t11 =  4.0*t8*t9;
    
    J(2, 0) = q[1]*t10;
    J(2, 1) = q[0]*t10 + q[1]*t11;
    J(2, 2) = q[3]*t10 + q[2]*t11;
    J(2, 3) = q[2]*t10;
  }
}

// SE3Quat Jacobians

inline void inverseJacobian(const SE3Quat& p, Eigen::Ref<Eigen::MatrixXd> J) {
  //J.resize(7, 7);
  J.setZero();
  
  const double t1 = p.q[1]*p.q[1];
  const double t2 = p.q[2]*p.q[2];
  const double t3 = p.q[3]*p.q[3];
  
  J(0, 0) =  2.0*t2            + 2.0*t3            - 1.0;
  J(0, 1) = -2.0*p.q[0]*p.q[3] - 2.0*p.q[1]*p.q[2];
  J(0, 2) =  2.0*p.q[0]*p.q[2] - 2.0*p.q[1]*p.q[3];
  
  J(1, 0) =  2.0*p.q[0]*p.q[3] - 2.0*p.q[1]*p.q[2];
  J(1, 1) =  2.0*t1            + 2.0*t3            - 1.0;
  J(1, 2) = -2.0*p.q[0]*p.q[1] - 2.0*p.q[2]*p.q[3];
  
  J(2, 0) = -2.0*p.q[0]*p.q[2] - 2.0*p.q[1]*p.q[3];
  J(2, 1) =  2.0*p.q[0]*p.q[1] - 2.0*p.q[2]*p.q[3];
  J(2, 2) =  2.0*t1            + 2.0*t2            - 1.0;
  
  J(0, 3) = 2.0*( p.q[2]*p.t[2]     - p.q[3]*p.t[1]                    );
  J(0, 4) = 2.0*(-p.q[2]*p.t[1]     - p.q[3]*p.t[2]                    );
  J(0, 5) = 2.0*(-p.q[1]*p.t[1] + 2.0*p.q[2]*p.t[0]     + p.q[0]*p.t[2]);
  J(0, 6) = 2.0*(-p.q[1]*p.t[2]     - p.q[0]*p.t[1] + 2.0*p.q[3]*p.t[0]);
  
  J(1, 3) = 2.0*(-p.q[1]*p.t[2]     + p.q[3]*p.t[0]                    );
  J(1, 4) = 2.0*(-p.q[2]*p.t[0] + 2.0*p.q[1]*p.t[1]     - p.q[0]*p.t[2]);
  J(1, 5) = 2.0*(-p.q[1]*p.t[0]     - p.q[3]*p.t[2]                    );
  J(1, 6) = 2.0*(-p.q[2]*p.t[2] + 2.0*p.q[3]*p.t[1]     + p.q[0]*p.t[0]);
  
  J(2, 3) = 2.0*(-p.q[2]*p.t[0]     + p.q[1]*p.t[1]                    );
  J(2, 4) = 2.0*(-p.q[3]*p.t[0]     + p.q[0]*p.t[1] + 2.0*p.q[1]*p.t[2]);
  J(2, 5) = 2.0*(-p.q[0]*p.t[0]     - p.q[3]*p.t[1] + 2.0*p.q[2]*p.t[2]);
  J(2, 6) = 2.0*(-p.q[1]*p.t[0]     - p.q[2]*p.t[1]                    );
  
  J(3, 3) =  1.0;
  J(4, 4) = -1.0;
  J(5, 5) = -1.0;
  J(6, 6) = -1.0;
}

inline void compositionJacobians(const SE3Quat& a, const SE3Quat& b, Eigen::Ref<Eigen::MatrixXd> J_a, Eigen::Ref<Eigen::MatrixXd> J_b) {
  //J_a.resize(7, 7);
  J_a.setZero();
  
  J_a.block<3, 3>(0, 0) = Eigen::Matrix3d::Identity();
  
  J_a(0, 3) = 2.0*(    -a.q[3]*b.t[1]     + a.q[2]*b.t[2]                    );
  J_a(0, 4) = 2.0*(     a.q[2]*b.t[1]     + a.q[3]*b.t[2]                    );
  J_a(0, 5) = 2.0*(-2.0*a.q[2]*b.t[0]     + a.q[1]*b.t[1]     + a.q[0]*b.t[2]);
  J_a(0, 6) = 2.0*(-2.0*a.q[3]*b.t[0]     - a.q[0]*b.t[1]     + a.q[1]*b.t[2]);
  
  J_a(1, 3) = 2.0*(     a.q[3]*b.t[0]     - a.q[1]*b.t[2]                    );
  J_a(1, 4) = 2.0*(     a.q[2]*b.t[0] - 2.0*a.q[1]*b.t[1]     - a.q[0]*b.t[2]);
  J_a(1, 5) = 2.0*(     a.q[1]*b.t[0]     + a.q[3]*b.t[2]                    );
  J_a(1, 6) = 2.0*(     a.q[0]*b.t[0] - 2.0*a.q[3]*b.t[1]     + a.q[2]*b.t[2]);
  
  J_a(2, 3) = 2.0*(    -a.q[2]*b.t[0]     + a.q[1]*b.t[1]                    );
  J_a(2, 4) = 2.0*(     a.q[3]*b.t[0]     + a.q[0]*b.t[1] - 2.0*a.q[1]*b.t[2]);
  J_a(2, 5) = 2.0*(    -a.q[0]*b.t[0]     + a.q[3]*b.t[1] - 2.0*a.q[2]*b.t[2]);
  J_a(2, 6) = 2.0*(     a.q[1]*b.t[0]     + a.q[2]*b.t[1]                    );
  
  J_a(3, 3) =  b.q[0]; J_a(3, 4) = -b.q[1]; J_a(3, 5) = -b.q[2]; J_a(3, 6) = -b.q[3];
  
  J_a(4, 3) =  b.q[1]; J_a(4, 4) =  b.q[0]; J_a(4, 5) =  b.q[3]; J_a(4, 6) = -b.q[2];
  
  J_a(5, 3) =  b.q[2]; J_a(5, 4) = -b.q[3]; J_a(5, 5) =  b.q[0]; J_a(5, 6) =  b.q[1];
  
  J_a(6, 3) =  b.q[3]; J_a(6, 4) =  b.q[2]; J_a(6, 5) = -b.q[1]; J_a(6, 6) =  b.q[0];
  
  J_a.block<4, 4>(3, 3) = J_a.block<4, 4>(3, 3);
  
  J_b.resize(7, 7);
  J_b.setZero();
  
  J_b(0, 0) = 1.0 - 2.0*(a.q[2]*a.q[2] + a.q[3]*a.q[3]);
	J_b(0, 1) =       2.0*(a.q[1]*a.q[2] - a.q[0]*a.q[3]);
	J_b(0, 2) =       2.0*(a.q[0]*a.q[2] + a.q[1]*a.q[3]);
  
	J_b(1, 0) =       2.0*(a.q[0]*a.q[3] + a.q[1]*a.q[2]);
	J_b(1, 1) = 1.0 - 2.0*(a.q[1]*a.q[1] + a.q[3]*a.q[3]);
	J_b(1, 2) =       2.0*(a.q[2]*a.q[3] - a.q[0]*a.q[1]);
  
	J_b(2, 0) =       2.0*(a.q[1]*a.q[3] - a.q[0]*a.q[2]);
	J_b(2, 1) =       2.0*(a.q[0]*a.q[1] + a.q[2]*a.q[3]);
	J_b(2, 2) = 1.0 - 2.0*(a.q[1]*a.q[1] + a.q[2]*a.q[2]);
	
	J_b(3, 3) =  a.q[0]; J_b(3, 4) = -a.q[1]; J_b(3, 5) = -a.q[2]; J_b(3, 6) = -a.q[3];
  
  J_b(4, 3) =  a.q[1]; J_b(4, 4) =  a.q[0]; J_b(4, 5) = -a.q[3]; J_b(4, 6) =  a.q[2];
  
  J_b(5, 3) =  a.q[2]; J_b(5, 4) =  a.q[3]; J_b(5, 5) =  a.q[0]; J_b(5, 6) = -a.q[1];
  
  J_b(6, 3) =  a.q[3]; J_b(6, 4) = -a.q[2]; J_b(6, 5) =  a.q[1]; J_b(6, 6) =  a.q[0];
  
  J_b.block<4, 4>(3, 3) = J_b.block<4, 4>(3, 3);
}

} //namespace ORB_SLAM

#endif // SE3QUAT_H
