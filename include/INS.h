/**
* This file is NOT part of ORB-SLAM.
*/

#ifndef INS_H
#define INS_H

#include <atomic>
#include <cstdint>
#include <list>
#include <map>
#include <string>
#include <utility>
#include <vector>

#include <boost/thread/mutex.hpp>

#include <Eigen/Geometry>

#include <ros/ros.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geographic_msgs/GeoPoseStamped.h>

#include "eigen.hpp"
#include "geodetic_converter.h"
#include "KeyFrame.h"
#include "SE3Quat.h"

namespace ORB_SLAM
{

class KeyFrame;

class INS
{

public:

    // ORB_SLAM KeyFrame poses are actualy inverse poses (better for reprojection).
    // So, the relative pose between KF0 -> KF1 is computed as follows: dT * T0 = T1 (from KF1 to KF0)
    struct InertialFactor {
        KeyFrame* src; // KF1
        KeyFrame* dst; // KF0
        
        SE3Quat relative_pose; // dT
        
        Eigen::Matrix6d information_sqrt; // order: tx ty tz qx qy qz
        
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    };

    // Internal state
    enum class GPSTransformState {
        SYSTEM_NOT_READY=-1,
        NO_MEASUREMENTS_YET=0,
        NOT_INITIALIZED=1,
        INITIALIZED=2
    };
    
    // Communication protocol state
    enum class CommunicationState {
        SYSTEM_READY=-1,
        NOT_INITIALIZED=0,
        WAITING_KF=1,
        WAITING_IMU=2,
        BA_PENDING=3
    };

    // Constructor from config file
    INS(std::string strSettingPath);

    //void CheckResetByPublishers();

    // Returns whether or not the GPS transform has been initialized
//    int GetGPSTransformState();

    // KeyFrame pose getter
    std::map<KeyFrame*, SE3Quat> GetPoses();

    // Call this funciton when a new KeyFrame is detected (Tracking Thread)
    void NotifyNewKeyFrame(KeyFrame* pKF);
    
    void NotifyBATerminated();

    // Set the pair of KyeFrames used for initialization
    bool SetInitializationKeyFrames(KeyFrame* pKF0, KeyFrame* pKF1);

    // This is the main function of the INS Thread
    void Run();
    
    CommunicationState GetCommunicationState();
    
    std::vector<InertialFactor> GetInertialFactors();

    // Save poses
//    void Save(const std::string& file);

    // Publish local keyframes
//    void PublishLocal(long unsigned int nLocalIdStart);

    void GetRelPosesStamed(std::vector<SE3Quat> &rel_poses, std::vector<double> &timestamps);
    
    SE3Quat Tbc();
    
    double Scale();

protected:

    // Inset a new KF - Pose assotiation into mmKFPoses. Uses: mGPSTransformState|mMutexKFPoses|mKeyframes
//    void InsertPose(KeyFrame* pKF, const SE3Quat& pose);

    // Pose interpolation (for synchronization)
    SE3Quat InterpolatePose(const SE3Quat& A, const SE3Quat& B, double t);

    // ROS pose grabber
//    void GrabPose(const geometry_msgs::PoseStampedConstPtr& msg);

    // Discard useless observations from memory
//    void MarginalizeObservations(std::size_t idx);

    // Associate the given KF with an observation or push it to the buffer
//    std::size_t Sync(KeyFrame* pKF);
    
    // Publish sync
    void PublishSync(KeyFrame* pKF);

    // Publish keyframe pose
//    void Publish(KeyFrame* pKF, const SE3Quat& pose);

    void GrabGPS(const geographic_msgs::GeoPoseStampedPtr& msg);
    void GrabIMU(const geometry_msgs::PoseWithCovarianceStampedPtr& msg);

    // Synchronization parameters
    double mSyncTh;
    double mSyncEps;

    // For ROS callback queue
    double mWallDuration;
    
    std::uint32_t mSyncSeq;

    // Together with mGPSTransform makes the Sim(3) transform sRx + t (to GPS XYZ)
    double mScale;
    //SE3Quat mGPSTransform;
    
    std::atomic<CommunicationState> mCommunicationState;

    boost::mutex mMutexSyncKFs;
    std::vector<KeyFrame*> mvSyncKFs;

    // Internal state variable
    std::atomic<GPSTransformState> mGPSTransformState;
    boost::mutex mMutexGPSData;
    std::vector<double> mvGPSTimestamps;
    std::vector<geographic_msgs::GeoPose> mvGPSData;

    // Geodetic converter
//    GeodeticConverter mGPSConverter;

    // Rigid-body transformation from camera coordinates to body frame
    SE3Quat Tbc_;
    SE3Quat Tbc_inv;
    
    KeyFrame* kf_src;
    KeyFrame* kf_dst;

    // KeyFrame -> INS pose (inverse) associaiton
    boost::mutex mMutexKFPoses;
    std::map<KeyFrame*, SE3Quat> mmKFPoses;
    
    boost::mutex mMutexInertialData;
    std::vector<InertialFactor> mvInertialData;

    // Temporal buffer for INS input data
    boost::mutex mMutexNewPoses;
    std::vector<double> mvOrderedTimestamps;
    std::vector<SE3Quat> mvPoses;

    // Temporal KF buffer (in case association was not possible because KF is past actual INS measurements)
    std::list<KeyFrame*> mlKFBuffer;

    // KeyFrame -> GPS pose association
    boost::mutex mMutexKFGPS;
    std::map<KeyFrame*, SE3Quat> mmKFGPS;

    // ROS publishers
    ros::NodeHandle nh;
    ros::Publisher sync_pub;
    //ros::Publisher pub_local;
//    ros::Publisher estimate_pub;

    std::vector<double> mvIMURelPosesTimestamps;
    std::vector<SE3Quat> mvIMURelPoses;
};

} //namespace ORB_SLAM

#endif // INS_H