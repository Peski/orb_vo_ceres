
#include <iostream>

#include <Eigen/Geometry>

#include "colmap/util/random.h"

#include "bundle_adjustment.h"
#include "INS.h"

int main() {

  std::cout << "Testing Optimization" << std::endl;
  std::cout << "--------------------" << std::endl;

{ // Constant A
  std::cout << "Testing constant pose A" << std::endl;

  ORB_SLAM::ConstraintGraph graph;

  ORB_SLAM::BundleAdjustmentOptions options;
  options.projection_loss_type = ORB_SLAM::BundleAdjustmentOptions::LossFunctionType::HUBER;
  options.projection_loss_scale = std::sqrt(5.991);
  options.compute_errors = false;
  options.print_summary = true;
  options.solver_options.max_num_iterations = 20;
  options.solver_options.minimizer_progress_to_stdout = true;

  ORB_SLAM::BundleAdjustmentConfig config;

  // Groundtruth
  Eigen::Quaterniond q = Eigen::AngleAxisd(colmap::RandomReal<double>(-EIGEN_PI / 2.0, EIGEN_PI / 2.0), Eigen::Vector3d::UnitX())
                         * Eigen::AngleAxisd(colmap::RandomReal<double>(-EIGEN_PI / 2.0, EIGEN_PI / 2.0), Eigen::Vector3d::UnitY())
                         * Eigen::AngleAxisd(colmap::RandomReal<double>(-EIGEN_PI / 2.0, EIGEN_PI / 2.0), Eigen::Vector3d::UnitZ());

  Eigen::Vector4d q_vec(q.w(), q.x(), q.y(), q.z());
  Eigen::Vector3d t_vec(colmap::RandomReal<double>(-1.0, 1.0),
                        colmap::RandomReal<double>(-1.0, 1.0),
                        colmap::RandomReal<double>(-1.0, 1.0));

  ORB_SLAM::pose_t pose_a; // Identity
  ORB_SLAM::pose_t pose_b(t_vec.x(), t_vec.y(), t_vec.z(), q.w(), q.x(), q.y(), q.z());

  ORB_SLAM::pose_t p_ba = pose_b.inverse() * pose_a;
  Eigen::Vector4d qba_vec(p_ba.qw, p_ba.qx, p_ba.qy, p_ba.qz);
  Eigen::Vector3d tba_vec(p_ba.tx, p_ba.ty, p_ba.tz);

  graph.AddPose(0, ORB_SLAM::SE3Quat::Identity()); // A - constant
  config.SetConstantPose(0);

  graph.AddPose(1, ORB_SLAM::SE3Quat::Identity()); // B - variable

  ORB_SLAM::PoseConstraint constraint(0, 1);

  ORB_SLAM::SE3Quat relative_pose(qba_vec, tba_vec);
  Eigen::Vector6d weight;
  weight << 1.0, 1.0, 1.0, 1.0, 1.0, 1.0;

  constraint.SetMeasurement(relative_pose);
  constraint.SetWeight(weight);

  graph.AddPoseConstraint(0, 1, constraint);

  ORB_SLAM::BundleAdjuster ba(options, config);
  ba.Solve(&graph);

  std::cout << "Goal:" << std::endl;
  std::cout << q_vec.transpose() << std::endl;
  std::cout << t_vec.transpose() << std::endl;

  std::cout << "Estimated:" << std::endl;
  ORB_SLAM::SE3Quat estimated = graph.GetPose(1);
  ORB_SLAM::pose_t p(estimated.Tvec(0), estimated.Tvec(1), estimated.Tvec(2),
                     estimated.Qvec(0), estimated.Qvec(1), estimated.Qvec(2), estimated.Qvec(3));

  p = p.inverse();
  std::cout << p.qw << " " << p.qx << " " << p.qy << " " << p.qz << std::endl;
  std::cout << p.tx << " " << p.ty << " " << p.tz << std::endl;

}

{ // Constant B
  std::cout << std::endl << "Testing constant pose B" << std::endl;

  ORB_SLAM::ConstraintGraph graph;

  ORB_SLAM::BundleAdjustmentOptions options;
  options.projection_loss_type = ORB_SLAM::BundleAdjustmentOptions::LossFunctionType::HUBER;
  options.projection_loss_scale = std::sqrt(5.991);
  options.compute_errors = false;
  options.print_summary = true;
  options.solver_options.max_num_iterations = 20;
  options.solver_options.minimizer_progress_to_stdout = true;

  ORB_SLAM::BundleAdjustmentConfig config;

  // Groundtruth
  Eigen::Quaterniond q = Eigen::AngleAxisd(colmap::RandomReal<double>(-EIGEN_PI / 2.0, EIGEN_PI / 2.0), Eigen::Vector3d::UnitX())
                         * Eigen::AngleAxisd(colmap::RandomReal<double>(-EIGEN_PI / 2.0, EIGEN_PI / 2.0), Eigen::Vector3d::UnitY())
                         * Eigen::AngleAxisd(colmap::RandomReal<double>(-EIGEN_PI / 2.0, EIGEN_PI / 2.0), Eigen::Vector3d::UnitZ());

  Eigen::Vector4d q_vec(q.w(), q.x(), q.y(), q.z());
  Eigen::Vector3d t_vec(colmap::RandomReal<double>(-1.0, 1.0),
                        colmap::RandomReal<double>(-1.0, 1.0),
                        colmap::RandomReal<double>(-1.0, 1.0));

  ORB_SLAM::pose_t pose_a(t_vec.x(), t_vec.y(), t_vec.z(), q.w(), q.x(), q.y(), q.z());
  ORB_SLAM::pose_t pose_b; // Identity

  ORB_SLAM::pose_t p_ba = pose_b.inverse() * pose_a;
  Eigen::Vector4d qba_vec(p_ba.qw, p_ba.qx, p_ba.qy, p_ba.qz);
  Eigen::Vector3d tba_vec(p_ba.tx, p_ba.ty, p_ba.tz);

  graph.AddPose(0, ORB_SLAM::SE3Quat::Identity()); // A - variable
  //config.SetConstantPose(0);

  graph.AddPose(1, ORB_SLAM::SE3Quat::Identity()); // B - constant
  config.SetConstantPose(1);

  ORB_SLAM::PoseConstraint constraint(0, 1);

  ORB_SLAM::SE3Quat relative_pose(qba_vec, tba_vec);
  Eigen::Vector6d weight;
  weight << 1.0, 1.0, 1.0, 1.0, 1.0, 1.0;

  constraint.SetMeasurement(relative_pose);
  constraint.SetWeight(weight);

  graph.AddPoseConstraint(0, 1, constraint);

  ORB_SLAM::BundleAdjuster ba(options, config);
  ba.Solve(&graph);

  std::cout << "Goal:" << std::endl;
  std::cout << q_vec.transpose() << std::endl;
  std::cout << t_vec.transpose() << std::endl;

  std::cout << "Estimated:" << std::endl;
  ORB_SLAM::SE3Quat estimated = graph.GetPose(0);
  ORB_SLAM::pose_t p(estimated.Tvec(0), estimated.Tvec(1), estimated.Tvec(2),
                     estimated.Qvec(0), estimated.Qvec(1), estimated.Qvec(2), estimated.Qvec(3));

  p = p.inverse();
  std::cout << p.qw << " " << p.qx << " " << p.qy << " " << p.qz << std::endl;
  std::cout << p.tx << " " << p.ty << " " << p.tz << std::endl;

}

{ // C-V-V
  std::cout << std::endl << "Testing constant-variable-variable" << std::endl;

  ORB_SLAM::ConstraintGraph graph;

  ORB_SLAM::BundleAdjustmentOptions options;
  options.projection_loss_type = ORB_SLAM::BundleAdjustmentOptions::LossFunctionType::HUBER;
  options.projection_loss_scale = std::sqrt(5.991);
  options.compute_errors = false;
  options.print_summary = true;
  options.solver_options.max_num_iterations = 20;
  options.solver_options.minimizer_progress_to_stdout = true;

  ORB_SLAM::BundleAdjustmentConfig config;

  // Groundtruth
  Eigen::Quaterniond q1 = Eigen::AngleAxisd(colmap::RandomReal<double>(-EIGEN_PI / 2.0, EIGEN_PI / 2.0), Eigen::Vector3d::UnitX())
                         * Eigen::AngleAxisd(colmap::RandomReal<double>(-EIGEN_PI / 2.0, EIGEN_PI / 2.0), Eigen::Vector3d::UnitY())
                         * Eigen::AngleAxisd(colmap::RandomReal<double>(-EIGEN_PI / 2.0, EIGEN_PI / 2.0), Eigen::Vector3d::UnitZ());

  Eigen::Vector4d q1_vec(q1.w(), q1.x(), q1.y(), q1.z());
  Eigen::Vector3d t1_vec(colmap::RandomReal<double>(-1.0, 1.0),
                         colmap::RandomReal<double>(-1.0, 1.0),
                         colmap::RandomReal<double>(-1.0, 1.0));

  Eigen::Quaterniond q2 = Eigen::AngleAxisd(colmap::RandomReal<double>(-EIGEN_PI / 2.0, EIGEN_PI / 2.0), Eigen::Vector3d::UnitX())
                         * Eigen::AngleAxisd(colmap::RandomReal<double>(-EIGEN_PI / 2.0, EIGEN_PI / 2.0), Eigen::Vector3d::UnitY())
                         * Eigen::AngleAxisd(colmap::RandomReal<double>(-EIGEN_PI / 2.0, EIGEN_PI / 2.0), Eigen::Vector3d::UnitZ());

  Eigen::Vector4d q2_vec(q2.w(), q2.x(), q2.y(), q2.z());
  Eigen::Vector3d t2_vec(colmap::RandomReal<double>(-1.0, 1.0),
                         colmap::RandomReal<double>(-1.0, 1.0),
                         colmap::RandomReal<double>(-1.0, 1.0));

  ORB_SLAM::pose_t pose_a; // Identity
  ORB_SLAM::pose_t pose_b(t1_vec.x(), t1_vec.y(), t1_vec.z(), q1.w(), q1.x(), q1.y(), q1.z());
  ORB_SLAM::pose_t pose_c(t2_vec.x(), t2_vec.y(), t2_vec.z(), q2.w(), q2.x(), q2.y(), q2.z());

  ORB_SLAM::pose_t p_ba = pose_b.inverse() * pose_a;
  Eigen::Vector4d qba_vec(p_ba.qw, p_ba.qx, p_ba.qy, p_ba.qz);
  Eigen::Vector3d tba_vec(p_ba.tx, p_ba.ty, p_ba.tz);

  ORB_SLAM::pose_t p_cb = pose_c.inverse() * pose_b;
  Eigen::Vector4d qcb_vec(p_cb.qw, p_cb.qx, p_cb.qy, p_cb.qz);
  Eigen::Vector3d tcb_vec(p_cb.tx, p_cb.ty, p_cb.tz);

  graph.AddPose(0, ORB_SLAM::SE3Quat::Identity()); // A - constant
  config.SetConstantPose(0);

  graph.AddPose(1, ORB_SLAM::SE3Quat::Identity()); // B - variable

  graph.AddPose(2, ORB_SLAM::SE3Quat::Identity()); // C - variable

{
  ORB_SLAM::PoseConstraint constraint(0, 1);

  ORB_SLAM::SE3Quat relative_pose(qba_vec, tba_vec);
  Eigen::Vector6d weight;
  weight << 1.0, 1.0, 1.0, 1.0, 1.0, 1.0;

  constraint.SetMeasurement(relative_pose);
  constraint.SetWeight(weight);

  graph.AddPoseConstraint(0, 1, constraint);
}

{
  ORB_SLAM::PoseConstraint constraint(1, 2);

  ORB_SLAM::SE3Quat relative_pose(qcb_vec, tcb_vec);
  Eigen::Vector6d weight;
  weight << 1.0, 1.0, 1.0, 1.0, 1.0, 1.0;

  constraint.SetMeasurement(relative_pose);
  constraint.SetWeight(weight);

  graph.AddPoseConstraint(1, 2, constraint);
}

  ORB_SLAM::BundleAdjuster ba(options, config);
  ba.Solve(&graph);

  std::cout << "Goal:" << std::endl;
  std::cout << q1_vec.transpose() << std::endl;
  std::cout << t1_vec.transpose() << std::endl;
  std::cout << q2_vec.transpose() << std::endl;
  std::cout << t2_vec.transpose() << std::endl;

  std::cout << "Estimated:" << std::endl;
  ORB_SLAM::SE3Quat estimated1 = graph.GetPose(1);
  ORB_SLAM::pose_t p1(estimated1.Tvec(0), estimated1.Tvec(1), estimated1.Tvec(2),
                      estimated1.Qvec(0), estimated1.Qvec(1), estimated1.Qvec(2), estimated1.Qvec(3));

  p1 = p1.inverse();
  std::cout << p1.qw << " " << p1.qx << " " << p1.qy << " " << p1.qz << std::endl;
  std::cout << p1.tx << " " << p1.ty << " " << p1.tz << std::endl;

  ORB_SLAM::SE3Quat estimated2 = graph.GetPose(2);
  ORB_SLAM::pose_t p2(estimated2.Tvec(0), estimated2.Tvec(1), estimated2.Tvec(2),
                      estimated2.Qvec(0), estimated2.Qvec(1), estimated2.Qvec(2), estimated2.Qvec(3));

  p2 = p2.inverse();
  std::cout << p2.qw << " " << p2.qx << " " << p2.qy << " " << p2.qz << std::endl;
  std::cout << p2.tx << " " << p2.ty << " " << p2.tz << std::endl;
}

{ // V-C-V
  std::cout << std::endl << "Testing variable-constant-variable" << std::endl;

  ORB_SLAM::ConstraintGraph graph;

  ORB_SLAM::BundleAdjustmentOptions options;
  options.projection_loss_type = ORB_SLAM::BundleAdjustmentOptions::LossFunctionType::HUBER;
  options.projection_loss_scale = std::sqrt(5.991);
  options.compute_errors = false;
  options.print_summary = true;
  options.solver_options.max_num_iterations = 20;
  options.solver_options.minimizer_progress_to_stdout = true;

  ORB_SLAM::BundleAdjustmentConfig config;

  // Groundtruth
  Eigen::Quaterniond q1 = Eigen::AngleAxisd(colmap::RandomReal<double>(-EIGEN_PI / 2.0, EIGEN_PI / 2.0), Eigen::Vector3d::UnitX())
                         * Eigen::AngleAxisd(colmap::RandomReal<double>(-EIGEN_PI / 2.0, EIGEN_PI / 2.0), Eigen::Vector3d::UnitY())
                         * Eigen::AngleAxisd(colmap::RandomReal<double>(-EIGEN_PI / 2.0, EIGEN_PI / 2.0), Eigen::Vector3d::UnitZ());

  Eigen::Vector4d q1_vec(q1.w(), q1.x(), q1.y(), q1.z());
  Eigen::Vector3d t1_vec(colmap::RandomReal<double>(-1.0, 1.0),
                         colmap::RandomReal<double>(-1.0, 1.0),
                         colmap::RandomReal<double>(-1.0, 1.0));

  Eigen::Quaterniond q2 = Eigen::AngleAxisd(colmap::RandomReal<double>(-EIGEN_PI / 2.0, EIGEN_PI / 2.0), Eigen::Vector3d::UnitX())
                         * Eigen::AngleAxisd(colmap::RandomReal<double>(-EIGEN_PI / 2.0, EIGEN_PI / 2.0), Eigen::Vector3d::UnitY())
                         * Eigen::AngleAxisd(colmap::RandomReal<double>(-EIGEN_PI / 2.0, EIGEN_PI / 2.0), Eigen::Vector3d::UnitZ());

  Eigen::Vector4d q2_vec(q2.w(), q2.x(), q2.y(), q2.z());
  Eigen::Vector3d t2_vec(colmap::RandomReal<double>(-1.0, 1.0),
                         colmap::RandomReal<double>(-1.0, 1.0),
                         colmap::RandomReal<double>(-1.0, 1.0));

  ORB_SLAM::pose_t pose_a(t1_vec.x(), t1_vec.y(), t1_vec.z(), q1.w(), q1.x(), q1.y(), q1.z());
  ORB_SLAM::pose_t pose_b; // Identity
  ORB_SLAM::pose_t pose_c(t2_vec.x(), t2_vec.y(), t2_vec.z(), q2.w(), q2.x(), q2.y(), q2.z());

  ORB_SLAM::pose_t p_ba = pose_b.inverse() * pose_a;
  Eigen::Vector4d qba_vec(p_ba.qw, p_ba.qx, p_ba.qy, p_ba.qz);
  Eigen::Vector3d tba_vec(p_ba.tx, p_ba.ty, p_ba.tz);

  ORB_SLAM::pose_t p_cb = pose_c.inverse() * pose_b;
  Eigen::Vector4d qcb_vec(p_cb.qw, p_cb.qx, p_cb.qy, p_cb.qz);
  Eigen::Vector3d tcb_vec(p_cb.tx, p_cb.ty, p_cb.tz);

  graph.AddPose(0, ORB_SLAM::SE3Quat::Identity()); // A - variable

  graph.AddPose(1, ORB_SLAM::SE3Quat::Identity()); // B - constant
  config.SetConstantPose(1);

  graph.AddPose(2, ORB_SLAM::SE3Quat::Identity()); // C - variable

{
  ORB_SLAM::PoseConstraint constraint(0, 1);

  ORB_SLAM::SE3Quat relative_pose(qba_vec, tba_vec);
  Eigen::Vector6d weight;
  weight << 1.0, 1.0, 1.0, 1.0, 1.0, 1.0;

  constraint.SetMeasurement(relative_pose);
  constraint.SetWeight(weight);

  graph.AddPoseConstraint(0, 1, constraint);
}

{
  ORB_SLAM::PoseConstraint constraint(1, 2);

  ORB_SLAM::SE3Quat relative_pose(qcb_vec, tcb_vec);
  Eigen::Vector6d weight;
  weight << 1.0, 1.0, 1.0, 1.0, 1.0, 1.0;

  constraint.SetMeasurement(relative_pose);
  constraint.SetWeight(weight);

  graph.AddPoseConstraint(1, 2, constraint);
}

  ORB_SLAM::BundleAdjuster ba(options, config);
  ba.Solve(&graph);

  std::cout << "Goal:" << std::endl;
  std::cout << q1_vec.transpose() << std::endl;
  std::cout << t1_vec.transpose() << std::endl;
  std::cout << q2_vec.transpose() << std::endl;
  std::cout << t2_vec.transpose() << std::endl;

  std::cout << "Estimated:" << std::endl;
  ORB_SLAM::SE3Quat estimated1 = graph.GetPose(0);
  ORB_SLAM::pose_t p1(estimated1.Tvec(0), estimated1.Tvec(1), estimated1.Tvec(2),
                      estimated1.Qvec(0), estimated1.Qvec(1), estimated1.Qvec(2), estimated1.Qvec(3));

  p1 = p1.inverse();
  std::cout << p1.qw << " " << p1.qx << " " << p1.qy << " " << p1.qz << std::endl;
  std::cout << p1.tx << " " << p1.ty << " " << p1.tz << std::endl;

  ORB_SLAM::SE3Quat estimated2 = graph.GetPose(2);
  ORB_SLAM::pose_t p2(estimated2.Tvec(0), estimated2.Tvec(1), estimated2.Tvec(2),
                      estimated2.Qvec(0), estimated2.Qvec(1), estimated2.Qvec(2), estimated2.Qvec(3));

  p2 = p2.inverse();
  std::cout << p2.qw << " " << p2.qx << " " << p2.qy << " " << p2.qz << std::endl;
  std::cout << p2.tx << " " << p2.ty << " " << p2.tz << std::endl;
}

{ // V-V-C
  std::cout << std::endl << "Testing variable-variable-constant" << std::endl;

  ORB_SLAM::ConstraintGraph graph;

  ORB_SLAM::BundleAdjustmentOptions options;
  options.projection_loss_type = ORB_SLAM::BundleAdjustmentOptions::LossFunctionType::HUBER;
  options.projection_loss_scale = std::sqrt(5.991);
  options.compute_errors = false;
  options.print_summary = true;
  options.solver_options.max_num_iterations = 20;
  options.solver_options.minimizer_progress_to_stdout = true;

  ORB_SLAM::BundleAdjustmentConfig config;

  // Groundtruth
  Eigen::Quaterniond q1 = Eigen::AngleAxisd(colmap::RandomReal<double>(-EIGEN_PI / 2.0, EIGEN_PI / 2.0), Eigen::Vector3d::UnitX())
                         * Eigen::AngleAxisd(colmap::RandomReal<double>(-EIGEN_PI / 2.0, EIGEN_PI / 2.0), Eigen::Vector3d::UnitY())
                         * Eigen::AngleAxisd(colmap::RandomReal<double>(-EIGEN_PI / 2.0, EIGEN_PI / 2.0), Eigen::Vector3d::UnitZ());

  Eigen::Vector4d q1_vec(q1.w(), q1.x(), q1.y(), q1.z());
  Eigen::Vector3d t1_vec(colmap::RandomReal<double>(-1.0, 1.0),
                         colmap::RandomReal<double>(-1.0, 1.0),
                         colmap::RandomReal<double>(-1.0, 1.0));

  Eigen::Quaterniond q2 = Eigen::AngleAxisd(colmap::RandomReal<double>(-EIGEN_PI / 2.0, EIGEN_PI / 2.0), Eigen::Vector3d::UnitX())
                         * Eigen::AngleAxisd(colmap::RandomReal<double>(-EIGEN_PI / 2.0, EIGEN_PI / 2.0), Eigen::Vector3d::UnitY())
                         * Eigen::AngleAxisd(colmap::RandomReal<double>(-EIGEN_PI / 2.0, EIGEN_PI / 2.0), Eigen::Vector3d::UnitZ());

  Eigen::Vector4d q2_vec(q2.w(), q2.x(), q2.y(), q2.z());
  Eigen::Vector3d t2_vec(colmap::RandomReal<double>(-1.0, 1.0),
                         colmap::RandomReal<double>(-1.0, 1.0),
                         colmap::RandomReal<double>(-1.0, 1.0));

  ORB_SLAM::pose_t pose_a(t1_vec.x(), t1_vec.y(), t1_vec.z(), q1.w(), q1.x(), q1.y(), q1.z());
  ORB_SLAM::pose_t pose_b(t2_vec.x(), t2_vec.y(), t2_vec.z(), q2.w(), q2.x(), q2.y(), q2.z());
  ORB_SLAM::pose_t pose_c; // Identity

  ORB_SLAM::pose_t p_ba = pose_b.inverse() * pose_a;
  Eigen::Vector4d qba_vec(p_ba.qw, p_ba.qx, p_ba.qy, p_ba.qz);
  Eigen::Vector3d tba_vec(p_ba.tx, p_ba.ty, p_ba.tz);

  ORB_SLAM::pose_t p_cb = pose_c.inverse() * pose_b;
  Eigen::Vector4d qcb_vec(p_cb.qw, p_cb.qx, p_cb.qy, p_cb.qz);
  Eigen::Vector3d tcb_vec(p_cb.tx, p_cb.ty, p_cb.tz);

  graph.AddPose(0, ORB_SLAM::SE3Quat::Identity()); // A - variable

  graph.AddPose(1, ORB_SLAM::SE3Quat::Identity()); // B - variable

  graph.AddPose(2, ORB_SLAM::SE3Quat::Identity()); // C - constant
  config.SetConstantPose(2);

{
  ORB_SLAM::PoseConstraint constraint(0, 1);

  ORB_SLAM::SE3Quat relative_pose(qba_vec, tba_vec);
  Eigen::Vector6d weight;
  weight << 1.0, 1.0, 1.0, 1.0, 1.0, 1.0;

  constraint.SetMeasurement(relative_pose);
  constraint.SetWeight(weight);

  graph.AddPoseConstraint(0, 1, constraint);
}

{
  ORB_SLAM::PoseConstraint constraint(1, 2);

  ORB_SLAM::SE3Quat relative_pose(qcb_vec, tcb_vec);
  Eigen::Vector6d weight;
  weight << 1.0, 1.0, 1.0, 1.0, 1.0, 1.0;

  constraint.SetMeasurement(relative_pose);
  constraint.SetWeight(weight);

  graph.AddPoseConstraint(1, 2, constraint);
}

  ORB_SLAM::BundleAdjuster ba(options, config);
  ba.Solve(&graph);

  std::cout << "Goal:" << std::endl;
  std::cout << q1_vec.transpose() << std::endl;
  std::cout << t1_vec.transpose() << std::endl;
  std::cout << q2_vec.transpose() << std::endl;
  std::cout << t2_vec.transpose() << std::endl;

  std::cout << "Estimated:" << std::endl;
  ORB_SLAM::SE3Quat estimated1 = graph.GetPose(0);
  ORB_SLAM::pose_t p1(estimated1.Tvec(0), estimated1.Tvec(1), estimated1.Tvec(2),
                      estimated1.Qvec(0), estimated1.Qvec(1), estimated1.Qvec(2), estimated1.Qvec(3));

  p1 = p1.inverse();
  std::cout << p1.qw << " " << p1.qx << " " << p1.qy << " " << p1.qz << std::endl;
  std::cout << p1.tx << " " << p1.ty << " " << p1.tz << std::endl;

  ORB_SLAM::SE3Quat estimated2 = graph.GetPose(1);
  ORB_SLAM::pose_t p2(estimated2.Tvec(0), estimated2.Tvec(1), estimated2.Tvec(2),
                      estimated2.Qvec(0), estimated2.Qvec(1), estimated2.Qvec(2), estimated2.Qvec(3));

  p2 = p2.inverse();
  std::cout << p2.qw << " " << p2.qx << " " << p2.qy << " " << p2.qz << std::endl;
  std::cout << p2.tx << " " << p2.ty << " " << p2.tz << std::endl;
}

  return 0;
}
