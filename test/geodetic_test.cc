
#include <ios>
#include <iomanip>
#include <iostream>

#include "geodetic_converter.h"

//

int main() {

  GeodeticConverter converter;
  std::cout << std::fixed << std::setprecision(6);
  
  converter.initialiseReference(36.715895, -4.476959, 300);
  
  double lat, lon, alt;
  double x, x_, y, y_, z, z_;
  double north, east, down;
  
  lat = 36.737959;
  lon = -4.457934;
  alt = 600.0;
  converter.geodetic2Ecef(lat, lon, alt, &x, &y, &z);
  converter.ecef2Ned(x, y, z, &north, &east, &down);
  converter.ned2Ecef(north, east, down, &x_, &y_, &z_);
  
  std::cout << "NED:" << std::endl;
  std::cout << north << " " << east << " " << down << std::endl;

  std::cout << "Expected:" << std::endl;
  std::cout << x << " " << y << " " << z << std::endl;
  
  std::cout << "Got:" << std::endl;
  std::cout << x_ << " " << y_ << " " << z_ << std::endl;
  std::cout << std::endl;
  
  lat = 36.682678;
  lon = -4.500467;
  alt = 0.0;
  converter.geodetic2Ecef(lat, lon, alt, &x, &y, &z);
  converter.ecef2Ned(x, y, z, &north, &east, &down);
  converter.ned2Ecef(north, east, down, &x_, &y_, &z_);

  std::cout << "Expected:" << std::endl;
  std::cout << x << " " << y << " " << z << std::endl;
  
  std::cout << "Got:" << std::endl;
  std::cout << x_ << " " << y_ << " " << z_ << std::endl;
  std::cout << std::endl;

  return 0;
}

// 36.715895, -4.476959, 300

// 36.737959, -4.457934, 600

// 36.682678, -4.500467, 0
