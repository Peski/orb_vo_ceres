
#include <iostream>

#include <Eigen/Geometry>

#include "colmap/util/random.h"

#include "bundle_adjustment.h"
#include "INS.h"
#include "SE3Quat.h"

int main() {

  std::cout << "Testing Optimization" << std::endl;
  std::cout << "--------------------" << std::endl;

  ORB_SLAM::ConstraintGraph graph;

  ORB_SLAM::BundleAdjustmentOptions options;
  options.projection_loss_type = ORB_SLAM::BundleAdjustmentOptions::LossFunctionType::HUBER;
  options.projection_loss_scale = std::sqrt(5.991);
  options.compute_errors = false;
  options.print_summary = true;
  options.solver_options.max_num_iterations = 20;
  options.solver_options.minimizer_progress_to_stdout = true;

  ORB_SLAM::BundleAdjustmentConfig config;

  // Groundtruth
  Eigen::Quaterniond q = Eigen::AngleAxisd(colmap::RandomReal<double>(-EIGEN_PI / 2.0, EIGEN_PI / 2.0), Eigen::Vector3d::UnitX())
                         * Eigen::AngleAxisd(colmap::RandomReal<double>(-EIGEN_PI / 2.0, EIGEN_PI / 2.0), Eigen::Vector3d::UnitY())
                         * Eigen::AngleAxisd(colmap::RandomReal<double>(-EIGEN_PI / 2.0, EIGEN_PI / 2.0), Eigen::Vector3d::UnitZ());

  Eigen::Vector4d q_vec(q.w(), q.x(), q.y(), q.z());
  Eigen::Vector3d t_vec(colmap::RandomReal<double>(-1.0, 1.0),
                        colmap::RandomReal<double>(-1.0, 1.0),
                        colmap::RandomReal<double>(-1.0, 1.0));

  ORB_SLAM::SE3Quat pose_a; // Identity
  ORB_SLAM::SE3Quat pose_b(t_vec.x(), t_vec.y(), t_vec.z(), q.w(), q.x(), q.y(), q.z());

  ORB_SLAM::SE3Quat p_ba = pose_b.inverse() * pose_a;
  Eigen::Vector4d qba_vec(p_ba.q[0], p_ba.q[1], p_ba.q[2], p_ba.q[3]);
  Eigen::Vector3d tba_vec(p_ba.t[0], p_ba.t[1], p_ba.t[2]);

  graph.AddPose(0, ORB_SLAM::SE3Quat::Identity()); // A - constant
  config.SetConstantPose(0);

  graph.AddPose(1, ORB_SLAM::SE3Quat::Identity()); // B - variable

  ORB_SLAM::PoseConstraint constraint(0, 1);

  ORB_SLAM::SE3Quat relative_pose(tba_vec, qba_vec);
  Eigen::Matrix6d weight = Eigen::Matrix6d::Identity();

  constraint.SetMeasurement(relative_pose);
  constraint.SetWeight(weight);

  graph.AddPoseConstraint(0, 1, constraint);

  ORB_SLAM::BundleAdjuster ba(options, config);
  ba.Solve(&graph);

  std::cout << "Goal:" << std::endl;
  std::cout << q_vec.transpose() << std::endl;
  std::cout << t_vec.transpose() << std::endl;

  std::cout << "Estimated:" << std::endl;
  ORB_SLAM::SE3Quat estimated = graph.GetPose(1);
  ORB_SLAM::SE3Quat p(estimated.t[0], estimated.t[1], estimated.t[2],
                     estimated.q[0], estimated.q[1], estimated.q[2], estimated.q[3]);

  p = p.inverse();
  std::cout << p.q[0] << " " << p.q[1] << " " << p.q[2] << " " << p.q[3] << std::endl;
  std::cout << p.t[0] << " " << p.t[1] << " " << p.t[2] << std::endl;

  return 0;
}
