
#include "bundle_adjustment.h"

#include <algorithm>
#include <limits>
#include <map>
#include <iomanip>
#include <utility>
#include <vector>

#ifdef OPENMP_ENABLED
#include <omp.h>
#endif

#include <ceres/ceres.h>
#include <ceres/rotation.h>

#include <colmap/base/pose.h>
#include <colmap/util/logging.h>
#include <colmap/util/math.h>
#include <colmap/util/misc.h>
#include <colmap/util/threading.h>

/*
#include "base/camera_models.h"
#include "base/cost_functions.h"
#include "base/projection.h"
#include "util/misc.h"
#include "util/threading.h"
#include "util/timer.h"
*/

#include <iostream>

// TODO Implement PoseConstraint::UpdateChi2()

namespace ORB_SLAM {

// Standard bundle adjustment cost function for variable
// camera pose and point parameters.
class BundleAdjustmentCostFunction {
 public:
  explicit BundleAdjustmentCostFunction(const Eigen::Vector4d& pinhole_params, const Eigen::Vector2d& point2D, const Eigen::Vector2d& weights)
      : fx_(pinhole_params(0)),
        fy_(pinhole_params(1)),
        cx_(pinhole_params(2)),
        cy_(pinhole_params(3)),
        observed_x_(point2D(0)),
        observed_y_(point2D(1)),
        weight_x_(weights(0)),
        weight_y_(weights(1))
  { }

  static ceres::CostFunction* Create(const Eigen::Vector4d& pinhole_params, const Eigen::Vector2d& point2D, const Eigen::Vector2d& weights) {
    return (new ceres::AutoDiffCostFunction<BundleAdjustmentCostFunction, 2, 4, 3, 3>(
              new BundleAdjustmentCostFunction(pinhole_params, point2D, weights))
            );
  }

  template <typename T>
  bool operator()(const T* const qvec, const T* const tvec, const T* const point3D,
                  T* residuals) const {
    // Rotate and translate
    T projection[3];
    ceres::UnitQuaternionRotatePoint(qvec, point3D, projection);
    projection[0] += tvec[0];
    projection[1] += tvec[1];
    projection[2] += tvec[2];

    // Project to image plane
    projection[0] /= projection[2];
    projection[1] /= projection[2];

    // Transform to image coordinates (pinhole model)
    residuals[0] = T(fx_) * projection[0] + T(cx_);
    residuals[1] = T(fy_) * projection[1] + T(cy_);

    // Re-projection error
    residuals[0] -= T(observed_x_);
    residuals[1] -= T(observed_y_);

    // Apply weights
    residuals[0] *= T(weight_x_);
    residuals[1] *= T(weight_y_);

    return true;
  }

 private:
  const double fx_, fy_;
  const double cx_, cy_;
  const double observed_x_, observed_y_;
  const double weight_x_, weight_y_;
};

// Bundle adjustment cost function for fixed
// camera pose and variable point parameters.
class BundleAdjustmentConstantPoseCostFunction {
 public:
  BundleAdjustmentConstantPoseCostFunction(const Eigen::Vector4d& qvec,
                                           const Eigen::Vector3d& tvec,
                                           const Eigen::Vector4d& pinhole_params,
                                           const Eigen::Vector2d& point2D,
                                           const Eigen::Vector2d& weights)
      : qw_(qvec(0)),
        qx_(qvec(1)),
        qy_(qvec(2)),
        qz_(qvec(3)),
        tx_(tvec(0)),
        ty_(tvec(1)),
        tz_(tvec(2)),
        fx_(pinhole_params(0)),
        fy_(pinhole_params(1)),
        cx_(pinhole_params(2)),
        cy_(pinhole_params(3)),
        observed_x_(point2D(0)),
        observed_y_(point2D(1)),
        weight_x_(weights(0)),
        weight_y_(weights(1))
  { }

  static ceres::CostFunction* Create(const Eigen::Vector4d& qvec,
                                     const Eigen::Vector3d& tvec,
                                     const Eigen::Vector4d& pinhole_params,
                                     const Eigen::Vector2d& point2D,
                                     const Eigen::Vector2d& weights) {
    return (new ceres::AutoDiffCostFunction<BundleAdjustmentConstantPoseCostFunction, 2, 3>(
              new BundleAdjustmentConstantPoseCostFunction(qvec, tvec, pinhole_params, point2D, weights))
            );
  }

  template <typename T>
  bool operator()(const T* const point3D, T* residuals) const {
    const T qvec[4] = {T(qw_), T(qx_), T(qy_), T(qz_)};

    // Rotate and translate
    T projection[3];
    ceres::UnitQuaternionRotatePoint(qvec, point3D, projection);
    projection[0] += T(tx_);
    projection[1] += T(ty_);
    projection[2] += T(tz_);

    // Project to image plane
    projection[0] /= projection[2];
    projection[1] /= projection[2];

    // Transform to image coordinates (pinhole model)
    residuals[0] = T(fx_) * projection[0] + T(cx_);
    residuals[1] = T(fy_) * projection[1] + T(cy_);

    // Re-projection error
    residuals[0] -= T(observed_x_);
    residuals[1] -= T(observed_y_);

    // Apply weights
    residuals[0] *= T(weight_x_);
    residuals[1] *= T(weight_y_);

    return true;
  }

 private:
  const double qw_, qx_, qy_, qz_;
  const double tx_, ty_, tz_;
  const double fx_, fy_;
  const double cx_, cy_;
  const double observed_x_, observed_y_;
  const double weight_x_, weight_y_;
};

// Relative pose cost function, with inverse pose parametrization
class RelativePoseCostFunction {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  
  RelativePoseCostFunction(const Eigen::Vector4d& qba_vec,
                           const Eigen::Vector3d& tba_vec,
                           const Eigen::Matrix6d& sqrt_information)
      : qba_w_(qba_vec(0)),
        qba_x_(qba_vec(1)),
        qba_y_(qba_vec(2)),
        qba_z_(qba_vec(3)),
        tba_x_(tba_vec(0)),
        tba_y_(tba_vec(1)),
        tba_z_(tba_vec(2)),
        sqrt_information_(sqrt_information)
  { }

  static ceres::CostFunction* Create(const Eigen::Vector4d& qba_vec,
                                     const Eigen::Vector3d& tba_vec,
                                     const Eigen::Matrix6d& sqrt_information) {
    return (new ceres::AutoDiffCostFunction<RelativePoseCostFunction, 6, 4, 3, 4, 3>(
              new RelativePoseCostFunction(qba_vec, tba_vec, sqrt_information))
            );
  }

  template <typename T>
  bool operator()(const T* const qa, const T* const ta, const T* const qb, const T* const tb, T* residuals) const {
    const T qa_inv[4] = {qa[0], T(-1.0) * qa[1], T(-1.0) * qa[2], T(-1.0) * qa[3]};

    T qba_estimated[4];
    ceres::QuaternionProduct(qb, qa_inv, qba_estimated);

    const T ta_inv[3] = {T(-1.0) * ta[0], T(-1.0) * ta[1], T(-1.0) * ta[2]};

    T tba_estimated[3];
    ceres::QuaternionRotatePoint(qba_estimated, ta_inv, tba_estimated);
    tba_estimated[0] += tb[0];
    tba_estimated[1] += tb[1];
    tba_estimated[2] += tb[2];

    const T qba_inv[4] = {qba_estimated[0], T(-1.0) * qba_estimated[1], T(-1.0) * qba_estimated[2], T(-1.0) * qba_estimated[3]};

    T dq[4];
    const T qba[4] = {T(qba_w_), T(qba_x_), T(qba_y_), T(qba_z_)};
    ceres::QuaternionProduct(qba, qba_inv, dq);

    const T tba[3] = {T(tba_x_), T(tba_y_), T(tba_z_)};

    residuals[0] = tba_estimated[0] - tba[0];
    residuals[1] = tba_estimated[1] - tba[1];
    residuals[2] = tba_estimated[2] - tba[2];
    residuals[3] = T(2.0) * dq[1];
    residuals[4] = T(2.0) * dq[2];
    residuals[5] = T(2.0) * dq[3];

    // Scale the residuals by the measurement uncertainty.
    Eigen::Map<Eigen::Matrix<T, 6, 1> > residuals_map(residuals);
    residuals_map.applyOnTheLeft(sqrt_information_.template cast<T>());

    return true;
  }

 private:
  const double qba_w_, qba_x_, qba_y_, qba_z_;
  const double tba_x_, tba_y_, tba_z_;
  Eigen::Matrix6d sqrt_information_;
};

// Relative pose cost function with constant A pose and inverse pose parametrization
class RelativePoseConstantACostFunction {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  
  RelativePoseConstantACostFunction(const Eigen::Vector4d& qba_vec,
                                    const Eigen::Vector3d& tba_vec,
                                    const Eigen::Vector4d& qa_vec,
                                    const Eigen::Vector3d& ta_vec,
                                    const Eigen::Matrix6d& sqrt_information)
      : qba_w_(qba_vec(0)),
        qba_x_(qba_vec(1)),
        qba_y_(qba_vec(2)),
        qba_z_(qba_vec(3)),
        tba_x_(tba_vec(0)),
        tba_y_(tba_vec(1)),
        tba_z_(tba_vec(2)),
        qa_w_(qa_vec(0)),
        qa_x_(qa_vec(1)),
        qa_y_(qa_vec(2)),
        qa_z_(qa_vec(3)),
        ta_x_(ta_vec(0)),
        ta_y_(ta_vec(1)),
        ta_z_(ta_vec(2)),
        sqrt_information_(sqrt_information)
  { }

  static ceres::CostFunction* Create(const Eigen::Vector4d& qba_vec,
                                     const Eigen::Vector3d& tba_vec,
                                     const Eigen::Vector4d& qa_vec,
                                     const Eigen::Vector3d& ta_vec,
                                     const Eigen::Matrix6d& sqrt_information) {
    return (new ceres::AutoDiffCostFunction<RelativePoseConstantACostFunction, 6, 4, 3>(
              new RelativePoseConstantACostFunction(qba_vec, tba_vec, qa_vec, ta_vec, sqrt_information))
            );
  }

  template <typename T>
  bool operator()(const T* const qb, const T* const tb, T* residuals) const {
    const T qa_inv[4] = {T(qa_w_), T(-1.0 * qa_x_), T(-1.0 * qa_y_), T(-1.0 * qa_z_)};

    T qba_estimated[4];
    ceres::QuaternionProduct(qb, qa_inv, qba_estimated);

    const T ta_inv[3] = {T(-1.0 * ta_x_), T(-1.0 * ta_y_), T(-1.0 * ta_z_)};

    T tba_estimated[3];
    ceres::QuaternionRotatePoint(qba_estimated, ta_inv, tba_estimated);
    tba_estimated[0] += tb[0];
    tba_estimated[1] += tb[1];
    tba_estimated[2] += tb[2];

    const T qba_inv[4] = {qba_estimated[0], T(-1.0) * qba_estimated[1], T(-1.0) * qba_estimated[2], T(-1.0) * qba_estimated[3]};

    T dq[4];
    const T qba[4] = {T(qba_w_), T(qba_x_), T(qba_y_), T(qba_z_)};
    ceres::QuaternionProduct(qba, qba_inv, dq);

    const T tba[3] = {T(tba_x_), T(tba_y_), T(tba_z_)};

    residuals[0] = tba_estimated[0] - tba[0];
    residuals[1] = tba_estimated[1] - tba[1];
    residuals[2] = tba_estimated[2] - tba[2];
    residuals[3] = T(2.0) * dq[1];
    residuals[4] = T(2.0) * dq[2];
    residuals[5] = T(2.0) * dq[3];

    // Scale the residuals by the measurement uncertainty.
    Eigen::Map<Eigen::Matrix<T, 6, 1> > residuals_map(residuals);
    residuals_map.applyOnTheLeft(sqrt_information_.template cast<T>());

    return true;
  }

 private:
  const double qba_w_, qba_x_, qba_y_, qba_z_;
  const double tba_x_, tba_y_, tba_z_;
  const double qa_w_, qa_x_, qa_y_, qa_z_;
  const double ta_x_, ta_y_, ta_z_;
  Eigen::Matrix6d sqrt_information_;
};

bool HasPointPositiveDepth(const Point3D& point, const SE3Quat& pose) {
  // Rotate and translate
  Eigen::Vector4d q_vec(pose.q[0], pose.q[1], pose.q[2], pose.q[3]);
  Eigen::Vector3d t_vec(pose.t[0], pose.t[1], pose.t[2]);

  Eigen::Vector3d projection = colmap::QuaternionRotatePoint(q_vec, point.XYZ());
  projection += t_vec;

  return (projection(2) > std::numeric_limits<double>::epsilon());
}

////////////////////////////////////////////////////////////////////////////////
// Point3D
////////////////////////////////////////////////////////////////////////////////

Point3D::Point3D() {}

Point3D::Point3D(const Eigen::Vector3d& xyz)
  : xyz_(xyz) {}

const Eigen::Vector3d& Point3D::XYZ() const { return xyz_; }

Eigen::Vector3d& Point3D::XYZ() { return xyz_; }

double Point3D::XYZ(const size_t idx) const { return xyz_(idx); }

double& Point3D::XYZ(const size_t idx) { return xyz_(idx); }

void Point3D::SetXYZ(const Eigen::Vector3d& xyz) { xyz_ = xyz; }

////////////////////////////////////////////////////////////////////////////////
// ProjectionConstraint
////////////////////////////////////////////////////////////////////////////////

ProjectionConstraint::ProjectionConstraint()
  : point_id_(std::numeric_limits<id_t>::max()), pose_id_(std::numeric_limits<id_t>::max()),
    level_(0), chi2_(std::numeric_limits<double>::quiet_NaN()),
    fx_(std::numeric_limits<double>::quiet_NaN()), fy_(std::numeric_limits<double>::quiet_NaN()),
    cx_(std::numeric_limits<double>::quiet_NaN()), cy_(std::numeric_limits<double>::quiet_NaN()),
    measurement_(Eigen::Vector2d(std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN())),
    weight_(Eigen::Vector2d(1.0, 1.0))
  { }

ProjectionConstraint::ProjectionConstraint(const id_t point_id, const id_t pose_id)
  : point_id_(point_id), pose_id_(pose_id),
    level_(0), chi2_(std::numeric_limits<double>::quiet_NaN()),
    fx_(std::numeric_limits<double>::quiet_NaN()), fy_(std::numeric_limits<double>::quiet_NaN()),
    cx_(std::numeric_limits<double>::quiet_NaN()), cy_(std::numeric_limits<double>::quiet_NaN()),
    measurement_(Eigen::Vector2d(std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN())),
    weight_(Eigen::Vector2d(1.0, 1.0))
  { }

id_t ProjectionConstraint::GetPointId() const { return point_id_; }
id_t ProjectionConstraint::GetPoseId() const { return pose_id_; }

int& ProjectionConstraint::Level() { return level_; }
const int& ProjectionConstraint::Level() const{ return level_; }
void ProjectionConstraint::SetLevel(const int level) { level_ = level; }

double& ProjectionConstraint::Chi2() { return chi2_; }
const double& ProjectionConstraint::Chi2() const { return chi2_; }

void ProjectionConstraint::UpdateChi2(const Point3D& point, const SE3Quat& pose) {
  // Rotate and translate
  Eigen::Vector4d q_vec(pose.q[0], pose.q[1], pose.q[2], pose.q[3]);
  Eigen::Vector3d t_vec(pose.t[0], pose.t[1], pose.t[2]);
  Eigen::Vector3d projection = colmap::QuaternionRotatePoint(q_vec, point.XYZ());
  projection += t_vec;

  // Project to image plane
  Eigen::Vector2d residuals(projection(0) / projection(2), projection(1) / projection(2));

  // Transform to image coordinates (pinhole model)
  residuals(0) = fx_ * residuals(0) + cx_;
  residuals(1) = fy_ * residuals(1) + cy_;

  // Re-projection error
  residuals(0) -= measurement_(0);
  residuals(1) -= measurement_(1);

  // Apply weights
  residuals(0) *= weight_(0);
  residuals(1) *= weight_(1);

  chi2_ = residuals(0)*residuals(0) + residuals(1)*residuals(1);
}
  
Eigen::Vector2d ProjectionConstraint::Weight() const { return weight_; }
void ProjectionConstraint::SetWeight(const Eigen::Vector2d& weight) { weight_ = weight; }

Eigen::Vector2d ProjectionConstraint::Measurement() const { return measurement_; }
void ProjectionConstraint::SetMeasurement(const Eigen::Vector2d& measurement) { measurement_ = measurement; }

bool ProjectionConstraint::Check() const {
  CHECK_OPTION_NE(point_id_, std::numeric_limits<id_t>::max());
  CHECK_OPTION_NE(pose_id_, std::numeric_limits<id_t>::max());

  CHECK_OPTION(!colmap::IsNaN(fx_));
  CHECK_OPTION(!colmap::IsNaN(fy_));
  CHECK_OPTION(!colmap::IsNaN(cx_));
  CHECK_OPTION(!colmap::IsNaN(cy_));

  CHECK_OPTION(!colmap::IsNaN(measurement_(0)));
  CHECK_OPTION(!colmap::IsNaN(measurement_(1)));

  CHECK_OPTION_GE(weight_(0), 0.0);
  CHECK_OPTION_GE(weight_(1), 0.0);

  return true;
}

double& ProjectionConstraint::fx() { return fx_; }
const double& ProjectionConstraint::fx() const { return fx_; }

double& ProjectionConstraint::fy() { return fy_; }
const double& ProjectionConstraint::fy() const { return fy_; }

double& ProjectionConstraint::cx() { return cx_; }
const double& ProjectionConstraint::cx() const { return cx_; }

double& ProjectionConstraint::cy() { return cy_; }
const double& ProjectionConstraint::cy() const { return cy_; }

////////////////////////////////////////////////////////////////////////////////
// PoseConstraint
////////////////////////////////////////////////////////////////////////////////

PoseConstraint::PoseConstraint()
  : pose_a_id_(std::numeric_limits<id_t>::max()), pose_b_id_(std::numeric_limits<id_t>::max()),
    level_(0), chi2_(std::numeric_limits<double>::quiet_NaN())
  {
    for (int i = 0; i < 4; ++i) {
      relative_pose_.q[i] = std::numeric_limits<double>::quiet_NaN();
    }

    for (int i = 0; i < 3; ++i) {
      relative_pose_.t[i] = std::numeric_limits<double>::quiet_NaN();
    }

    weight_ = Eigen::Matrix6d::Identity();
  }

PoseConstraint::PoseConstraint(const id_t pose_a_id, const id_t pose_b_id)
  : pose_a_id_(pose_a_id), pose_b_id_(pose_b_id),
    level_(0), chi2_(std::numeric_limits<double>::quiet_NaN())
  {
    for (int i = 0; i < 4; ++i) {
      relative_pose_.q[i] = std::numeric_limits<double>::quiet_NaN();
    }

    for (int i = 0; i < 3; ++i) {
      relative_pose_.t[i] = std::numeric_limits<double>::quiet_NaN();
    }

    weight_ = Eigen::Matrix6d::Identity();
  }

id_t PoseConstraint::GetPoseAId() const { return pose_a_id_; }
id_t PoseConstraint::GetPoseBId() const { return pose_b_id_; }

int& PoseConstraint::Level() { return level_; }
const int& PoseConstraint::Level() const{ return level_; }
void PoseConstraint::SetLevel(const int level) { level_ = level; }

/*
double& PoseConstraint::Chi2() { return chi2_; }
const double& PoseConstraint::Chi2() const { return chi2_; }

void PoseConstraint::UpdateChi2(const SE3Quat& pose_a, const SE3Quat& pose_b) {

  Eigen::Vector4d qa_inv = colmap::InvertQuaternion(pose_a.Qvec());

  Eigen::Vector4d qab_estimated = colmap::ConcatenateQuaternions(pose_b.Qvec(), qa_inv);
  Eigen::Vector3d tab_estimated = colmap::QuaternionRotatePoint(qa_inv, pose_b.Tvec() - pose_a.Tvec());

  Eigen::Vector4d dq = colmap::ConcatenateQuaternions(colmap::InvertQuaternion(qab_estimated),
                                                      relative_pose_.Qvec());

  Eigen::Vector6d residuals;
  residuals(0) = 2.0 * dq[1];
  residuals(1) = 2.0 * dq[2];
  residuals(2) = 2.0 * dq[3];
  residuals(3) = tab_estimated(0) - relative_pose_.Tvec(0);
  residuals(4) = tab_estimated(1) - relative_pose_.Tvec(1);
  residuals(5) = tab_estimated(2) - relative_pose_.Tvec(2);

    // Apply weights
  residuals(0) *= weight_(0);
  residuals(1) *= weight_(1);
  residuals(2) *= weight_(2);
  residuals(3) *= weight_(3);
  residuals(4) *= weight_(4);
  residuals(5) *= weight_(5);
}
*/

Eigen::Matrix6d PoseConstraint::Weight() const { return weight_; }

void PoseConstraint::SetWeight(const Eigen::Matrix6d& weight) { weight_ = weight; }

SE3Quat PoseConstraint::Measurement() const { return relative_pose_; }

void PoseConstraint::SetMeasurement(const SE3Quat& measurement) { relative_pose_ = measurement; }

bool PoseConstraint::Check() const {
  CHECK_OPTION_NE(pose_a_id_, std::numeric_limits<id_t>::max());
  CHECK_OPTION_NE(pose_b_id_, std::numeric_limits<id_t>::max());

  for (int i = 0; i < 4; ++i)
    CHECK_OPTION(!colmap::IsNaN(relative_pose_.q[i]));

  for (int i = 0; i < 3; ++i)
    CHECK_OPTION(!colmap::IsNaN(relative_pose_.t[i]));

  // TODO Check weight_ definite positive

  return true;
}

////////////////////////////////////////////////////////////////////////////////
// ConstraintGraph
////////////////////////////////////////////////////////////////////////////////

ConstraintGraph::ConstraintGraph() { }

void ConstraintGraph::AddPoint(const id_t id, const Point3D& point) {
  CHECK(!HasPoint(id));
  points_[id] = point;
}

void ConstraintGraph::AddPose(const id_t id, const SE3Quat& pose) {
  CHECK(!HasPose(id));
  poses_[id] = pose;
}

void ConstraintGraph::AddPoseConstraint(const id_t pose_a_id, const id_t pose_b_id, const PoseConstraint& constraint) {
  pair_id_t pair_id = std::make_pair(pose_a_id, pose_b_id);
  CHECK(!HasPoseConstraint(pair_id));
  pose_constraints_[pair_id] = constraint;
}

void ConstraintGraph::AddPoseConstraint(const pair_id_t pair_id, const PoseConstraint& constraint) {
  CHECK(!HasPoseConstraint(pair_id));
  pose_constraints_[pair_id] = constraint;
}

void ConstraintGraph::AddProjectionConstraint(const id_t point_id, const id_t pose_id, const ProjectionConstraint& constraint) {
  pair_id_t pair_id = std::make_pair(point_id, pose_id);
  CHECK(!HasProjectionConstraint(pair_id));
  projection_constraints_[pair_id] = constraint;
}

void ConstraintGraph::AddProjectionConstraint(const pair_id_t pair_id, const ProjectionConstraint& constraint) {
  CHECK(!HasProjectionConstraint(pair_id));
  projection_constraints_[pair_id] = constraint;
}

void ConstraintGraph::RemovePoseConstraint(const id_t pose_a_id, const id_t pose_b_id) {
  pair_id_t pair_id = std::make_pair(pose_a_id, pose_b_id);
  CHECK(HasPoseConstraint(pair_id));
  pose_constraints_.erase(pair_id);
}

void ConstraintGraph::RemovePoseConstraint(const pair_id_t pair_id) {
  CHECK(HasPoseConstraint(pair_id));
  pose_constraints_.erase(pair_id);
}

void ConstraintGraph::RemoveProjectionConstraint(const id_t point_id, const id_t pose_id) {
  pair_id_t pair_id = std::make_pair(point_id, pose_id);
  CHECK(HasProjectionConstraint(pair_id));
  projection_constraints_.erase(pair_id);
}

void ConstraintGraph::RemoveProjectionConstraint(const pair_id_t pair_id) {
  CHECK(HasProjectionConstraint(pair_id));
  projection_constraints_.erase(pair_id);
}

bool ConstraintGraph::HasPoint(const id_t id) const {
  return points_.find(id) != points_.end();
}

bool ConstraintGraph::HasPose(const id_t id) const {
  return poses_.find(id) != poses_.end();
}

bool ConstraintGraph::HasPoseConstraint(const id_t pose_a_id, const id_t pose_b_id) const {
  return pose_constraints_.find(std::make_pair(pose_a_id, pose_b_id)) != pose_constraints_.end();
}

bool ConstraintGraph::HasPoseConstraint(const pair_id_t pair_id) const {
  return pose_constraints_.find(pair_id) != pose_constraints_.end();
}

bool ConstraintGraph::HasProjectionConstraint(const id_t point_id, const id_t pose_id) const {
  return projection_constraints_.find(std::make_pair(point_id, pose_id)) != projection_constraints_.end();
}

bool ConstraintGraph::HasProjectionConstraint(const pair_id_t pair_id) const {
  return projection_constraints_.find(pair_id) != projection_constraints_.end();
}

PoseConstraint& ConstraintGraph::GetPoseConstraint(const id_t pose_a_id, const id_t pose_b_id) {
  CHECK(HasPoseConstraint(pose_a_id, pose_b_id));
  return pose_constraints_.at(std::make_pair(pose_a_id, pose_b_id));
}

const PoseConstraint& ConstraintGraph::GetPoseConstraint(const id_t pose_a_id, const id_t pose_b_id) const {
  CHECK(HasPoseConstraint(pose_a_id, pose_b_id));
  return pose_constraints_.at(std::make_pair(pose_a_id, pose_b_id));
}

ProjectionConstraint& ConstraintGraph::GetProjectionConstraint(const id_t point_id, const id_t pose_id) {
  CHECK(HasProjectionConstraint(point_id, pose_id));
  return projection_constraints_.at(std::make_pair(point_id, pose_id));
}

const ProjectionConstraint& ConstraintGraph::GetProjectionConstraint(const id_t point_id, const id_t pose_id) const {
  CHECK(HasProjectionConstraint(point_id, pose_id));
  return projection_constraints_.at(std::make_pair(point_id, pose_id));
}

PoseConstraint& ConstraintGraph::GetPoseConstraint(const pair_id_t pair_id) {
  CHECK(HasPoseConstraint(pair_id));
  return pose_constraints_.at(pair_id);
}

const PoseConstraint& ConstraintGraph::GetPoseConstraint(const pair_id_t pair_id) const {
  CHECK(HasPoseConstraint(pair_id));
  return pose_constraints_.at(pair_id);
}

ProjectionConstraint& ConstraintGraph::GetProjectionConstraint(const pair_id_t pair_id) {
  CHECK(HasProjectionConstraint(pair_id));
  return projection_constraints_.at(pair_id);
}

const ProjectionConstraint& ConstraintGraph::GetProjectionConstraint(const pair_id_t pair_id) const {
  CHECK(HasProjectionConstraint(pair_id));
  return projection_constraints_.at(pair_id);
}

Point3D& ConstraintGraph::GetPoint(const id_t id) {
  CHECK(HasPoint(id));
  return points_.at(id);
}

const Point3D& ConstraintGraph::GetPoint(const id_t id) const {
  CHECK(HasPoint(id));
  return points_.at(id);
}

SE3Quat& ConstraintGraph::GetPose(const id_t id) {
  CHECK(HasPose(id));
  return poses_.at(id);
}

const SE3Quat& ConstraintGraph::GetPose(const id_t id) const {
  CHECK(HasPose(id));
  return poses_.at(id);
}

const std::unordered_map<id_t, Point3D>& ConstraintGraph::GetPoints() const { return points_; }

const std::unordered_map<id_t, SE3Quat>& ConstraintGraph::GetPoses() const { return poses_; }

const std::unordered_map<pair_id_t, PoseConstraint>& ConstraintGraph::GetPoseConstraints() const { return pose_constraints_; }

const std::unordered_map<pair_id_t, ProjectionConstraint>& ConstraintGraph::GetProjectionConstraints() const { return projection_constraints_; }

////////////////////////////////////////////////////////////////////////////////
// BundleAdjustmentOptions
////////////////////////////////////////////////////////////////////////////////

ceres::LossFunction* BundleAdjustmentOptions::CreateLossFunction(const LossFunctionType loss_function_type,
                                                                 const double loss_function_scale) {
  ceres::LossFunction* loss_function = nullptr;
  switch (loss_function_type) {
    case LossFunctionType::TRIVIAL:
      loss_function = new ceres::TrivialLoss();
      break;
    case LossFunctionType::HUBER:
      loss_function = new ceres::HuberLoss(loss_function_scale);
      break;
    case LossFunctionType::SOFT_L1:
      loss_function = new ceres::SoftLOneLoss(loss_function_scale);
      break;
    case LossFunctionType::CAUCHY:
      loss_function = new ceres::CauchyLoss(loss_function_scale);
      break;
  }
  CHECK_NOTNULL(loss_function);
  return loss_function;
}

bool BundleAdjustmentOptions::Check() const {
  CHECK_OPTION_GE(pose_loss_scale, 0.0);
  CHECK_OPTION_GE(projection_loss_scale, 0.0);
  CHECK_OPTION_GE(optimization_level, 0);
  return true;
}

////////////////////////////////////////////////////////////////////////////////
// BundleAdjustmentConfig
////////////////////////////////////////////////////////////////////////////////

BundleAdjustmentConfig::BundleAdjustmentConfig() {}

size_t BundleAdjustmentConfig::NumConstantPoses() const {
  return constant_poses_.size();
}

size_t BundleAdjustmentConfig::NumConstantTvecs() const {
  return constant_tvecs_.size();
}

size_t BundleAdjustmentConfig::NumConstantPoints() const {
  return constant_points_.size();
}

void BundleAdjustmentConfig::SetConstantPose(const id_t id) {
  CHECK(!HasConstantTvec(id));
  constant_poses_.insert(id);
}

bool BundleAdjustmentConfig::HasConstantPose(const id_t id) const {
  return constant_poses_.find(id) != constant_poses_.end();
}

void BundleAdjustmentConfig::RemoveConstantPose(const id_t id) {
  constant_poses_.erase(id);
}

void BundleAdjustmentConfig::SetConstantTvec(const id_t id,
                                             const std::vector<int>& idxs) {
  CHECK_GT(idxs.size(), 0);
  CHECK_LE(idxs.size(), 3);
  // CHECK(HasImage(image_id));
  CHECK(!HasConstantPose(id));
  CHECK(!colmap::VectorContainsDuplicateValues(idxs))
      << "Tvec indices must not contain duplicates";
  constant_tvecs_.emplace(id, idxs);
}

bool BundleAdjustmentConfig::HasConstantTvec(const id_t id) const {
  return constant_tvecs_.find(id) != constant_tvecs_.end();
}

void BundleAdjustmentConfig::RemoveConstantTvec(const id_t id) {
  constant_tvecs_.erase(id);
}

void BundleAdjustmentConfig::SetConstantPoint(const id_t id) {
  constant_points_.insert(id);
}

bool BundleAdjustmentConfig::HasConstantPoint(const id_t id) const {
  return constant_points_.find(id) != constant_points_.end();
}

void BundleAdjustmentConfig::RemoveConstantPoint(const id_t id) {
  constant_points_.erase(id);
}

const std::unordered_set<id_t>& BundleAdjustmentConfig::ConstantPoints() const {
  return constant_points_;
}

const std::unordered_set<id_t>& BundleAdjustmentConfig::ConstantPoses() const {
  return constant_poses_;
}

const std::vector<int>& BundleAdjustmentConfig::ConstantTvec(const id_t id) const {
  CHECK(HasConstantTvec(id));
  return constant_tvecs_.at(id);
}

const std::unordered_map<id_t, std::vector<int>>& BundleAdjustmentConfig::ConstantTvecs() const {
  return constant_tvecs_;
}

////////////////////////////////////////////////////////////////////////////////
// BundleAdjuster
////////////////////////////////////////////////////////////////////////////////

BundleAdjuster::BundleAdjuster(const BundleAdjustmentOptions& options,
                               const BundleAdjustmentConfig& config)
    : options_(options), config_(config) {
  CHECK(options_.Check());
}

bool BundleAdjuster::Solve(ConstraintGraph* graph) {
  CHECK_NOTNULL(graph);
  CHECK(!problem_) << "Cannot use the same BundleAdjuster multiple times";

  problem_.reset(new ceres::Problem());

  SetUp(graph);

  if (problem_->NumResiduals() == 0) {
    return false;
  }

  ceres::Solver::Options solver_options = options_.solver_options;

  // Empirical choice.
  const size_t kMaxNumPosesDirectDenseSolver = 50;
  const size_t kMaxNumPosesDirectSparseSolver = 1000;
  const size_t num_poses = graph->GetPoses().size();
  // TODO Change linear solver type when addition constrainst are actived
  if (num_poses <= kMaxNumPosesDirectDenseSolver) {
    solver_options.linear_solver_type = ceres::DENSE_SCHUR;
  } else if (num_poses <= kMaxNumPosesDirectSparseSolver) {
    solver_options.linear_solver_type = ceres::SPARSE_SCHUR;
  } else {  // Indirect sparse (preconditioned CG) solver.
    solver_options.linear_solver_type = ceres::ITERATIVE_SCHUR;
    solver_options.preconditioner_type = ceres::SCHUR_JACOBI;
  }

  solver_options.num_threads =
      colmap::GetEffectiveNumThreads(solver_options.num_threads);
#if CERES_VERSION_MAJOR < 2
  solver_options.num_linear_solver_threads =
      colmap::GetEffectiveNumThreads(solver_options.num_linear_solver_threads);
#endif  // CERES_VERSION_MAJOR

  std::string solver_error;
  CHECK(solver_options.IsValid(&solver_error)) << solver_error;

  ceres::Solve(solver_options, problem_.get(), &summary_);

  if (solver_options.minimizer_progress_to_stdout) {
    std::cout << std::endl;
  }

  if (options_.print_summary) {
    colmap::PrintHeading2("Bundle adjustment report");
    PrintSolverSummary(summary_);
  }
  
  // Covariance
  /*
  ceres::Covariance::Options covariance_options;
  covariance_options.algorithm_type = ceres::DENSE_SVD;
  covariance_options.null_space_rank = -1;
  
  ceres::Covariance covariance(covariance_options);
  
  std::unordered_map<id_t, SE3Quat>::const_iterator cit = std::max_element(graph->GetPoses().cbegin(), graph->GetPoses().cend(),
    [](const std::pair<id_t, SE3Quat>& lhs, const std::pair<id_t, SE3Quat>& rhs){
        return lhs.first < rhs.first;
    });
  
  SE3Quat& pose = graph->GetPose(cit->first);
  
  std::vector<std::pair<const double*, const double*>> covariance_blocks;
  covariance_blocks.emplace_back(pose.q, pose.q);
  covariance_blocks.emplace_back(pose.t, pose.t);
  covariance_blocks.emplace_back(pose.q, pose.t);
  
  CHECK(covariance.Compute(covariance_blocks, problem_.get()));
  */

  TearDown(graph);

  return true;
}

const ceres::Solver::Summary& BundleAdjuster::Summary() const {
  return summary_;
}

Eigen::MatrixXd BundleAdjuster::Covariance() const {
  Eigen::MatrixXd covariance(6, 6);
  
  /*
  for () {
  }
  */
  
  return covariance;
}

void BundleAdjuster::SetUp(ConstraintGraph* graph) {

  // Initialize structures to compute the number of observations
  // Poses
  for (const std::pair<id_t, SE3Quat>& entry : graph->GetPoses()) {
    poses_num_observations_[entry.first] = 0;
  }

  // Points
  for (const std::pair<id_t, Point3D>& entry : graph->GetPoints()) {
    points_num_observations_[entry.first] = 0;
  }

  // Create loss functions
  std::size_t nconstraints;
  ceres::LossFunction* pose_loss = BundleAdjustmentOptions::CreateLossFunction(
                                     options_.pose_loss_type, options_.pose_loss_scale);
  ceres::LossFunction* projection_loss = BundleAdjustmentOptions::CreateLossFunction(
                                           options_.projection_loss_type, options_.projection_loss_scale);

  // Add constraints to the optimization problem
  //   Projection constraints
  nconstraints = 0;
  for (const std::pair<pair_id_t, ProjectionConstraint>& edge : graph->GetProjectionConstraints()) {
    pair_id_t edge_id = edge.first;
    id_t point_id = edge_id.first;
    id_t pose_id = edge_id.second;

    CHECK(graph->HasPoint(point_id));
    CHECK(graph->HasPose(pose_id));

    const ProjectionConstraint& constraint = edge.second;
    CHECK(constraint.Check());

    if (constraint.Level() > options_.optimization_level) continue;

    Eigen::Vector4d camera_params(constraint.fx(), constraint.fy(), constraint.cx(), constraint.cy());
    Eigen::Vector2d measurement = constraint.Measurement();
    Eigen::Vector2d weight = constraint.Weight();

    ceres::CostFunction* cost_function = nullptr;
    ceres::ResidualBlockId block_id = nullptr;

    double* xyz_data = graph->GetPoint(point_id).XYZ().data();

    SE3Quat& pose = graph->GetPose(pose_id);
    Eigen::Vector4d q_vec(pose.q[0], pose.q[1], pose.q[2], pose.q[3]);
    Eigen::Vector3d t_vec(pose.t[0], pose.t[1], pose.t[2]);
    double* qvec_data = pose.q;
    double* tvec_data = pose.t;

    bool costant_pose = config_.HasConstantPose(pose_id);
    if (costant_pose) { // Constant pose cost function
      cost_function = BundleAdjustmentConstantPoseCostFunction::Create(q_vec, t_vec,
                                                                       camera_params, measurement, weight);
      block_id = problem_->AddResidualBlock(cost_function, projection_loss, xyz_data);
    } else { // Default cost function
      cost_function = BundleAdjustmentCostFunction::Create(camera_params, measurement, weight);
      block_id = problem_->AddResidualBlock(cost_function, projection_loss, qvec_data,
                                            tvec_data, xyz_data);
    }

    CHECK_NOTNULL(cost_function);
    CHECK_NOTNULL(block_id);

    nconstraints++;

    if (options_.compute_errors) {
      residuals_[block_id] = edge_id;
    }

    poses_num_observations_.at(pose_id)++;
    points_num_observations_.at(point_id)++;
  }

  if (nconstraints == 0)
    delete projection_loss;

  //   Pose constraints
  nconstraints = 0;
  for (const std::pair<pair_id_t, PoseConstraint>& edge : graph->GetPoseConstraints()) {
    pair_id_t edge_id = edge.first;
    id_t pose_a_id = edge_id.first;
    id_t pose_b_id = edge_id.second;

    CHECK(graph->HasPose(pose_a_id));
    CHECK(graph->HasPose(pose_b_id));

    const PoseConstraint& constraint = edge.second;
    CHECK(constraint.Check());

    if (constraint.Level() > options_.optimization_level) continue;

    SE3Quat measurement = constraint.Measurement();
    Eigen::Vector4d qm_vec(measurement.q[0], measurement.q[1], measurement.q[2], measurement.q[3]);
    Eigen::Vector3d tm_vec(measurement.t[0], measurement.t[1], measurement.t[2]);

    Eigen::Matrix6d weight = constraint.Weight();

    ceres::CostFunction* cost_function = nullptr;
    ceres::ResidualBlockId block_id = nullptr;

    SE3Quat& pose_a = graph->GetPose(pose_a_id);
    Eigen::Vector4d qa_vec(pose_a.q[0], pose_a.q[1], pose_a.q[2], pose_a.q[3]);
    Eigen::Vector3d ta_vec(pose_a.t[0], pose_a.t[1], pose_a.t[2]);
    double* qa_vec_data = pose_a.q;
    double* ta_vec_data = pose_a.t;

    SE3Quat& pose_b = graph->GetPose(pose_b_id);
    Eigen::Vector4d qb_vec(pose_b.q[0], pose_b.q[1], pose_b.q[2], pose_b.q[3]);
    Eigen::Vector3d tb_vec(pose_b.t[0], pose_b.t[1], pose_b.t[2]);
    double* qb_vec_data = pose_b.q;
    double* tb_vec_data = pose_b.t;

    if (!config_.HasConstantPose(pose_a_id) && !config_.HasConstantPose(pose_b_id)) {
      cost_function = RelativePoseCostFunction::Create(qm_vec, tm_vec, weight);
      block_id = problem_->AddResidualBlock(cost_function, pose_loss, qa_vec_data, ta_vec_data,
                                            qb_vec_data, tb_vec_data);
    } else if (config_.HasConstantPose(pose_a_id) && !config_.HasConstantPose(pose_b_id)) {
      cost_function = RelativePoseConstantACostFunction::Create(qm_vec, tm_vec,
                                                                qa_vec, ta_vec,
                                                                weight);
      block_id = problem_->AddResidualBlock(cost_function, pose_loss, qb_vec_data, tb_vec_data);
    } else if (!config_.HasConstantPose(pose_a_id) && config_.HasConstantPose(pose_b_id)) {
      Eigen::Vector4d qba_vec;
      Eigen::Vector3d tba_vec;
      colmap::InvertPose(qm_vec, tm_vec, &qba_vec, &tba_vec);
      cost_function = RelativePoseConstantACostFunction::Create(qba_vec, tba_vec,
                                                                qb_vec, tb_vec,
                                                                weight);
      block_id = problem_->AddResidualBlock(cost_function, pose_loss, qa_vec_data, ta_vec_data);
    } else continue;

    CHECK_NOTNULL(cost_function);
    CHECK_NOTNULL(block_id);

    nconstraints++;

    // TODO residuals?
    // if (options_.compute_errors) {
    //   residuals_[block_id] = edge_id;
    // }

    // poses_num_observations_.at(pose_id)++;
    // points_num_observations_.at(point_id)++;
  }

  if (nconstraints == 0)
    delete pose_loss;

  ParameterizePoints(graph);
  ParameterizePoses(graph);
}

void BundleAdjuster::TearDown(ConstraintGraph* graph) {

  if (options_.compute_errors) {
    ceres::Problem::EvaluateOptions evaluation_options;
    evaluation_options.apply_loss_function = false;
    evaluation_options.num_threads = 1;

    for (const std::pair<ceres::ResidualBlockId, pair_id_t>& entry : residuals_) {
      ceres::ResidualBlockId resiudal = entry.first;

      pair_id_t edge_id = entry.second;
      id_t point_id = edge_id.first;
      id_t pose_id = edge_id.second;

      evaluation_options.residual_blocks = {resiudal};
      double chi2 = std::numeric_limits<double>::quiet_NaN();
      bool result = problem_->Evaluate(evaluation_options, &chi2, nullptr, nullptr, nullptr);
      CHECK(result);

      graph->GetProjectionConstraint(point_id, pose_id).Chi2() = chi2;
    }
  }
}

void BundleAdjuster::ParameterizePoints(ConstraintGraph* graph) {
  for (const std::pair<id_t, Point3D>& entry : graph->GetPoints()) {
    id_t point_id = entry.first;
    if (points_num_observations_.at(point_id) == 0) continue;
    if (config_.HasConstantPoint(point_id)) {
      problem_->SetParameterBlockConstant(graph->GetPoint(point_id).XYZ().data());
    }
  }
}

void BundleAdjuster::ParameterizePoses(ConstraintGraph* graph) {
  for (const std::pair<id_t, SE3Quat>& entry : graph->GetPoses()) {
    id_t pose_id = entry.first;
    if (poses_num_observations_.at(pose_id) == 0) continue;
    if (!config_.HasConstantPose(pose_id)) {
      ceres::LocalParameterization* quaternion_parameterization =
          new ceres::QuaternionParameterization;
      problem_->SetParameterization(graph->GetPose(pose_id).q, quaternion_parameterization);
      if (config_.HasConstantTvec(pose_id)) {
        const std::vector<int>& constant_tvec_idxs = config_.ConstantTvec(pose_id);
        ceres::SubsetParameterization* tvec_parameterization =
            new ceres::SubsetParameterization(3, constant_tvec_idxs);
        problem_->SetParameterization(graph->GetPose(pose_id).t, tvec_parameterization);
      }
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

void PrintSolverSummary(const ceres::Solver::Summary& summary) {
  std::cout << std::right << std::setw(16) << "Residuals : ";
  std::cout << std::left << summary.num_residuals_reduced << std::endl;

  std::cout << std::right << std::setw(16) << "Parameters : ";
  std::cout << std::left << summary.num_effective_parameters_reduced
            << std::endl;

  std::cout << std::right << std::setw(16) << "Iterations : ";
  std::cout << std::left
            << summary.num_successful_steps + summary.num_unsuccessful_steps
            << std::endl;

  std::cout << std::right << std::setw(16) << "Time : ";
  std::cout << std::left << summary.total_time_in_seconds << " [s]"
            << std::endl;

  std::cout << std::right << std::setw(16) << "Initial cost : ";
  std::cout << std::right << std::setprecision(6)
            << std::sqrt(summary.initial_cost / summary.num_residuals_reduced)
            << " [px]" << std::endl;

  std::cout << std::right << std::setw(16) << "Final cost : ";
  std::cout << std::right << std::setprecision(6)
            << std::sqrt(summary.final_cost / summary.num_residuals_reduced)
            << " [px]" << std::endl;

  std::cout << std::right << std::setw(16) << "Termination : ";

  std::string termination = "";

  switch (summary.termination_type) {
    case ceres::CONVERGENCE:
      termination = "Convergence";
      break;
    case ceres::NO_CONVERGENCE:
      termination = "No convergence";
      break;
    case ceres::FAILURE:
      termination = "Failure";
      break;
    case ceres::USER_SUCCESS:
      termination = "User success";
      break;
    case ceres::USER_FAILURE:
      termination = "User failure";
      break;
    default:
      termination = "Unknown";
      break;
  }

  std::cout << std::right << termination << std::endl;
  std::cout << std::endl;
}

////////////////////////////////////////////////////////////////////////////////

}  // namespace ORB_SLAM

