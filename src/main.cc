	/**
* This file is part of ORB-SLAM.
*
* Copyright (C) 2014 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <http://webdiis.unizar.es/~raulmur/orbslam/>
*
* ORB-SLAM is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM. If not, see <http://www.gnu.org/licenses/>.
*/

#include<iostream>
#include<fstream>
#include<ros/ros.h>
#include<ros/package.h>
#include<boost/thread.hpp>

#include <glog/logging.h>

#include<opencv2/core/core.hpp>

#include "Tracking.h"
#include "FramePublisher.h"
#include "Map.h"
#include "MapPublisher.h"
#include "LocalMapping.h"
//#include "LoopClosing.h"
#include "KeyFrameDatabase.h"
#include "ORBVocabulary.h"

#include "INS.h"

#include "Converter.h"
#include "SE3Quat.h"

const std::string PROJECT_NAME = "orb_vo_ceres";

using namespace std;


int main(int argc, char **argv)
{
    // Initialize Google's logging library.
    google::InitGoogleLogging(argv[0]);
    FLAGS_logtostderr=1;

    std::cout << "ORB-VO (ceres) running!" << std::endl;
    ros::init(argc, argv, "ORB_VO");
    ros::start();

    cout << endl << "ORB-SLAM Copyright (C) 2014 Raul Mur-Artal" << endl <<
            "This program comes with ABSOLUTELY NO WARRANTY;" << endl  <<
            "This is free software, and you are welcome to redistribute it" << endl <<
            "under certain conditions. See LICENSE.txt." << endl;

    if(argc != 3)
    {
        cerr << endl << "Usage: rosrun ORB_VO ORB_VO path_to_vocabulary path_to_settings (absolute or relative to package directory)" << endl;
        ros::shutdown();
        return 1;
    }

    // Load Settings and Check
    string strSettingsFile = ros::package::getPath(PROJECT_NAME)+"/"+argv[2];

    cv::FileStorage fsSettings(strSettingsFile.c_str(), cv::FileStorage::READ);
    if(!fsSettings.isOpened())
    {
        ROS_ERROR("Wrong path to settings. Path must be abolut or relative to ORB_VO package directory.");
        ros::shutdown();
        return 1;
    }

    //Create Frame Publisher for image_view
    ORB_SLAM::FramePublisher FramePub;

    //Load ORB Vocabulary
    /* Old version to load vocabulary using cv::FileStorage
    string strVocFile = ros::package::getPath("ORB_SLAM")+"/"+argv[1];
    cout << endl << "Loading ORB Vocabulary. This could take a while." << endl;
    cv::FileStorage fsVoc(strVocFile.c_str(), cv::FileStorage::READ);
    if(!fsVoc.isOpened())
    {
        cerr << endl << "Wrong path to vocabulary. Path must be absolut or relative to ORB_SLAM package directory." << endl;
        ros::shutdown();
        return 1;
    }
    ORB_SLAM::ORBVocabulary Vocabulary;
    Vocabulary.load(fsVoc);
    */
    
    // New version to load vocabulary from text file "Data/ORBvoc.txt". 
    // If you have an own .yml vocabulary, use the function
    // saveToTextFile in Thirdparty/DBoW2/DBoW2/TemplatedVocabulary.h
    string strVocFile = ros::package::getPath(PROJECT_NAME)+"/"+argv[1];
    cout << endl << "Loading ORB Vocabulary. This could take a while." << endl;
    
    ORB_SLAM::ORBVocabulary Vocabulary;
    bool bVocLoad = Vocabulary.loadFromTextFile(strVocFile);

    if(!bVocLoad)
    {
        cerr << "Wrong path to vocabulary. Path must be absolut or relative to ORB_VO package directory." << endl;
        cerr << "Falied to open at: " << strVocFile << endl;
        ros::shutdown();
        return 1;
    }

    cout << "Vocabulary loaded!" << endl << endl;

    //Create KeyFrame Database
    ORB_SLAM::KeyFrameDatabase Database(Vocabulary);

    //Create the map
    ORB_SLAM::Map World;

    FramePub.SetMap(&World);

    //Create Map Publisher for Rviz
    ORB_SLAM::MapPublisher MapPub(&World);

    //Initialize the Tracking Thread and launch
    ORB_SLAM::Tracking Tracker(&Vocabulary, &FramePub, &MapPub, &World, strSettingsFile);
    boost::thread trackingThread(&ORB_SLAM::Tracking::Run,&Tracker);

    Tracker.SetKeyFrameDatabase(&Database);

    //Initialize the Local Mapping Thread and launch
    int ins_mode = fsSettings["INS.Mode"];
    cout << "INS.Mode: " << ins_mode << endl;
    
    ORB_SLAM::LocalMapping LocalMapper(&World, ins_mode);
    boost::thread localMappingThread(&ORB_SLAM::LocalMapping::Run,&LocalMapper);

    //Initialize the Loop Closing Thread and launch
    //ORB_SLAM::LoopClosing LoopCloser(&World, &Database, &Vocabulary);
    //boost::thread loopClosingThread(&ORB_SLAM::LoopClosing::Run, &LoopCloser);

    //Initialize the INS thread and launch
    ORB_SLAM::INS INS(strSettingsFile);
    boost::thread insThread(&ORB_SLAM::INS::Run,&INS);

    //Set pointers between threads
    Tracker.SetLocalMapper(&LocalMapper);
    //Tracker.SetLoopClosing(&LoopCloser);
    Tracker.SetINS(&INS);

    LocalMapper.SetTracker(&Tracker);
    //LocalMapper.SetLoopCloser(&LoopCloser);
    LocalMapper.SetINS(&INS);

    //LoopCloser.SetTracker(&Tracker);
    //LoopCloser.SetLocalMapper(&LocalMapper);

    //This "main" thread will show the current processed frame and publish the map
    float fps = fsSettings["Camera.fps"];
    if(fps==0)
        fps=30;

    ros::Rate r(fps);

    while (ros::ok())
    {
        FramePub.Refresh();
        MapPub.Refresh();
        Tracker.CheckResetByPublishers();
        r.sleep();
    }

    // Save keyframe poses at the end of the execution
    ofstream f;

    vector<ORB_SLAM::KeyFrame*> vpKFs = World.GetAllKeyFrames();
    sort(vpKFs.begin(),vpKFs.end(),ORB_SLAM::KeyFrame::lId);

    cout << endl << "Saving Keyframe Trajectory to KeyFrameTrajectory.txt" << endl;
    string strFile = ros::package::getPath(PROJECT_NAME)+"/"+"KeyFrameTrajectory.txt";
    f.open(strFile.c_str());
    f << fixed;

    for(size_t i=0; i<vpKFs.size(); i++)
    {
        ORB_SLAM::KeyFrame* pKF = vpKFs[i];

        if(pKF->isBad())
            continue;

        cv::Mat R = pKF->GetRotation().t();
        vector<float> q = ORB_SLAM::Converter::toQuaternion(R);
        cv::Mat t = pKF->GetCameraCenter();
        f << setprecision(6) << pKF->mTimeStamp << setprecision(7) << " " << t.at<float>(0) << " " << t.at<float>(1) << " " << t.at<float>(2)
          << " " << q[0] << " " << q[1] << " " << q[2] << " " << q[3] << endl;

    }
    f.close();
    
    // Save Body relative poses
    ofstream fbp;
    
    cout << endl << "Saving Keyframe Body relative poses to BodyRelPoses.txt" << endl;
    string strFileBP = ros::package::getPath(PROJECT_NAME)+"/"+"BodyRelPoses.txt";
    fbp.open(strFileBP.c_str());
    fbp << fixed;

    ORB_SLAM::SE3Quat Tbc = INS.Tbc();
    const double scale = INS.Scale();
    for(size_t i=1; i<vpKFs.size(); i++)
    {
        ORB_SLAM::KeyFrame* pKFprev = vpKFs[i-1];
        
        if(pKFprev->isBad())
            continue;
        
        cv::Mat Rprev = pKFprev->GetRotation().t();
        vector<float> qprev = ORB_SLAM::Converter::toQuaternion(Rprev);
        cv::Mat tprev = pKFprev->GetCameraCenter();
        
        ORB_SLAM::SE3Quat Pprev(tprev.at<float>(0), tprev.at<float>(1), tprev.at<float>(2),
                            qprev.at(3), qprev.at(0), qprev.at(1), qprev.at(2));
        
        ORB_SLAM::KeyFrame* pKF = vpKFs[i];

        if(pKF->isBad())
            continue;

        cv::Mat R = pKF->GetRotation().t();
        vector<float> q = ORB_SLAM::Converter::toQuaternion(R);
        cv::Mat t = pKF->GetCameraCenter();
        
        ORB_SLAM::SE3Quat P(t.at<float>(0), t.at<float>(1), t.at<float>(2),
                            q.at(3), q.at(0), q.at(1), q.at(2));
                            
        ORB_SLAM::SE3Quat dT = Tbc * Pprev.inverse() * P * Tbc.inverse();
        dT.t[0] *= scale;
        dT.t[1] *= scale;
        dT.t[2] *= scale;
        
        fbp << setprecision(6) << pKF->mTimeStamp << setprecision(7) << " " << dT.t[0] << " " << dT.t[1] << " " << dT.t[2]
          << " " << dT.q[1] << " " << dT.q[2] << " " << dT.q[3] << " " << dT.q[0] << endl;
    }
    fbp.close();

    //string strFileINS = ros::package::getPath(PROJECT_NAME)+"/"+"KeyFrameTrajectoryINS.txt";
    //INS.Save(strFileINS);
    
    // Save Bundle Adjustment execution times
    ofstream fba;

    vector<double> vBA = LocalMapper.GetBATimes();

    cout << endl << "Saving Bundle Adjustment execution times to BATimes.txt" << endl;
    string strFileBA = ros::package::getPath(PROJECT_NAME)+"/"+"BATimes.txt";
    fba.open(strFileBA.c_str());
    fba << fixed;

    for(const double t : vBA)
    {
        fba << setprecision(6) << t << endl;
    }
    fba.close();
    
    // Save IMU relative poses
    ofstream frt;

    vector<double> vT;
    vector<ORB_SLAM::SE3Quat> vRP;
    INS.GetRelPosesStamed(vRP, vT);

    cout << endl << "Saving IMU relative poses to IMUMeasurements.txt" << endl;
    string strFileRT = ros::package::getPath(PROJECT_NAME)+"/"+"IMURelTrajectory.txt";
    frt.open(strFileRT.c_str());
    frt << fixed;

    for(size_t i = 0; i < vT.size(); ++i)
    {
        ORB_SLAM::SE3Quat rp = vRP.at(i);
        frt << setprecision(6) << vT[i] << setprecision(7) << " " << rp.t[0] << " " << rp.t[1] << " " << rp.t[2] << " "
            << rp.q[1] << " " << rp.q[2] << " " << rp.q[3] << " " << rp.q[0] << endl;
    }
    frt.close();
    
    /*
    strFile = ros::package::getPath(PROJECT_NAME)+"/"+"INSKeyFrameTrajectory.txt";
    f.open(strFile.c_str());
    f << fixed;

    for(size_t i=0; i<vpKFs.size(); i++)
    {
      ORB_SLAM::KeyFrame* pKF = vpKFs[i];

      if(!pKF->GetRefined())
        continue;
      
      ORB_SLAM::SE3Quat actual_pose = ORB_SLAM::Converter::toSE3Quat<float>(pKF->GetPoseINS()).inverse();
      
      f << setprecision(6) << pKF->mTimeStamp << setprecision(7) << " " << actual_pose.t[0] << " " << actual_pose.t[1] << " " << actual_pose.t[2]
          << " " << actual_pose.q[1] << " " << actual_pose.q[2] << " " << actual_pose.q[3] << " " << actual_pose.q[0] << endl;
    }
    f.close();
    */

    ros::shutdown();

    return 0;
}
