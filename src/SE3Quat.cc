
#include "SE3Quat.h"

#include <cmath>

namespace ORB_SLAM
{

SE3Quat::SE3Quat()
  : t{0.0, 0.0, 0.0}, q{1.0, 0.0, 0.0, 0.0}
{
}

SE3Quat::SE3Quat(double tx, double ty, double tz, double qw, double qx, double qy, double qz)
  : t{tx, ty, tz}, q{qw, qx, qy, qz}
{
    q_normalize();
}

SE3Quat::SE3Quat(const Eigen::Vector3d& t_vec, const Eigen::Vector4d& q_vec)
  : t{t_vec(0), t_vec(1), t_vec(2)}, q{q_vec(0), q_vec(1), q_vec(2), q_vec(3)}
{
    q_normalize();
}

template<typename Scalar, int Type>
SE3Quat::SE3Quat(const Eigen::Transform<Scalar, 3, Type> &T)
{
    Eigen::Quaternion<Scalar> quat(T.rotation());
    quat.normalize();

    t[0] = T.translation().x();
    t[1] = T.translation().y();
    t[2] = T.translation().z();
    q[0] = quat.w();
    q[1] = quat.x();
    q[2] = quat.y();
    q[3] = quat.z();
}

// To Eigen Transform (implicit conversion)
template<typename Scalar, int Type>
SE3Quat::operator Eigen::Transform<Scalar, 3, Type>() const {
    Eigen::Quaternion<Scalar> quat(q[0], q[1], q[2], q[3]);
    quat.normalize();

    Eigen::Transform<Scalar, 3, Type> T(quat);

    T.translation().x() = t[0];
    T.translation().y() = t[1];
    T.translation().z() = t[2];

    return T;
}

// Inverse
SE3Quat SE3Quat::inverse() const
{
    return Eigen::Isometry3d(*this).inverse();
}

// Concatenation
SE3Quat SE3Quat::operator *(const SE3Quat& other) const
{
    return Eigen::Isometry3d(*this) * Eigen::Isometry3d(other);
}

void SE3Quat::q_normalize()
{
    double norm = std::sqrt(q[0]*q[0] + q[1]*q[1] + q[2]*q[2] + q[3]*q[3]);
    q[0] /= norm;
    q[1] /= norm;
    q[2] /= norm;
    q[3] /= norm;
}

SE3Quat SE3Quat::Identity()
{
    return SE3Quat(0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0);
}

} //namespace ORB_SLAM
