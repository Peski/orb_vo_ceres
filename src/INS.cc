
#include "INS.h"

#include <algorithm>
#include <cmath>
#include <iostream>
#include <iterator>
#include <stdexcept>

#include <opencv2/core.hpp>

#include <ros/callback_queue.h>

#include <std_msgs/Header.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>

#include <Eigen/Dense>

#include "Converter.h"

#include <colmap/util/random.h>

#include "gps.hpp"

#include "quat.hpp"
#include "jacobians.hpp"

namespace ORB_SLAM
{

namespace {
    // Find closest
    int find_closest(const std::vector<double>& vector, double value) {
        if (vector.size() < 2) return -1;
        
        std::vector<double>::const_iterator nxt = std::lower_bound(vector.cbegin(), vector.cend(), value);
        if (nxt == vector.cbegin() || nxt == vector.cend()) return -1;
        
        std::vector<double>::const_iterator prv = std::prev(nxt);
        if ((value - *prv) < (*nxt - value)) {
            return std::distance(vector.cbegin(), prv);
        } else {
            return std::distance(vector.cbegin(), nxt);
        }
    }
    
    /* Debug only
    std::string state_str(INS::CommunicationState state) {
    
        std::string str;
        switch (state) {
            case INS::CommunicationState::SYSTEM_READY:
                str = "SYSTEM_READY";
                break;
            case INS::CommunicationState::NOT_INITIALIZED:
                str = "NOT_INITIALIZED";
                break;
            case INS::CommunicationState::WAITING_KF:
                str = "WAITING_KF";
                break;
            case INS::CommunicationState::WAITING_IMU:
                str = "WAITING_IMU";
                break;
            case INS::CommunicationState::BA_PENDING:
                str = "BA_PENDING";
                break;
        }
        
        return str;
    }
    */
    
    std::string state_str(INS::GPSTransformState state) {
    
        std::string str;
        switch (state) {
            case INS::GPSTransformState::SYSTEM_NOT_READY:
                str = "SYSTEM_NOT_READY";
                break;
            case INS::GPSTransformState::NO_MEASUREMENTS_YET:
                str = "NO_MEASUREMENTS_YET";
                break;
            case INS::GPSTransformState::NOT_INITIALIZED:
                str = "NOT_INITIALIZED";
                break;
            case INS::GPSTransformState::INITIALIZED:
                str = "INITIALIZED";
                break;
        }
        
        return str;
    }
    
    /*
    std::string to_string(double number) {
      std::stringstream ss;
      ss << std::fixed << std::setprecision(6) << number;
      return ss.str();
    }
    */
}

INS::INS(std::string strSettingPath)
{
    cv::FileStorage fSettings(strSettingPath, cv::FileStorage::READ);

    cv::Mat Tbc;
    fSettings["Tbc"] >> Tbc;
    Tbc.convertTo(Tbc, CV_32F);

    Eigen::Isometry3d T = Eigen::Isometry3d::Identity();
    T.linear() = Converter::toMatrix3d(Tbc.colRange(0,3));
    T.translation() = Converter::toVector3d(Tbc.col(3));

    Tbc_ = SE3Quat(T);
    Tbc_inv = Tbc_.inverse();

    double Hz = fSettings["INS.Hz"];
    if (Hz <= 1.0) throw std::runtime_error("invalid INS.Hz value");

    double epsilon = 0.05 / Hz;

    mSyncTh = 1.0/Hz + 2.0*epsilon;
    mSyncEps = epsilon;

    mWallDuration = 0.5 / Hz;
    
    kf_src = nullptr;
    kf_dst = nullptr;

    std::cout << "INS Parameters: " << std::endl;
    std::cout << "- Tbc: " << Tbc << std::endl;
    std::cout << "- Hz: " << Hz << std::endl;
    std::cout << std::endl;

    mScale = 1.0;
    //mGPSTransform = SE3Quat::Identity();

    mCommunicationState.store(CommunicationState::SYSTEM_READY);
    mGPSTransformState.store(GPSTransformState::SYSTEM_NOT_READY);

    mSyncSeq = 0;
    sync_pub = nh.advertise<std_msgs::Header>("/orb_vo/sync", 10);
    //estimate_pub = nh.advertise<geometry_msgs::PoseWithCovarianceStamped>("/orb_vo/estimate", 10);
    
    //std::cout << "[INS] " << state_str(mCommunicationState.load()) << std::endl;
}

/*
int INS::GetGPSTransformState() {
    return mGPSTransformState.load();
}

std::map<KeyFrame*, SE3Quat> INS::GetPoses()
{
    boost::mutex::scoped_lock lock_map(mMutexKFPoses);

    return mmKFPoses; // returns a copy!
}
*/

void INS::Run()
{
    ros::NodeHandle nodeHandler;
    ros::CallbackQueue callback_queue;
    nodeHandler.setCallbackQueue(&callback_queue);

    ros::Subscriber sub_gps = nodeHandler.subscribe("/ins/reference", 100, &INS::GrabGPS, this);
    ros::Subscriber sub_imu = nodeHandler.subscribe("/imu/estimate", 100, &INS::GrabIMU, this);

    mGPSTransformState.store(GPSTransformState::NO_MEASUREMENTS_YET);

    while (ros::ok())
    {
      callback_queue.callAvailable(ros::WallDuration(mWallDuration));
    }
}

INS::CommunicationState INS::GetCommunicationState() {
    return mCommunicationState.load();
}

/*
void INS::Save(const std::string& file) {

    std::map<KeyFrame*, SE3Quat> mKFPoses; // Local copy
    {
      boost::mutex::scoped_lock lock(mMutexKFPoses);
      mKFPoses = mmKFPoses;
    }

    std::vector<KeyFrame*> vpKF;
    vpKF.reserve(mKFPoses.size());
    for (const std::pair<KeyFrame*, SE3Quat>& entry : mKFPoses)
      vpKF.push_back(entry.first);

    if (vpKF.empty()) return;

    std::sort(vpKF.begin(), vpKF.end(), ORB_SLAM::KeyFrame::lId);

    std::ofstream f(file);
    f << fixed;

    for (std::vector<KeyFrame*>::const_iterator it = vpKF.cbegin(); it != vpKF.cend(); ++it) {
      KeyFrame* pKF = *it;

      SE3Quat T = mKFPoses[pKF];

      f << setprecision(6) << pKF->mTimeStamp << setprecision(7) << " " << T.t[0] << " " << T.t[1] << " " << T.t[2]
        << " " << T.q[1] << " " << T.q[2] << " " << T.q[3] << " " << T.q[0] << endl;
    }
    f.close();
}
*/

SE3Quat INS::InterpolatePose(const SE3Quat& A, const SE3Quat& B, double t)
{
    if (t <= 0.0) return A;
    if (t >= 1.0) return B;

    Eigen::Vector3d tran = Eigen::Vector3d(A.t[0], A.t[1], A.t[2]);
    tran = t*(Eigen::Vector3d(B.t[0], B.t[1], B.t[2]) - tran);

    Eigen::Quaterniond quat = Eigen::Quaterniond(A.q[0], A.q[1], A.q[2], A.q[3]);
    quat.slerp(t, Eigen::Quaterniond(B.q[0], B.q[1], B.q[2], B.q[3]));

    SE3Quat interpolated;
    interpolated.t[0] = tran.x();
    interpolated.t[1] = tran.y();
    interpolated.t[2] = tran.z();
    interpolated.q[0] = quat.w();
    interpolated.q[1] = quat.x();
    interpolated.q[2] = quat.y();
    interpolated.q[3] = quat.z();
    interpolated.q_normalize();

    return interpolated;
}

void INS::PublishSync(KeyFrame* pKF) {
    std_msgs::Header msg;
    msg.seq = mSyncSeq;
    msg.stamp = ros::Time(pKF->mTimeStamp);
    msg.frame_id = std::to_string(pKF->mnId);
    
    sync_pub.publish(msg);
    mSyncSeq++;
}

/*
void INS::Publish(KeyFrame* pKF, const SE3Quat& pose)
{
    geometry_msgs::PoseStamped msg;
    msg.header.stamp = ros::Time(pKF->mTimeStamp);
    msg.header.frame_id = std::to_string(pKF->mnId);

    msg.pose.position.x = pose.t[0];
    msg.pose.position.y = pose.t[1];
    msg.pose.position.z = pose.t[2];
    msg.pose.orientation.w = pose.q[0];
    msg.pose.orientation.x = pose.q[1];
    msg.pose.orientation.y = pose.q[2];
    msg.pose.orientation.z = pose.q[3];

    pub.publish(msg);
}

void INS::PublishLocal(long unsigned int nLocalIdStart)
{
    boost::mutex::scoped_lock lock_map(mMutexKFGPS);

    for (std::map<KeyFrame*, SE3Quat>::iterator it = mmKFGPS.begin(); it != mmKFGPS.end(); ++it) {
        KeyFrame* pKF = it->first;
        if (pKF->mnId < nLocalIdStart) continue;

        SE3Quat pose = it->second;

        geometry_msgs::PoseStamped msg;
        msg.header.stamp = ros::Time(pKF->mTimeStamp);
        msg.header.frame_id = std::to_string(pKF->mnId);

        msg.pose.position.x = pose.t[0];
        msg.pose.position.y = pose.t[1];
        msg.pose.position.z = pose.t[2];
        msg.pose.orientation.w = pose.q[0];
        msg.pose.orientation.x = pose.q[1];
        msg.pose.orientation.y = pose.q[2];
        msg.pose.orientation.z = pose.q[3];

        pub_local.publish(msg);
    }
}
*/

void INS::NotifyNewKeyFrame(KeyFrame* pKF)
{
    boost::mutex::scoped_lock lock(mMutexSyncKFs);

    CommunicationState state = mCommunicationState.load();
    if (state == CommunicationState::WAITING_KF) {
        mCommunicationState.store(CommunicationState::WAITING_IMU);
        kf_dst = kf_src;
        kf_src = pKF;
        PublishSync(pKF);
    } else if (state == CommunicationState::SYSTEM_READY) {
        mCommunicationState.store(CommunicationState::NOT_INITIALIZED);
        kf_dst = pKF;
        PublishSync(pKF);
    } else if (state == CommunicationState::NOT_INITIALIZED) { // Special case (first KF)
        mCommunicationState.store(CommunicationState::BA_PENDING);
        kf_src = pKF;
        PublishSync(pKF);
    } else { // Any other state...
        mvSyncKFs.push_back(pKF);
    }
    
    //std::cout << "[NotifyNewKeyFrame] " << state_str(state) << " -> " << state_str(mCommunicationState.load()) << std::endl;
    
    //SE3Quat pose = Converter::toSE3Quat<float>(pKF->GetPose()).inverse();
    //Publish(pKF, pose);

    /*
    {
        boost::mutex::scoped_lock lock_map(mMutexKFGPS);

        SE3Quat pose = Converter::toSE3Quat<float>(pKF->GetPose()).inverse();
        if (mGPSTransformState.load() == WORKING) {
            Eigen::Isometry3d S(mGPSTransform);
            Eigen::Isometry3d T(pose);

            // TODO Translation only!
            T.translation() = mScale * S.linear() * T.translation() + S.translation();

            pose = SE3Quat(T);
            Publish(pKF, pose);
        }

        mmKFGPS[pKF] = pose;
    }
    */

/*
    if (!mlKFBuffer.empty()) {
        std::list<KeyFrame*> buffer_copy = mlKFBuffer;
        mlKFBuffer.clear();

        std::size_t min_idx = std::numeric_limits<std::size_t>::max();
        for (KeyFrame* pKF : buffer_copy) {
            std::size_t idx = Sync(pKF);
            if (idx < min_idx) min_idx = idx;
        }        

        MarginalizeObservations(min_idx);
    }

    std::size_t idx = Sync(pKF);
    MarginalizeObservations(idx);
*/
}

void INS::NotifyBATerminated() {
    
    boost::mutex::scoped_lock lock(mMutexSyncKFs);
    
    // TODO send estimated relative pose (with covariance)
    
    //CommunicationState state = mCommunicationState.load();
    
    KeyFrame* kf = nullptr;
    if (!mvSyncKFs.empty()) kf = mvSyncKFs.back();
    mvSyncKFs.clear();
    
    if (kf == nullptr) {
        mCommunicationState.store(CommunicationState::WAITING_KF);
    } else {
        mCommunicationState.store(CommunicationState::WAITING_IMU);
        kf_dst = kf_src;
        kf_src = kf;
        PublishSync(kf);
    }
    
    //std::cout << "[NotifyBATerminated] " << state_str(state) << " -> " << state_str(mCommunicationState.load()) << std::endl;
}

bool INS::SetInitializationKeyFrames(KeyFrame* pKF0, KeyFrame* pKF1)
{
    if (pKF0 == pKF1) {
        std::cout << "[INS] Invalid initialization pair" << std::endl;
        return true;
    }

    // TODO What if re-initialization required?
    //      Polling or fail?
    GPSTransformState state = mGPSTransformState.load();
    if (state == GPSTransformState::INITIALIZED) {
        std::cout << "[INS] Already initialized!" << std::endl;
        return true;
    }
    if (state != GPSTransformState::NOT_INITIALIZED) {
        std::cout << "[INS] Invalid state to start initialization! " << state_str(state) << std::endl;
        //return false;
        std::cout << "Waiting appropriate state..." << std::endl;
        while (mGPSTransformState.load() != GPSTransformState::NOT_INITIALIZED);
    }
    
    boost::mutex::scoped_lock lock(mMutexGPSData);
    
    int idx0 = find_closest(mvGPSTimestamps, pKF0->mTimeStamp);
    int idx1 = find_closest(mvGPSTimestamps, pKF1->mTimeStamp);
    
    if (idx0 < 0 || idx1 < 0) {
        std::cout << "[INS] Can't find matches for initialization" << std::endl;
        return false;
    }

    geographic_msgs::GeoPose kf0_gps = mvGPSData[idx0];
    Eigen::Vector3d kf0_ecef = gps::geodetic2Ecef(kf0_gps.position.latitude, kf0_gps.position.longitude, kf0_gps.position.altitude);
    
    geographic_msgs::GeoPose kf1_gps = mvGPSData[idx1];
    Eigen::Vector3d kf1_ecef = gps::geodetic2Ecef(kf1_gps.position.latitude, kf1_gps.position.longitude, kf1_gps.position.altitude);

    Eigen::Vector3d kf0 = Converter::toVector3d(pKF0->GetCameraCenter());
    Eigen::Vector3d kf1 = Converter::toVector3d(pKF1->GetCameraCenter());
    
    mScale = (kf1_ecef - kf0_ecef).norm() / (kf1 - kf0).norm();
    
    std::cout << "[INS] mScale: " << mScale << std::endl;

/*
    // TODO Actually compute the transformation
    double scale = 1.0;
    Eigen::Isometry3d S = Eigen::Isometry3d::Identity();

    mScale = scale;
    mGPSTransform = SE3Quat(S);

    {
        boost::mutex::scoped_lock lock_map(mMutexKFGPS);

        // Transform all keyframes
        for (std::map<KeyFrame*, SE3Quat>::iterator it = mmKFGPS.begin(); it != mmKFGPS.end(); ++it) {
            Eigen::Isometry3d T(it->second);

            // TODO Translation only!
            T.translation() = scale * S.linear() * T.translation() + S.translation();

            it->second = SE3Quat(T);

            Publish(it->first, it->second);
        }

        mGPSTransformState.store(WORKING);
    }
    std::cout << "[INS] Initialized!" << std::endl;
*/

    mGPSTransformState.store(GPSTransformState::INITIALIZED);

    return true;
}

void INS::GrabGPS(const geographic_msgs::GeoPoseStampedPtr& msg)
{
    boost::mutex::scoped_lock lock(mMutexGPSData);

    if (mGPSTransformState.load() == GPSTransformState::INITIALIZED) return; // TODO unsuscribe;

    if (mGPSTransformState.load() == GPSTransformState::NO_MEASUREMENTS_YET) {
        mGPSTransformState.store(GPSTransformState::NOT_INITIALIZED);
    }

    double ts = msg->header.stamp.toSec();
    if (mvGPSTimestamps.empty() || ts > mvGPSTimestamps.back())
    {
        mvGPSTimestamps.push_back(ts);
        mvGPSData.push_back(msg->pose);
    }
}

void INS::GrabIMU(const geometry_msgs::PoseWithCovarianceStampedPtr& msg) {

    boost::mutex::scoped_lock lock(mMutexSyncKFs);

    //CommunicationState state = mCommunicationState.load();

    if (mCommunicationState.load() == CommunicationState::WAITING_IMU) { // Waiting response
            
        InertialFactor imu_data;
        
        imu_data.src = kf_src;
        imu_data.dst = kf_dst;
        
        geometry_msgs::Pose msg_pose = msg->pose.pose;
        
        SE3Quat dT_body(msg_pose.position.x, msg_pose.position.y, msg_pose.position.z,
                        msg_pose.orientation.w, msg_pose.orientation.x, msg_pose.orientation.y, msg_pose.orientation.z);

        mvIMURelPosesTimestamps.push_back(msg->header.stamp.toSec());
        mvIMURelPoses.push_back(dT_body);
        
        /*
        imu_data.relative_pose = Tbc_inv * dT_body.inverse() * Tbc_;
        imu_data.relative_pose.t[0] /= mScale;
        imu_data.relative_pose.t[1] /= mScale;
        imu_data.relative_pose.t[2] /= mScale;
        */
        
        Eigen::MatrixXd covariance(6, 6);
        for (int i = 0; i < 6; ++i) 
          for (int j = 0; j < 6; ++j)
            covariance(i, j) = msg->pose.covariance[i*6 + j];
        
        Eigen::MatrixXd J;
        
        // fromYPR
        double ypr[3];
        toYPR(dT_body.q, ypr);
        
        J.resize(7, 6);
        J.setZero();
        
        J.block<3, 3>(0, 0) = Eigen::Matrix3d::Identity();
        
        fromYPRJacobian(ypr, J.block<4, 3>(3, 3));
        covariance = J*covariance*J.transpose();
        
        // inverse
        J.resize(7, 7);
        inverseJacobian(dT_body, J);
        covariance = J*covariance*J.transpose();
        
        SE3Quat dT_body_inv = dT_body.inverse();
        
        // J_a
        Eigen::MatrixXd J_aux(7, 7);
        
        compositionJacobians(dT_body_inv, Tbc_, J, J_aux);
        covariance = J*covariance*J.transpose();
        
        SE3Quat constraint;
        constraint = dT_body_inv * Tbc_;
        
        // J_b
        compositionJacobians(Tbc_inv, constraint, J_aux, J);
        covariance = J*covariance*J.transpose();
        
        constraint = Tbc_inv * constraint;
        
        // Scale
        J.setZero();
        J.block<3, 3>(0, 0) = (1.0 / mScale) * Eigen::Matrix3d::Identity();
        J.block<4, 4>(3, 3) =                  Eigen::Matrix4d::Identity();
        covariance = J*covariance*J.transpose();
        
        constraint.t[0] /= mScale;
        constraint.t[1] /= mScale;
        constraint.t[2] /= mScale;
        
        imu_data.relative_pose = constraint;
        
        Eigen::Matrix6d covariance_simplified;
        covariance_simplified.block<3, 3>(0, 0) = covariance.block<3, 3>(0, 0);
        covariance_simplified.block<3, 3>(0, 3) = covariance.block<3, 3>(0, 4);
        covariance_simplified.block<3, 3>(3, 0) = covariance.block<3, 3>(4, 0);
        covariance_simplified.block<3, 3>(3, 3) = covariance.block<3, 3>(4, 4);
        
        Eigen::Matrix6d information = covariance_simplified.inverse();
        
        Eigen::SelfAdjointEigenSolver<Eigen::Matrix6d> solver(information);
        imu_data.information_sqrt = solver.operatorSqrt();
        
        boost::mutex::scoped_lock lock2(mMutexInertialData);
        mvInertialData.push_back(imu_data);
        
        mCommunicationState.store(CommunicationState::BA_PENDING);
    } else {
        std::cout << "[INS] Communication protocol violated!" << std::endl;
    }
    
    //std::cout << "[GrabIMU] " << state_str(state) << " -> " << state_str(mCommunicationState.load()) << std::endl;
}

std::vector<INS::InertialFactor> INS::GetInertialFactors() {
    boost::mutex::scoped_lock lock(mMutexInertialData);
    return mvInertialData;
}

void INS::GetRelPosesStamed(std::vector<SE3Quat> &rel_poses, std::vector<double> &timestamps)
{
    rel_poses = mvIMURelPoses;
    timestamps = mvIMURelPosesTimestamps;
}

SE3Quat INS::Tbc()
{
    return Tbc_;
}

double INS::Scale()
{
    return mScale;
}

/*
void INS::GrabINS(const geometry_msgs::PoseStampedConstPtr& msg)
{
    if (mGPSTransformState.load() == NO_MEASUREMENTS_YET) {
        mGPSTransformState.store(NOT_INITIALIZED);
    }

    boost::mutex::scoped_lock lock(mMutexNewPoses);

    double ts = msg->header.stamp.toSec();
    SE3Quat p = SE3Quat(msg->pose.position.x, msg->pose.position.y, msg->pose.position.z,
                      msg->pose.orientation.w, msg->pose.orientation.x, msg->pose.orientation.y, msg->pose.orientation.z);

    if (mvOrderedTimestamps.empty() || ts >= mvOrderedTimestamps.back())
    {
        mvOrderedTimestamps.push_back(ts);
        mvPoses.push_back(p);
    }
}

void INS::MarginalizeObservations(std::size_t idx)
{
    if (idx > 0) {
        boost::mutex::scoped_lock lock(mMutexNewPoses);

        // Remove mvPoses and mvOrderedTimestamps elements with idx < min_idx
        std::vector<double> newTimestamps(std::next(mvOrderedTimestamps.begin(), idx-1), mvOrderedTimestamps.end());
        std::vector<SE3Quat> newPoses(std::next(mvPoses.begin(), idx-1), mvPoses.end());

        mvOrderedTimestamps = newTimestamps;
        mvPoses = newPoses;
    }
}

void INS::InsertPose(KeyFrame* pKF, const SE3Quat& pose)
{
    boost::mutex::scoped_lock lock_map(mMutexKFPoses);
    //TODO insert (pose * Tic_).inverse() instead
    mmKFPoses[pKF] = pose;
}

std::size_t INS::Sync(KeyFrame* pKF)
{
    if (mvOrderedTimestamps.empty()) return 0;

    SE3Quat pose;
    std::size_t ret;
    {
        boost::mutex::scoped_lock lock(mMutexNewPoses);

        double ts = pKF->mTimeStamp;

        // find pKF->mTimeStamp in mvOrderedTimestamps (upper and lower bound)
        std::vector<double>::iterator next = std::lower_bound(mvOrderedTimestamps.begin(), mvOrderedTimestamps.end(), ts);
        if (next != mvOrderedTimestamps.end() && std::abs(*next - ts) < mSyncEps) {
            std::size_t idx = std::distance(mvOrderedTimestamps.begin(), next);
            pose = mvPoses[idx];
            ret = idx;
        } else if (next == mvOrderedTimestamps.begin()) {
            if (std::abs(*next - ts) < mSyncEps) {
                pose = mvPoses.front();
                ret = 0;
            } else {
                // pose not accepted...
                return 0;
            }
        } else {
            std::vector<double>::iterator prev = std::prev(next);
            if (next == mvOrderedTimestamps.end()) {
                if (std::abs(ts - *prev) < mSyncEps) {
                    pose = mvPoses.back();
                    ret = std::distance(mvOrderedTimestamps.begin(), --mvOrderedTimestamps.end());
                } else {
                    mlKFBuffer.push_back(pKF);
                    // pose not accepted...
                    return std::distance(mvOrderedTimestamps.begin(), --mvOrderedTimestamps.end());
                }
            } else {
                // check upper bound - lower bound < th (or (ts - lb) < epsilon)
                if ((ts - *prev) < mSyncEps) {
                    std::size_t idx = std::distance(mvOrderedTimestamps.begin(), prev);
                    pose = mvPoses[idx];
                    ret = idx;
                } else if ((*next - *prev) < mSyncTh) {
                    // interpolate pose
                    std::size_t idx_a = std::distance(mvOrderedTimestamps.begin(), prev);
                    std::size_t idx_b = std::distance(mvOrderedTimestamps.begin(), next);
                    double t = ts - *prev / (*next - *prev);
                    t = std::max(0.0, std::min(1.0, t));

                    pose = InterpolatePose(mvPoses[idx_a], mvPoses[idx_b], t);
                    ret = std::distance(mvOrderedTimestamps.begin(), prev);
                } else {
                    // pose not accepted...
                    return std::distance(mvOrderedTimestamps.begin(), prev);
                }
            }
        }
    }

    InsertPose(pKF, pose);
    return ret;
}
*/

} //namespace ORB_SLAM

