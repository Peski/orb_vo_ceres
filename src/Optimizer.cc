﻿/**
* This file is part of ORB-SLAM.
*
* Copyright (C) 2014 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <http://webdiis.unizar.es/~raulmur/orbslam/>
*
* ORB-SLAM is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Optimizer.h"

#include <algorithm>
#include <cmath>
#include <iterator>
#include <limits>
#include <list>
#include <map>
#include <unordered_set>
#include <vector>

#include "bundle_adjustment.h"

/*
#include "Thirdparty/g2o/g2o/core/block_solver.h"
#include "Thirdparty/g2o/g2o/core/optimization_algorithm_levenberg.h"
#include "Thirdparty/g2o/g2o/solvers/linear_solver_eigen.h"
#include "Thirdparty/g2o/g2o/types/types_six_dof_expmap.h"
#include "Thirdparty/g2o/g2o/core/robust_kernel_impl.h"
#include "Thirdparty/g2o/g2o/solvers/linear_solver_dense.h"
#include "Thirdparty/g2o/g2o/types/types_seven_dof_expmap.h"
*/

//#include<Eigen/StdVector>

// Colmap
#include <colmap/base/pose.h>
#include <colmap/util/alignment.h>
#include <colmap/util/logging.h>
#include <colmap/util/math.h>

#include "Converter.h"

namespace {

// TODO Pointer to std::atomic<bool>!

class StopFlagCheckerCallback : public ceres::IterationCallback {
public:
  StopFlagCheckerCallback(bool* pbStopFlag)
      : pbStopFlag_(pbStopFlag) {}

  virtual ceres::CallbackReturnType operator()(const ceres::IterationSummary& summary) {
    if (pbStopFlag_ && *pbStopFlag_) {
      return ceres::SOLVER_TERMINATE_SUCCESSFULLY;
    }

    return ceres::SOLVER_CONTINUE;
  }

private:
  bool* pbStopFlag_;
};

} // namespace

namespace ORB_SLAM
{

void Optimizer::GlobalBundleAdjustemnt(Map* pMap, int nIterations, bool* pbStopFlag)
{
    std::vector<KeyFrame*> vpKFs = pMap->GetAllKeyFrames();
    std::vector<MapPoint*> vpMP = pMap->GetAllMapPoints();
    BundleAdjustment(vpKFs,vpMP,nIterations,pbStopFlag);
}

void Optimizer::BundleAdjustment(const std::vector<KeyFrame *> &vpKFs, const std::vector<MapPoint *> &vpMP, int nIterations, bool* pbStopFlag)
{
    ConstraintGraph graph;

    BundleAdjustmentOptions options;
    options.projection_loss_type = BundleAdjustmentOptions::LossFunctionType::HUBER;
    options.projection_loss_scale = std::sqrt(5.991);
    options.compute_errors = false;
    options.print_summary = false;
    options.solver_options.max_num_iterations = nIterations;

    StopFlagCheckerCallback callback(pbStopFlag);
    if (pbStopFlag)
        options.solver_options.callbacks.push_back(&callback);

    BundleAdjustmentConfig config;

    long unsigned int maxKFid = 0;

    // SET KEYFRAME VERTICES
    for(size_t i=0, iend=vpKFs.size(); i<iend; i++)
    {
        KeyFrame* pKF = vpKFs[i];
        if(pKF->isBad())
            continue;

        graph.AddPose(pKF->mnId, Converter::toSE3Quat<float>(pKF->GetPose()));
        if (pKF->mnId == 0)
            config.SetConstantPose(pKF->mnId);
        if(pKF->mnId > maxKFid)
            maxKFid=pKF->mnId;
    }

    // SET MAP POINT VERTICES
    for(size_t i=0, iend=vpMP.size(); i<iend;i++)
    {
        MapPoint* pMP = vpMP[i];
        if(pMP->isBad())
            continue;
        int id = pMP->mnId+maxKFid+1;
        graph.AddPoint(id, Converter::toVector3d(pMP->GetWorldPos()));

        std::map<KeyFrame*,size_t> observations = pMP->GetObservations();

        //SET EDGES
        for(std::map<KeyFrame*,size_t>::iterator mit=observations.begin(), mend=observations.end(); mit!=mend; mit++)
        {
            KeyFrame* pKF = mit->first;
            if(pKF->isBad())
                continue;
            Eigen::Matrix<double,2,1> obs;
            cv::KeyPoint kpUn = pKF->GetKeyPointUn(mit->second);
            obs << kpUn.pt.x, kpUn.pt.y;

            ProjectionConstraint e(id, pKF->mnId);

            e.SetMeasurement(obs);
            float invSigma = std::sqrt(pKF->GetInvSigma2(kpUn.octave));
            e.SetWeight(Eigen::Vector2d(invSigma, invSigma));

            e.fx() = pKF->fx;
            e.fy() = pKF->fy;
            e.cx() = pKF->cx;
            e.cy() = pKF->cy;

            graph.AddProjectionConstraint(id, pKF->mnId, e);
        }
    }

    // Optimize!
    {
      BundleAdjuster ba(options, config);
      ba.Solve(&graph);
    }

    // Recover optimized data

    //Keyframes
    for(size_t i=0, iend=vpKFs.size(); i<iend; i++)
    {
        KeyFrame* pKF = vpKFs[i];
        pKF->SetPose(Converter::toCvMat<float>(graph.GetPose(pKF->mnId)));
    }

    //Points
    for(size_t i=0, iend=vpMP.size(); i<iend;i++)
    {
        MapPoint* pMP = vpMP[i];
        pMP->SetWorldPos(Converter::toCvMat(graph.GetPoint(pMP->mnId+maxKFid+1)));
        pMP->UpdateNormalAndDepth();
    }

}

int Optimizer::PoseOptimization(Frame *pFrame)
{
    ConstraintGraph graph;

    BundleAdjustmentOptions options;
    options.projection_loss_type = BundleAdjustmentOptions::LossFunctionType::HUBER;
    options.projection_loss_scale = std::sqrt(5.991);
    options.compute_errors = true;
    options.print_summary = false;

    BundleAdjustmentConfig config;

    int nInitialCorrespondences=0;

    // SET FRAME VERTEX
    graph.AddPose(0, Converter::toSE3Quat<float>(pFrame->mTcw));

    // SET MAP POINT VERTICES
    std::vector<pair_id_t> vpEdges;
    std::vector<size_t> vnIndexEdge;

    const int N = pFrame->mvpMapPoints.size();
    vpEdges.reserve(N);
    vnIndexEdge.reserve(N);

    for(int i=0; i<N; i++)
    {
        MapPoint* pMP = pFrame->mvpMapPoints[i];
        if(pMP)
        {
            graph.AddPoint(i+1, Converter::toVector3d(pMP->GetWorldPos()));
            config.SetConstantPoint(i+1);

            nInitialCorrespondences++;
            pFrame->mvbOutlier[i] = false;

            //SET EDGE
            Eigen::Matrix<double,2,1> obs;
            cv::KeyPoint kpUn = pFrame->mvKeysUn[i];
            obs << kpUn.pt.x, kpUn.pt.y;

            ProjectionConstraint e(i+1, 0);
            e.SetMeasurement(obs);
            const float invSigma = std::sqrt(pFrame->mvInvLevelSigma2[kpUn.octave]);
            e.SetWeight(Eigen::Vector2d(invSigma, invSigma));

            e.fx() = pFrame->fx;
            e.fy() = pFrame->fy;
            e.cx() = pFrame->cx;
            e.cy() = pFrame->cy;

            e.SetLevel(0);

            graph.AddProjectionConstraint(i+1, 0, e);

            vpEdges.push_back(std::make_pair(i+1, 0));
            vnIndexEdge.push_back(i);
        }

    }

    // We perform 4 optimizations, decreasing the inlier region
    // From second to final optimization we include only inliers in the optimization
    // At the end of each optimization we check which points are inliers
    const float chi2[4]={9.210,7.378,5.991,5.991};
    const int its[4]={10,10,7,5};

    int nBad=0;
    for(size_t it=0; it<4; it++)
    {
        options.solver_options.max_num_iterations = its[it];
        {
            BundleAdjuster ba(options, config);
            ba.Solve(&graph);
        }

        nBad=0;
        for(size_t i=0, iend=vpEdges.size(); i<iend; i++)
        {
            pair_id_t pair_id = vpEdges[i];
            ProjectionConstraint& e = graph.GetProjectionConstraint(pair_id);

            const size_t idx = vnIndexEdge[i];

            if(pFrame->mvbOutlier[idx]) {
                e.UpdateChi2(graph.GetPoint(pair_id.first), graph.GetPose(pair_id.second));
            }

            if(e.Chi2()>chi2[it])
            {                
                pFrame->mvbOutlier[idx]=true;
                e.SetLevel(1);
                nBad++;
            }
            else if(e.Chi2()<=chi2[it])
            {
                pFrame->mvbOutlier[idx]=false;
                e.SetLevel(0);
            }
        }

        if(graph.GetProjectionConstraints().size()<10)
            break;
    }

    // Recover optimized pose and return number of inliers
    cv::Mat pose = Converter::toCvMat<float>(graph.GetPose(0));
    pose.copyTo(pFrame->mTcw);

    return nInitialCorrespondences-nBad;
}

bool Optimizer::LocalBundleAdjustment(KeyFrame *pKF, const std::vector<INS::InertialFactor>& vImuData, bool *pbStopFlag, long unsigned int *nLocalIdStart)
{
  //bool finished = LocalBundleAdjustmentVisual(pKF, pbStopFlag, nLocalIdStart);
  //if (!pbStopFlag || !(*pbStopFlag)) {
  //  LocalBundleAdjustmentInertial(pKF, vImuData);
  //}
  //return finished;

    // Local KeyFrames: First Breath Search from Current Keyframe
    std::list<KeyFrame*> lLocalKeyFrames;

    lLocalKeyFrames.push_back(pKF);
    pKF->mnBALocalForKF = pKF->mnId;

    std::vector<KeyFrame*> vNeighKFs = pKF->GetVectorCovisibleKeyFrames();
    for(int i=0, iend=vNeighKFs.size(); i<iend; i++)
    {
        KeyFrame* pKFi = vNeighKFs[i];
        pKFi->mnBALocalForKF = pKF->mnId;
        if(!pKFi->isBad())
            lLocalKeyFrames.push_back(pKFi);
    }
    
    if (lLocalKeyFrames.size() <= 2) return true; // At least 3 are needed, as the first two
    // keyframes are kept fixed to avoid scale gauge freedom
    
    long unsigned int nLocalWindowStart = std::numeric_limits<long unsigned int>::max();
    for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin(), lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    {
        KeyFrame* pKF = *lit;
        if (pKF->mnId < nLocalWindowStart) nLocalWindowStart = pKF->mnId;
    }

    // Local MapPoints seen in Local KeyFrames
    std::list<MapPoint*> lLocalMapPoints;
    for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin(), lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    {
        std::vector<MapPoint*> vpMPs = (*lit)->GetMapPointMatches();
        for(std::vector<MapPoint*>::iterator vit=vpMPs.begin(), vend=vpMPs.end(); vit!=vend; vit++)
        {
            MapPoint* pMP = *vit;
            if(pMP)
                if(!pMP->isBad())
                    if(pMP->mnBALocalForKF!=pKF->mnId)
                    {
                        lLocalMapPoints.push_back(pMP);
                        pMP->mnBALocalForKF=pKF->mnId;
                    }
        }
    }

    // Fixed Keyframes. Keyframes that see Local MapPoints but that are not Local Keyframes
    std::unordered_set<KeyFrame*> sFixedCameras;
    for(std::list<MapPoint*>::iterator lit=lLocalMapPoints.begin(), lend=lLocalMapPoints.end(); lit!=lend; lit++)
    {
        std::map<KeyFrame*,size_t> observations = (*lit)->GetObservations();
        for(std::map<KeyFrame*,size_t>::iterator mit=observations.begin(), mend=observations.end(); mit!=mend; mit++)
        {
            KeyFrame* pKFi = mit->first;

            if(pKFi->mnBALocalForKF!=pKF->mnId && pKFi->mnBAFixedForKF!=pKF->mnId)
            {                
                pKFi->mnBAFixedForKF=pKF->mnId;
                if(!pKFi->isBad())
                    sFixedCameras.insert(pKFi);
            }
        }
    }

    // Setup optimizer
    ConstraintGraph graph;

    BundleAdjustmentOptions options;
    options.projection_loss_type = BundleAdjustmentOptions::LossFunctionType::HUBER;
    options.projection_loss_scale = std::sqrt(5.991);
    options.compute_errors = true;
    options.print_summary = false;
    options.solver_options.max_num_iterations = 20;

    StopFlagCheckerCallback callback(pbStopFlag);
    if (pbStopFlag)
        options.solver_options.callbacks.push_back(&callback);

    BundleAdjustmentConfig config;

    long unsigned int maxKFid = 0;

    // SET LOCAL KEYFRAME VERTICES
    for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin(), lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    {
        KeyFrame* pKFi = *lit;
        graph.AddPose(pKFi->mnId, Converter::toSE3Quat<float>(pKFi->GetPose()));
        if (pKFi->mnId == 0 || pKFi->mnId == 1) // Keep scale!
            config.SetConstantPose(pKFi->mnId);
        if(pKFi->mnId>maxKFid)
            maxKFid=pKFi->mnId;
    }

    // SET FIXED KEYFRAME VERTICES
    for(std::unordered_set<KeyFrame*>::iterator lit=sFixedCameras.begin(), lend=sFixedCameras.end(); lit!=lend; lit++)
    {
        KeyFrame* pKFi = *lit;
        graph.AddPose(pKFi->mnId, Converter::toSE3Quat<float>(pKFi->GetPose()));
        config.SetConstantPose(pKFi->mnId);
        if(pKFi->mnId>maxKFid)
            maxKFid=pKFi->mnId;
    }

    // SET MAP POINT VERTICES
    const int nExpectedSize = (lLocalKeyFrames.size()+sFixedCameras.size())*lLocalMapPoints.size();

    std::vector<pair_id_t> vpEdges;
    vpEdges.reserve(nExpectedSize);

    std::vector<KeyFrame*> vpEdgeKF;
    vpEdgeKF.reserve(nExpectedSize);

    std::vector<MapPoint*> vpMapPointEdge;
    vpMapPointEdge.reserve(nExpectedSize);

    for(std::list<MapPoint*>::iterator lit=lLocalMapPoints.begin(), lend=lLocalMapPoints.end(); lit!=lend; lit++)
    {
        MapPoint* pMP = *lit;

        int id = pMP->mnId+maxKFid+1;
        graph.AddPoint(id, Converter::toVector3d(pMP->GetWorldPos()));

        std::map<KeyFrame*,size_t> observations = pMP->GetObservations();

        //SET EDGES
        for(std::map<KeyFrame*,size_t>::iterator mit=observations.begin(), mend=observations.end(); mit!=mend; mit++)
        {
            KeyFrame* pKFi = mit->first;

            if(!pKFi->isBad())
            {
                Eigen::Matrix<double,2,1> obs;
                cv::KeyPoint kpUn = pKFi->GetKeyPointUn(mit->second);
                obs << kpUn.pt.x, kpUn.pt.y;

                ProjectionConstraint e(id, pKFi->mnId);

                e.SetMeasurement(obs);
                float invSigma = std::sqrt(pKFi->GetInvSigma2(kpUn.octave));
                e.SetWeight(Eigen::Vector2d(invSigma, invSigma));

                e.fx() = pKFi->fx;
                e.fy() = pKFi->fy;
                e.cx() = pKFi->cx;
                e.cy() = pKFi->cy;

                graph.AddProjectionConstraint(id, pKFi->mnId, e);
                vpEdges.push_back(std::make_pair(id, pKFi->mnId));
                vpEdgeKF.push_back(pKFi);
                vpMapPointEdge.push_back(pMP);
            }
        }
    }
    
    // RELATIVE CONSTRAINTS
    // Add fixed base inertial keyframe if needed
    std::vector<INS::InertialFactor>::const_iterator local_cit = std::lower_bound(vImuData.cbegin(), vImuData.cend(), nLocalWindowStart, [](const INS::InertialFactor& lhs, long unsigned int rhs)
      {
        return lhs.src->mnId < rhs;
      });
     
    // assert(inertial_cit != vImuData.cend())
    
    KeyFrame* pKF_inertial_base = local_cit->dst;
        
    // Add base inertial keyframe, if needed
    if (!graph.HasPose(pKF_inertial_base->mnId)) {
        graph.AddPose(pKF_inertial_base->mnId, Converter::toSE3Quat<float>(pKF_inertial_base->GetPose()));
    }
    // Set it as constant
    config.SetConstantPose(pKF_inertial_base->mnId);
    
    std::vector<INS::InertialFactor>::const_iterator inertial_cit = local_cit;
    while (inertial_cit != vImuData.cend() && inertial_cit->src->mnId <= pKF->mnId) {
        const INS::InertialFactor& imu = *inertial_cit;
        id_t id_a = imu.dst->mnId;
        id_t id_b = imu.src->mnId;
        
        PoseConstraint e(id_a, id_b);
        
        e.SetMeasurement(imu.relative_pose);
        e.SetWeight(imu.information_sqrt);
        
        graph.AddPoseConstraint(id_a, id_b, e);
        std::advance(inertial_cit, 1);
    }

    bool optimization_finished = false;
    {
      BundleAdjuster ba(options, config);
      ba.Solve(&graph);
      // If BA has been interrupted, the termination type is: ceres::USE_SUCCESS
      optimization_finished = (ba.Summary().termination_type != ceres::USER_SUCCESS);
    }

    // Check inlier observations
    for(size_t i=0, iend=vpEdges.size(); i<iend;i++)
    {
        pair_id_t pair_id = vpEdges[i];
        ProjectionConstraint& e = graph.GetProjectionConstraint(pair_id);
        MapPoint* pMP = vpMapPointEdge[i];

        if(pMP->isBad())
            continue;

        id_t point_id = e.GetPointId();
        id_t pose_id = e.GetPoseId();
        CHECK(!colmap::IsNaN(e.Chi2())); // Debug
        if(e.Chi2() > 5.991 || !HasPointPositiveDepth(graph.GetPoint(point_id), graph.GetPose(pose_id)))
        {
            KeyFrame* pKFi = vpEdgeKF[i];
            pKFi->EraseMapPointMatch(pMP);
            pMP->EraseObservation(pKFi);

            graph.RemoveProjectionConstraint(pair_id);
        }
    }

    // Recover optimized data

    //Keyframes
    for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin(), lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    {
        KeyFrame* pKF = *lit;
        pKF->SetPose(Converter::toCvMat<float>(graph.GetPose(pKF->mnId)));
    }
    //Points
    for(std::list<MapPoint*>::iterator lit=lLocalMapPoints.begin(), lend=lLocalMapPoints.end(); lit!=lend; lit++)
    {
        MapPoint* pMP = *lit;
        pMP->SetWorldPos(Converter::toCvMat(graph.GetPoint(pMP->mnId+maxKFid+1)));
        pMP->UpdateNormalAndDepth();
    }

    // Optimize again without the outliers

    options.solver_options.max_num_iterations = 25;
    {
      BundleAdjuster ba(options, config);
      ba.Solve(&graph);
    }

    // Check inlier observations
    for(size_t i=0, iend=vpEdges.size(); i<iend;i++)
    {
        pair_id_t pair_id = vpEdges[i];
        if(!graph.HasProjectionConstraint(pair_id))
            continue;

        ProjectionConstraint& e = graph.GetProjectionConstraint(pair_id);

        MapPoint* pMP = vpMapPointEdge[i];

        if(pMP->isBad())
            continue;

        id_t point_id = e.GetPointId();
        id_t pose_id = e.GetPoseId();
        CHECK(!colmap::IsNaN(e.Chi2())); // Debug
        if(e.Chi2()>5.991 || !HasPointPositiveDepth(graph.GetPoint(point_id), graph.GetPose(pose_id)))
        {
            KeyFrame* pKF = vpEdgeKF[i];
            pKF->EraseMapPointMatch(pMP->GetIndexInKeyFrame(pKF));
            pMP->EraseObservation(pKF);
        }
    }

    // Recover optimized data

    //Keyframes
    for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin(), lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    {
        KeyFrame* pKF = *lit;
        pKF->SetPose(Converter::toCvMat<float>(graph.GetPose(pKF->mnId)));
    }

    //Points
    for(std::list<MapPoint*>::iterator lit=lLocalMapPoints.begin(), lend=lLocalMapPoints.end(); lit!=lend; lit++)
    {
        MapPoint* pMP = *lit;
        pMP->SetWorldPos(Converter::toCvMat(graph.GetPoint(pMP->mnId+maxKFid+1)));
        pMP->UpdateNormalAndDepth();
    }

    if (nLocalIdStart) *nLocalIdStart = nLocalWindowStart;
    
    return optimization_finished;
}

/*
                VISUAL ONLY
*/
// DEBUG ONLY
bool Optimizer::LocalBundleAdjustmentVisual(KeyFrame *pKF, bool *pbStopFlag, long unsigned int *nLocalIdStart)
{
    // Local KeyFrames: First Breath Search from Current Keyframe
    std::list<KeyFrame*> lLocalKeyFrames;

    lLocalKeyFrames.push_back(pKF);
    pKF->mnBALocalForKF = pKF->mnId;

    std::vector<KeyFrame*> vNeighKFs = pKF->GetVectorCovisibleKeyFrames();
    for(int i=0, iend=vNeighKFs.size(); i<iend; i++)
    {
        KeyFrame* pKFi = vNeighKFs[i];
        pKFi->mnBALocalForKF = pKF->mnId;
        if(!pKFi->isBad())
            lLocalKeyFrames.push_back(pKFi);
    }
    
    if (lLocalKeyFrames.size() <= 2) return true; // At least 3 are needed, as the first two
    // keyframes are kept fixed to avoid scale gauge freedom
    
    long unsigned int nLocalWindowStart = std::numeric_limits<long unsigned int>::max();
    for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin(), lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    {
        KeyFrame* pKF = *lit;
        if (pKF->mnId < nLocalWindowStart) nLocalWindowStart = pKF->mnId;
    }

    // Local MapPoints seen in Local KeyFrames
    std::list<MapPoint*> lLocalMapPoints;
    for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin(), lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    {
        std::vector<MapPoint*> vpMPs = (*lit)->GetMapPointMatches();
        for(std::vector<MapPoint*>::iterator vit=vpMPs.begin(), vend=vpMPs.end(); vit!=vend; vit++)
        {
            MapPoint* pMP = *vit;
            if(pMP)
                if(!pMP->isBad())
                    if(pMP->mnBALocalForKF!=pKF->mnId)
                    {
                        lLocalMapPoints.push_back(pMP);
                        pMP->mnBALocalForKF=pKF->mnId;
                    }
        }
    }

    // Fixed Keyframes. Keyframes that see Local MapPoints but that are not Local Keyframes
    std::unordered_set<KeyFrame*> sFixedCameras;
    for(std::list<MapPoint*>::iterator lit=lLocalMapPoints.begin(), lend=lLocalMapPoints.end(); lit!=lend; lit++)
    {
        std::map<KeyFrame*,size_t> observations = (*lit)->GetObservations();
        for(std::map<KeyFrame*,size_t>::iterator mit=observations.begin(), mend=observations.end(); mit!=mend; mit++)
        {
            KeyFrame* pKFi = mit->first;

            if(pKFi->mnBALocalForKF!=pKF->mnId && pKFi->mnBAFixedForKF!=pKF->mnId)
            {                
                pKFi->mnBAFixedForKF=pKF->mnId;
                if(!pKFi->isBad())
                    sFixedCameras.insert(pKFi);
            }
        }
    }

    // Setup optimizer
    ConstraintGraph graph;

    BundleAdjustmentOptions options;
    options.projection_loss_type = BundleAdjustmentOptions::LossFunctionType::HUBER;
    options.projection_loss_scale = std::sqrt(5.991);
    options.compute_errors = true;
    options.print_summary = false;
    options.solver_options.max_num_iterations = 10;

    StopFlagCheckerCallback callback(pbStopFlag);
    if (pbStopFlag)
        options.solver_options.callbacks.push_back(&callback);

    BundleAdjustmentConfig config;

    long unsigned int maxKFid = 0;

    // SET LOCAL KEYFRAME VERTICES
    for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin(), lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    {
        KeyFrame* pKFi = *lit;
        graph.AddPose(pKFi->mnId, Converter::toSE3Quat<float>(pKFi->GetPose()));
        if (pKFi->mnId == 0 || pKFi->mnId == 1) // Keep scale!
            config.SetConstantPose(pKFi->mnId);
        if(pKFi->mnId>maxKFid)
            maxKFid=pKFi->mnId;
    }

    // SET FIXED KEYFRAME VERTICES
    for(std::unordered_set<KeyFrame*>::iterator lit=sFixedCameras.begin(), lend=sFixedCameras.end(); lit!=lend; lit++)
    {
        KeyFrame* pKFi = *lit;
        graph.AddPose(pKFi->mnId, Converter::toSE3Quat<float>(pKFi->GetPose()));
        config.SetConstantPose(pKFi->mnId);
        if(pKFi->mnId>maxKFid)
            maxKFid=pKFi->mnId;
    }

    // SET MAP POINT VERTICES
    const int nExpectedSize = (lLocalKeyFrames.size()+sFixedCameras.size())*lLocalMapPoints.size();

    std::vector<pair_id_t> vpEdges;
    vpEdges.reserve(nExpectedSize);

    std::vector<KeyFrame*> vpEdgeKF;
    vpEdgeKF.reserve(nExpectedSize);

    std::vector<MapPoint*> vpMapPointEdge;
    vpMapPointEdge.reserve(nExpectedSize);

    for(std::list<MapPoint*>::iterator lit=lLocalMapPoints.begin(), lend=lLocalMapPoints.end(); lit!=lend; lit++)
    {
        MapPoint* pMP = *lit;

        int id = pMP->mnId+maxKFid+1;
        graph.AddPoint(id, Converter::toVector3d(pMP->GetWorldPos()));

        std::map<KeyFrame*,size_t> observations = pMP->GetObservations();

        //SET EDGES
        for(std::map<KeyFrame*,size_t>::iterator mit=observations.begin(), mend=observations.end(); mit!=mend; mit++)
        {
            KeyFrame* pKFi = mit->first;

            if(!pKFi->isBad())
            {
                Eigen::Matrix<double,2,1> obs;
                cv::KeyPoint kpUn = pKFi->GetKeyPointUn(mit->second);
                obs << kpUn.pt.x, kpUn.pt.y;

                ProjectionConstraint e(id, pKFi->mnId);

                e.SetMeasurement(obs);
                float invSigma = std::sqrt(pKFi->GetInvSigma2(kpUn.octave));
                e.SetWeight(Eigen::Vector2d(invSigma, invSigma));

                e.fx() = pKFi->fx;
                e.fy() = pKFi->fy;
                e.cx() = pKFi->cx;
                e.cy() = pKFi->cy;

                graph.AddProjectionConstraint(id, pKFi->mnId, e);
                vpEdges.push_back(std::make_pair(id, pKFi->mnId));
                vpEdgeKF.push_back(pKFi);
                vpMapPointEdge.push_back(pMP);
            }
        }
    }

    bool optimization_finished = false;
    {
      BundleAdjuster ba(options, config);
      ba.Solve(&graph);
      // If BA has been interrupted, the termination type is: ceres::USE_SUCCESS
      optimization_finished = (ba.Summary().termination_type != ceres::USER_SUCCESS);
    }

    // Check inlier observations
    for(size_t i=0, iend=vpEdges.size(); i<iend;i++)
    {
        pair_id_t pair_id = vpEdges[i];
        ProjectionConstraint& e = graph.GetProjectionConstraint(pair_id);
        MapPoint* pMP = vpMapPointEdge[i];

        if(pMP->isBad())
            continue;

        id_t point_id = e.GetPointId();
        id_t pose_id = e.GetPoseId();
        CHECK(!colmap::IsNaN(e.Chi2())); // Debug
        if(e.Chi2() > 5.991 || !HasPointPositiveDepth(graph.GetPoint(point_id), graph.GetPose(pose_id)))
        {
            KeyFrame* pKFi = vpEdgeKF[i];
            pKFi->EraseMapPointMatch(pMP);
            pMP->EraseObservation(pKFi);

            graph.RemoveProjectionConstraint(pair_id);
        }
    }

    // Recover optimized data

    //Keyframes
    for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin(), lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    {
        KeyFrame* pKF = *lit;
        pKF->SetPose(Converter::toCvMat<float>(graph.GetPose(pKF->mnId)));
    }
    //Points
    for(std::list<MapPoint*>::iterator lit=lLocalMapPoints.begin(), lend=lLocalMapPoints.end(); lit!=lend; lit++)
    {
        MapPoint* pMP = *lit;
        pMP->SetWorldPos(Converter::toCvMat(graph.GetPoint(pMP->mnId+maxKFid+1)));
        pMP->UpdateNormalAndDepth();
    }

    // Optimize again without the outliers

    options.solver_options.max_num_iterations = 15;
    {
      BundleAdjuster ba(options, config);
      ba.Solve(&graph);
    }

    // Check inlier observations
    for(size_t i=0, iend=vpEdges.size(); i<iend;i++)
    {
        pair_id_t pair_id = vpEdges[i];
        if(!graph.HasProjectionConstraint(pair_id))
            continue;

        ProjectionConstraint& e = graph.GetProjectionConstraint(pair_id);

        MapPoint* pMP = vpMapPointEdge[i];

        if(pMP->isBad())
            continue;

        id_t point_id = e.GetPointId();
        id_t pose_id = e.GetPoseId();
        CHECK(!colmap::IsNaN(e.Chi2())); // Debug
        if(e.Chi2()>5.991 || !HasPointPositiveDepth(graph.GetPoint(point_id), graph.GetPose(pose_id)))
        {
            KeyFrame* pKF = vpEdgeKF[i];
            pKF->EraseMapPointMatch(pMP->GetIndexInKeyFrame(pKF));
            pMP->EraseObservation(pKF);
        }
    }

    // Recover optimized data

    //Keyframes
    for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin(), lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    {
        KeyFrame* pKF = *lit;
        pKF->SetPose(Converter::toCvMat<float>(graph.GetPose(pKF->mnId)));
    }

    //Points
    for(std::list<MapPoint*>::iterator lit=lLocalMapPoints.begin(), lend=lLocalMapPoints.end(); lit!=lend; lit++)
    {
        MapPoint* pMP = *lit;
        pMP->SetWorldPos(Converter::toCvMat(graph.GetPoint(pMP->mnId+maxKFid+1)));
        pMP->UpdateNormalAndDepth();
    }

    if (nLocalIdStart) *nLocalIdStart = nLocalWindowStart;
    
    return optimization_finished;
}

/*
                INERTIAL ONLY
*/
/*                  DEBUG                  
bool Optimizer::LocalBundleAdjustmentInertial(KeyFrame *pKF, const std::vector<INS::InertialFactor>& vImuData, bool *pbStopFlag, long unsigned int *nLocalIdStart)
{    
    if (vImuData.empty()) return true; // No data, nothing to do
    
    //std::cout << "pKF->mnId: " << pKF->mnId << std::endl;
    
    // Local KeyFrames: First Breath Search from Current Keyframe
    std::list<KeyFrame*> lLocalKeyFrames;

    lLocalKeyFrames.push_back(pKF);
    pKF->mnBALocalForKF = pKF->mnId;

    std::vector<KeyFrame*> vNeighKFs = pKF->GetVectorCovisibleKeyFrames();
    for(int i=0, iend=vNeighKFs.size(); i<iend; i++)
    {
        KeyFrame* pKFi = vNeighKFs[i];
        pKFi->mnBALocalForKF = pKF->mnId;
        if(!pKFi->isBad())
            lLocalKeyFrames.push_back(pKFi);
    }
    
    long unsigned int nLocalWindowStart = std::numeric_limits<long unsigned int>::max();
    for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin(), lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    {
        KeyFrame* pKF = *lit;
        if (pKF->mnId < nLocalWindowStart) nLocalWindowStart = pKF->mnId;
    }

    // Local MapPoints seen in Local KeyFrames
    //std::list<MapPoint*> lLocalMapPoints;
    //for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin(), lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    //{
        //std::vector<MapPoint*> vpMPs = (*lit)->GetMapPointMatches();
        //for(std::vector<MapPoint*>::iterator vit=vpMPs.begin(), vend=vpMPs.end(); vit!=vend; vit++)
        //{
            //MapPoint* pMP = *vit;
            //if(pMP)
                //if(!pMP->isBad())
                    //if(pMP->mnBALocalForKF!=pKF->mnId)
                    //{
                        //lLocalMapPoints.push_back(pMP);
                        //pMP->mnBALocalForKF=pKF->mnId;
                    //}
        //}
    //}

    // Fixed Keyframes. Keyframes that see Local MapPoints but that are not Local Keyframes
    //std::unordered_set<KeyFrame*> sFixedCameras;
    //for(std::list<MapPoint*>::iterator lit=lLocalMapPoints.begin(), lend=lLocalMapPoints.end(); lit!=lend; lit++)
    //{
        //std::map<KeyFrame*,size_t> observations = (*lit)->GetObservations();
        //for(std::map<KeyFrame*,size_t>::iterator mit=observations.begin(), mend=observations.end(); mit!=mend; mit++)
        //{
            //KeyFrame* pKFi = mit->first;

            //if(pKFi->mnBALocalForKF!=pKF->mnId && pKFi->mnBAFixedForKF!=pKF->mnId)
            //{                
                //pKFi->mnBAFixedForKF=pKF->mnId;
                //if(!pKFi->isBad())
                    //sFixedCameras.insert(pKFi);
            //}
        //}
    //}

    // Setup optimizer
    ConstraintGraph graph;

    BundleAdjustmentOptions options;
    options.projection_loss_type = BundleAdjustmentOptions::LossFunctionType::TRIVIAL;
    //options.projection_loss_type = BundleAdjustmentOptions::LossFunctionType::HUBER;
    //options.projection_loss_scale = std::sqrt(5.991);
    options.compute_errors = false;
    options.print_summary = true;
    options.solver_options.max_num_iterations = 100;

    StopFlagCheckerCallback callback(pbStopFlag);
    if (pbStopFlag)
        options.solver_options.callbacks.push_back(&callback);

    BundleAdjustmentConfig config;

    //long unsigned int maxKFid = 0;
    
    // SET LOCAL KEYFRAME VERTICES
    //for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin(), lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    //{
        //KeyFrame* pKFi = *lit;
        //graph.AddPose(pKFi->mnId, Converter::toSE3Quat<float>(pKFi->GetPoseINS()));
        //if (pKFi->mnId == 0 || pKFi->mnId == 1) // Keep scale!
            //config.SetConstantPose(pKFi->mnId);
        //if(pKFi->mnId>maxKFid)
            //maxKFid=pKFi->mnId;
    //}

    // SET FIXED KEYFRAME VERTICES
    //for(std::unordered_set<KeyFrame*>::iterator lit=sFixedCameras.begin(), lend=sFixedCameras.end(); lit!=lend; lit++)
    //{
        //KeyFrame* pKFi = *lit;
        //graph.AddPose(pKFi->mnId, Converter::toSE3Quat<float>(pKFi->GetPoseINS()));
        //config.SetConstantPose(pKFi->mnId);
        //if(pKFi->mnId>maxKFid)
            //maxKFid=pKFi->mnId;
    //}
    
    // RELATIVE CONSTRAINTS
    
    std::cout << "pKF->mnId: " << pKF->mnId << std::endl;
    std::cout << "nLocalWindowStart: " << nLocalWindowStart << std::endl;

    // Add fixed base inertial keyframe if needed
    std::vector<INS::InertialFactor>::const_iterator local_cit = std::lower_bound(vImuData.cbegin(), vImuData.cend(), nLocalWindowStart, [](const INS::InertialFactor& lhs, long unsigned int rhs)
      {
        return lhs.src->mnId < rhs;
      });
     
    // assert(inertial_cit != vImuData.cend())
    
    KeyFrame* pKF_inertial_base = local_cit->dst;
    std::cout << "Inertial base: " << pKF_inertial_base->mnId << std::endl;
        
    // Add base inertial keyframe, if needed
    if (!graph.HasPose(pKF_inertial_base->mnId)) {
        std::cout << "Adding pose: " << pKF_inertial_base->mnId << std::endl;
        graph.AddPose(pKF_inertial_base->mnId, Converter::toSE3Quat<float>(pKF_inertial_base->GetPoseINS()));
    }
    // Set it as constant
    config.SetConstantPose(pKF_inertial_base->mnId);
    
    std::vector<INS::InertialFactor>::const_iterator inertial_cit = local_cit;
    while (inertial_cit != vImuData.cend() && inertial_cit->src->mnId <= pKF->mnId) {
        const INS::InertialFactor& imu = *inertial_cit;
        id_t id_a = imu.dst->mnId;
        id_t id_b = imu.src->mnId;
        
        std::cout << id_a << " -> " << id_b << std::endl;
        
        graph.AddPose(id_b, Converter::toSE3Quat<float>(imu.src->GetPoseINS()));
        
        PoseConstraint e(id_a, id_b);
        e.SetMeasurement(imu.relative_pose);
        
        Eigen::Vector6d weight;
        weight << 1.0, 1.0, 1.0, 1.0, 1.0, 1.0;
        e.SetWeight(weight);
        
        graph.AddPoseConstraint(id_a, id_b, e);
        std::advance(inertial_cit, 1);
    }

    //bool optimization_finished = false;
    {
      BundleAdjuster ba(options, config);
      ba.Solve(&graph);
      // If BA has been interrupted, the termination type is: ceres::USE_SUCCESS
      //optimization_finished = (ba.Summary().termination_type != ceres::USER_SUCCESS);
    }

    // Recover optimized data

    //Keyframes
    //for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin(), lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    //{
        //KeyFrame* pKF = *lit;
        //pKF->SetRefined(true);
        //pKF->SetPoseINS(Converter::toCvMat<float>(graph.GetPose(pKF->mnId)));
    //}

    //if (nLocalIdStart) *nLocalIdStart = nLocalWindowStart;
    
    //return optimization_finished;
    
    inertial_cit = local_cit;
    while (inertial_cit != vImuData.cend() && inertial_cit->dst->mnId < pKF->mnId) {
        KeyFrame* pKF_ = inertial_cit->dst;
        pKF_->SetRefined(true);
        pKF_->SetPoseINS(Converter::toCvMat<float>(graph.GetPose(pKF_->mnId)));
        
        std::advance(inertial_cit, 1);
    }
    
    pKF->SetRefined(true);
    pKF->SetPoseINS(Converter::toCvMat<float>(graph.GetPose(pKF->mnId)));
    
    if (nLocalIdStart) *nLocalIdStart = nLocalWindowStart;
    
    return true;
}
*/


/* 

                VISUAL-INERTIAL FUSION

bool Optimizer::LocalBundleAdjustment(KeyFrame *pKF, const std::vector<INS::InertialFactor>& vImuData, bool *pbStopFlag, long unsigned int *nLocalIdStart)
{
    // Local KeyFrames: First Breath Search from Current Keyframe
    std::list<KeyFrame*> lLocalKeyFrames;

    lLocalKeyFrames.push_back(pKF);
    pKF->mnBALocalForKF = pKF->mnId;

    std::vector<KeyFrame*> vNeighKFs = pKF->GetVectorCovisibleKeyFrames();
    for(int i=0, iend=vNeighKFs.size(); i<iend; i++)
    {
        KeyFrame* pKFi = vNeighKFs[i];
        pKFi->mnBALocalForKF = pKF->mnId;
        if(!pKFi->isBad())
            lLocalKeyFrames.push_back(pKFi);
    }
    
    long unsigned int nLocalWindowStart = std::numeric_limits<long unsigned int>::max();
    for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin(), lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    {
        KeyFrame* pKF = *lit;
        if (pKF->mnId < nLocalWindowStart) nLocalWindowStart = pKF->mnId;
    }

    // Local MapPoints seen in Local KeyFrames
    std::list<MapPoint*> lLocalMapPoints;
    for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin(), lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    {
        std::vector<MapPoint*> vpMPs = (*lit)->GetMapPointMatches();
        for(std::vector<MapPoint*>::iterator vit=vpMPs.begin(), vend=vpMPs.end(); vit!=vend; vit++)
        {
            MapPoint* pMP = *vit;
            if(pMP)
                if(!pMP->isBad())
                    if(pMP->mnBALocalForKF!=pKF->mnId)
                    {
                        lLocalMapPoints.push_back(pMP);
                        pMP->mnBALocalForKF=pKF->mnId;
                    }
        }
    }

    // Fixed Keyframes. Keyframes that see Local MapPoints but that are not Local Keyframes
    std::unordered_set<KeyFrame*> sFixedCameras;
    for(std::list<MapPoint*>::iterator lit=lLocalMapPoints.begin(), lend=lLocalMapPoints.end(); lit!=lend; lit++)
    {
        std::map<KeyFrame*,size_t> observations = (*lit)->GetObservations();
        for(std::map<KeyFrame*,size_t>::iterator mit=observations.begin(), mend=observations.end(); mit!=mend; mit++)
        {
            KeyFrame* pKFi = mit->first;

            if(pKFi->mnBALocalForKF!=pKF->mnId && pKFi->mnBAFixedForKF!=pKF->mnId)
            {                
                pKFi->mnBAFixedForKF=pKF->mnId;
                if(!pKFi->isBad())
                    sFixedCameras.insert(pKFi);
            }
        }
    }

    // Setup optimizer
    ConstraintGraph graph;

    BundleAdjustmentOptions options;
    options.projection_loss_type = BundleAdjustmentOptions::LossFunctionType::HUBER;
    options.projection_loss_scale = std::sqrt(5.991);
    options.compute_errors = true;
    options.print_summary = false;
    options.solver_options.max_num_iterations = 10;

    StopFlagCheckerCallback callback(pbStopFlag);
    if (pbStopFlag)
        options.solver_options.callbacks.push_back(&callback);

    BundleAdjustmentConfig config;

    long unsigned int maxKFid = 0;

    // SET LOCAL KEYFRAME VERTICES
    for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin(), lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    {
        KeyFrame* pKFi = *lit;
        graph.AddPose(pKFi->mnId, Converter::toSE3Quat<float>(pKFi->GetPose()));
        if (pKFi->mnId == 0 || pKFi->mnId == 1) // Keep scale!
            config.SetConstantPose(pKFi->mnId);
        if(pKFi->mnId>maxKFid)
            maxKFid=pKFi->mnId;
    }

    // SET FIXED KEYFRAME VERTICES
    for(std::unordered_set<KeyFrame*>::iterator lit=sFixedCameras.begin(), lend=sFixedCameras.end(); lit!=lend; lit++)
    {
        KeyFrame* pKFi = *lit;
        graph.AddPose(pKFi->mnId, Converter::toSE3Quat<float>(pKFi->GetPose()));
        config.SetConstantPose(pKFi->mnId);
        if(pKFi->mnId>maxKFid)
            maxKFid=pKFi->mnId;
    }

    // SET MAP POINT VERTICES
    const int nExpectedSize = (lLocalKeyFrames.size()+sFixedCameras.size())*lLocalMapPoints.size();

    std::vector<pair_id_t> vpEdges;
    vpEdges.reserve(nExpectedSize);

    std::vector<KeyFrame*> vpEdgeKF;
    vpEdgeKF.reserve(nExpectedSize);

    std::vector<MapPoint*> vpMapPointEdge;
    vpMapPointEdge.reserve(nExpectedSize);

    for(std::list<MapPoint*>::iterator lit=lLocalMapPoints.begin(), lend=lLocalMapPoints.end(); lit!=lend; lit++)
    {
        MapPoint* pMP = *lit;

        int id = pMP->mnId+maxKFid+1;
        graph.AddPoint(id, Converter::toVector3d(pMP->GetWorldPos()));

        std::map<KeyFrame*,size_t> observations = pMP->GetObservations();

        //SET EDGES
        for(std::map<KeyFrame*,size_t>::iterator mit=observations.begin(), mend=observations.end(); mit!=mend; mit++)
        {
            KeyFrame* pKFi = mit->first;

            if(!pKFi->isBad())
            {
                Eigen::Matrix<double,2,1> obs;
                cv::KeyPoint kpUn = pKFi->GetKeyPointUn(mit->second);
                obs << kpUn.pt.x, kpUn.pt.y;

                ProjectionConstraint e(id, pKFi->mnId);

                e.SetMeasurement(obs);
                float invSigma = std::sqrt(pKFi->GetInvSigma2(kpUn.octave));
                e.SetWeight(Eigen::Vector2d(invSigma, invSigma));

                e.fx() = pKFi->fx;
                e.fy() = pKFi->fy;
                e.cx() = pKFi->cx;
                e.cy() = pKFi->cy;

                graph.AddProjectionConstraint(id, pKFi->mnId, e);
                vpEdges.push_back(std::make_pair(id, pKFi->mnId));
                vpEdgeKF.push_back(pKFi);
                vpMapPointEdge.push_back(pMP);
            }
        }
    }

    // RELATIVE POSE CONSTRAINTS
    std::vector<INS::InertialFactor>::const_iterator cit = std::lower_bound(vImuData.cbegin(), vImuData.cend(), nLocalWindowStart, [](const INS::InertialFactor& lhs, long unsigned int rhs)
        {
            return lhs.src->mnId < rhs;
        });
        
    if (sFixedCameras.find(cit->dst) == sFixedCameras.end()) {
        graph.AddPose(cit->dst->mnId, Converter::toSE3Quat<float>(cit->dst->GetPose()));
        config.SetConstantPose(cit->dst->mnId);
        sFixedCameras.insert(cit->dst);
    }
    
    while (cit != vImuData.cend() && cit->src->mnId < pKF->mnId) {
        const INS::InertialFactor& imu = *cit;
        id_t id_a = imu.dst->mnId;
        id_t id_b = imu.src->mnId;
        
        PoseConstraint e(id_a, id_b);
        e.SetMeasurement(imu.relative_pose);
        
        Eigen::Vector6d weight;
        weight << 1.0, 1.0, 1.0, 1.0, 1.0, 1.0;
        e.SetWeight(weight);
        
        graph.AddPoseConstraint(id_a, id_b, e);
        ++cit;
    }

    bool optimization_finished = false;
    {
      BundleAdjuster ba(options, config);
      ba.Solve(&graph);
      // If BA has been interrupted, the termination type is: ceres::USE_SUCCESS
      optimization_finished = (ba.Summary().termination_type != ceres::USER_SUCCESS);
    }

    // Check inlier observations
    for(size_t i=0, iend=vpEdges.size(); i<iend;i++)
    {
        pair_id_t pair_id = vpEdges[i];
        ProjectionConstraint& e = graph.GetProjectionConstraint(pair_id);
        MapPoint* pMP = vpMapPointEdge[i];

        if(pMP->isBad())
            continue;

        id_t point_id = e.GetPointId();
        id_t pose_id = e.GetPoseId();
        CHECK(!colmap::IsNaN(e.Chi2())); // Debug
        if(e.Chi2() > 5.991 || !HasPointPositiveDepth(graph.GetPoint(point_id), graph.GetPose(pose_id)))
        {
            KeyFrame* pKFi = vpEdgeKF[i];
            pKFi->EraseMapPointMatch(pMP);
            pMP->EraseObservation(pKFi);

            graph.RemoveProjectionConstraint(pair_id);
        }
    }

    // Recover optimized data

    //Keyframes
    for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin(), lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    {
        KeyFrame* pKF = *lit;
        pKF->SetPose(Converter::toCvMat<float>(graph.GetPose(pKF->mnId)));
    }
    //Points
    for(std::list<MapPoint*>::iterator lit=lLocalMapPoints.begin(), lend=lLocalMapPoints.end(); lit!=lend; lit++)
    {
        MapPoint* pMP = *lit;
        pMP->SetWorldPos(Converter::toCvMat(graph.GetPoint(pMP->mnId+maxKFid+1)));
        pMP->UpdateNormalAndDepth();
    }

    // Optimize again without the outliers

    options.solver_options.max_num_iterations = 15;
    {
      BundleAdjuster ba(options, config);
      ba.Solve(&graph);
    }

    // Check inlier observations
    for(size_t i=0, iend=vpEdges.size(); i<iend;i++)
    {
        pair_id_t pair_id = vpEdges[i];
        if(!graph.HasProjectionConstraint(pair_id))
            continue;

        ProjectionConstraint& e = graph.GetProjectionConstraint(pair_id);

        MapPoint* pMP = vpMapPointEdge[i];

        if(pMP->isBad())
            continue;

        id_t point_id = e.GetPointId();
        id_t pose_id = e.GetPoseId();
        CHECK(!colmap::IsNaN(e.Chi2())); // Debug
        if(e.Chi2()>5.991 || !HasPointPositiveDepth(graph.GetPoint(point_id), graph.GetPose(pose_id)))
        {
            KeyFrame* pKF = vpEdgeKF[i];
            pKF->EraseMapPointMatch(pMP->GetIndexInKeyFrame(pKF));
            pMP->EraseObservation(pKF);
        }
    }

    // Recover optimized data

    //Keyframes
    for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin(), lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    {
        KeyFrame* pKF = *lit;
        pKF->SetPose(Converter::toCvMat<float>(graph.GetPose(pKF->mnId)));
    }

    //Points
    for(std::list<MapPoint*>::iterator lit=lLocalMapPoints.begin(), lend=lLocalMapPoints.end(); lit!=lend; lit++)
    {
        MapPoint* pMP = *lit;
        pMP->SetWorldPos(Converter::toCvMat(graph.GetPoint(pMP->mnId+maxKFid+1)));
        pMP->UpdateNormalAndDepth();
    }

    if (nLocalIdStart) *nLocalIdStart = nLocalWindowStart;
    
    return optimization_finished;
}
*/


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                            //
//                                                  OLD STUFF                                                 //
//                                                                                                            //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/*
bool Optimizer::LocalBundleAdjustment(KeyFrame *pKF, bool* pbStopFlag, long unsigned int* nLocalIdStart)
{
    // Local KeyFrames: First Breath Search from Current Keyframe
    std::list<KeyFrame*> lLocalKeyFrames;

    lLocalKeyFrames.push_back(pKF);
    pKF->mnBALocalForKF = pKF->mnId;

    std::vector<KeyFrame*> vNeighKFs = pKF->GetVectorCovisibleKeyFrames();
    for(int i=0, iend=vNeighKFs.size(); i<iend; i++)
    {
        KeyFrame* pKFi = vNeighKFs[i];
        pKFi->mnBALocalForKF = pKF->mnId;
        if(!pKFi->isBad())
            lLocalKeyFrames.push_back(pKFi);
    }

    // Local MapPoints seen in Local KeyFrames
    std::list<MapPoint*> lLocalMapPoints;
    for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin() , lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    {
        std::vector<MapPoint*> vpMPs = (*lit)->GetMapPointMatches();
        for(std::vector<MapPoint*>::iterator vit=vpMPs.begin(), vend=vpMPs.end(); vit!=vend; vit++)
        {
            MapPoint* pMP = *vit;
            if(pMP)
                if(!pMP->isBad())
                    if(pMP->mnBALocalForKF!=pKF->mnId)
                    {
                        lLocalMapPoints.push_back(pMP);
                        pMP->mnBALocalForKF=pKF->mnId;
                    }
        }
    }

    // Fixed Keyframes. Keyframes that see Local MapPoints but that are not Local Keyframes
    std::list<KeyFrame*> lFixedCameras;
    for(std::list<MapPoint*>::iterator lit=lLocalMapPoints.begin(), lend=lLocalMapPoints.end(); lit!=lend; lit++)
    {
        std::map<KeyFrame*,size_t> observations = (*lit)->GetObservations();
        for(std::map<KeyFrame*,size_t>::iterator mit=observations.begin(), mend=observations.end(); mit!=mend; mit++)
        {
            KeyFrame* pKFi = mit->first;

            if(pKFi->mnBALocalForKF!=pKF->mnId && pKFi->mnBAFixedForKF!=pKF->mnId)
            {                
                pKFi->mnBAFixedForKF=pKF->mnId;
                if(!pKFi->isBad())
                    lFixedCameras.push_back(pKFi);
            }
        }
    }

    // Setup optimizer
    ConstraintGraph graph;

    BundleAdjustmentOptions options;
    options.projection_loss_type = BundleAdjustmentOptions::LossFunctionType::HUBER;
    options.projection_loss_scale = std::sqrt(5.991);
    options.compute_errors = true;
    options.print_summary = false;
    options.solver_options.max_num_iterations = 15;

    StopFlagCheckerCallback callback(pbStopFlag);
    if (pbStopFlag)
        options.solver_options.callbacks.push_back(&callback);

    BundleAdjustmentConfig config;

    long unsigned int maxKFid = 0;

    // SET LOCAL KEYFRAME VERTICES
    for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin(), lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    {
        KeyFrame* pKFi = *lit;
        graph.AddPose(pKFi->mnId, Converter::toSE3Quat<float>(pKFi->GetPose()));
        if (pKFi->mnId == 0 || pKFi->mnId == 1) // Keep scale!
            config.SetConstantPose(pKFi->mnId);
        if(pKFi->mnId>maxKFid)
            maxKFid=pKFi->mnId;
    }

    // SET FIXED KEYFRAME VERTICES
    for(std::list<KeyFrame*>::iterator lit=lFixedCameras.begin(), lend=lFixedCameras.end(); lit!=lend; lit++)
    {
        KeyFrame* pKFi = *lit;
        graph.AddPose(pKFi->mnId, Converter::toSE3Quat<float>(pKFi->GetPose()));
        config.SetConstantPose(pKFi->mnId);
        if(pKFi->mnId>maxKFid)
            maxKFid=pKFi->mnId;
    }

    // SET MAP POINT VERTICES
    const int nExpectedSize = (lLocalKeyFrames.size()+lFixedCameras.size())*lLocalMapPoints.size();

    std::vector<pair_id_t> vpEdges;
    vpEdges.reserve(nExpectedSize);

    std::vector<KeyFrame*> vpEdgeKF;
    vpEdgeKF.reserve(nExpectedSize);

    std::vector<MapPoint*> vpMapPointEdge;
    vpMapPointEdge.reserve(nExpectedSize);

    for(std::list<MapPoint*>::iterator lit=lLocalMapPoints.begin(), lend=lLocalMapPoints.end(); lit!=lend; lit++)
    {
        MapPoint* pMP = *lit;

        int id = pMP->mnId+maxKFid+1;
        graph.AddPoint(id, Converter::toVector3d(pMP->GetWorldPos()));

        std::map<KeyFrame*,size_t> observations = pMP->GetObservations();

        //SET EDGES
        for(std::map<KeyFrame*,size_t>::iterator mit=observations.begin(), mend=observations.end(); mit!=mend; mit++)
        {
            KeyFrame* pKFi = mit->first;

            if(!pKFi->isBad())
            {
                Eigen::Matrix<double,2,1> obs;
                cv::KeyPoint kpUn = pKFi->GetKeyPointUn(mit->second);
                obs << kpUn.pt.x, kpUn.pt.y;

                ProjectionConstraint e(id, pKFi->mnId);

                e.SetMeasurement(obs);
                float invSigma = std::sqrt(pKFi->GetInvSigma2(kpUn.octave));
                e.SetWeight(Eigen::Vector2d(invSigma, invSigma));

                e.fx() = pKFi->fx;
                e.fy() = pKFi->fy;
                e.cx() = pKFi->cx;
                e.cy() = pKFi->cy;

                graph.AddProjectionConstraint(id, pKFi->mnId, e);
                vpEdges.push_back(std::make_pair(id, pKFi->mnId));
                vpEdgeKF.push_back(pKFi);
                vpMapPointEdge.push_back(pMP);
            }
        }
    }

    bool optimization_finished = false;
    {
      BundleAdjuster ba(options, config);
      ba.Solve(&graph);
      // If BA has been interrupted, the termination type is: ceres::USE_SUCCESS
      optimization_finished = (ba.Summary().termination_type != ceres::USER_SUCCESS);
    }

    // Check inlier observations
    for(size_t i=0, iend=vpEdges.size(); i<iend;i++)
    {
        pair_id_t pair_id = vpEdges[i];
        ProjectionConstraint& e = graph.GetProjectionConstraint(pair_id);
        MapPoint* pMP = vpMapPointEdge[i];

        if(pMP->isBad())
            continue;

        id_t point_id = e.GetPointId();
        id_t pose_id = e.GetPoseId();
        CHECK(!colmap::IsNaN(e.Chi2())); // Debug
        if(e.Chi2() > 5.991 || !HasPointPositiveDepth(graph.GetPoint(point_id), graph.GetPose(pose_id)))
        {
            KeyFrame* pKFi = vpEdgeKF[i];
            pKFi->EraseMapPointMatch(pMP);
            pMP->EraseObservation(pKFi);

            graph.RemoveProjectionConstraint(pair_id);
        }
    }

    // Recover optimized data

    //Keyframes
    for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin(), lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    {
        KeyFrame* pKF = *lit;
        pKF->SetPose(Converter::toCvMat<float>(graph.GetPose(pKF->mnId)));
    }
    //Points
    for(std::list<MapPoint*>::iterator lit=lLocalMapPoints.begin(), lend=lLocalMapPoints.end(); lit!=lend; lit++)
    {
        MapPoint* pMP = *lit;
        pMP->SetWorldPos(Converter::toCvMat(graph.GetPoint(pMP->mnId+maxKFid+1)));
        pMP->UpdateNormalAndDepth();
    }

    // Optimize again without the outliers

    options.solver_options.max_num_iterations = 10;
    {
      BundleAdjuster ba(options, config);
      ba.Solve(&graph);
    }

    // Check inlier observations
    for(size_t i=0, iend=vpEdges.size(); i<iend;i++)
    {
        pair_id_t pair_id = vpEdges[i];
        if(!graph.HasProjectionConstraint(pair_id))
            continue;

        ProjectionConstraint& e = graph.GetProjectionConstraint(pair_id);

        MapPoint* pMP = vpMapPointEdge[i];

        if(pMP->isBad())
            continue;

        id_t point_id = e.GetPointId();
        id_t pose_id = e.GetPoseId();
        CHECK(!colmap::IsNaN(e.Chi2())); // Debug
        if(e.Chi2()>5.991 || !HasPointPositiveDepth(graph.GetPoint(point_id), graph.GetPose(pose_id)))
        {
            KeyFrame* pKF = vpEdgeKF[i];
            pKF->EraseMapPointMatch(pMP->GetIndexInKeyFrame(pKF));
            pMP->EraseObservation(pKF);
        }
    }

    // Recover optimized data

    //Keyframes
    for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin(), lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    {
        KeyFrame* pKF = *lit;
        pKF->SetPose(Converter::toCvMat<float>(graph.GetPose(pKF->mnId)));
    }

    //Points
    for(std::list<MapPoint*>::iterator lit=lLocalMapPoints.begin(), lend=lLocalMapPoints.end(); lit!=lend; lit++)
    {
        MapPoint* pMP = *lit;
        pMP->SetWorldPos(Converter::toCvMat(graph.GetPoint(pMP->mnId+maxKFid+1)));
        pMP->UpdateNormalAndDepth();
    }

    // Compute minimum KF Id of the local KFs, if required
    if (nLocalIdStart) {
        *nLocalIdStart = std::numeric_limits<long unsigned int>::max();
        for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin(), lend=lLocalKeyFrames.end(); lit!=lend; lit++)
        {
            KeyFrame* pKF = *lit;
            if (pKF->mnId < *nLocalIdStart) *nLocalIdStart = pKF->mnId;
        }
    }
    
    return optimization_finished;
}
*/

/*
void Optimizer::LocalBundleAdjustment(KeyFrame *pKF, const std::map<KeyFrame*, SE3Quat> &mpKFPose, bool* pbStopFlag)
{
    // Local KeyFrames: First Breath Search from Current Keyframe
    std::list<KeyFrame*> lLocalKeyFrames;

    lLocalKeyFrames.push_back(pKF);
    pKF->mnBALocalForKF = pKF->mnId;

    std::vector<KeyFrame*> vNeighKFs = pKF->GetVectorCovisibleKeyFrames();
    for(int i=0, iend=vNeighKFs.size(); i<iend; i++)
    {
        KeyFrame* pKFi = vNeighKFs[i];
        pKFi->mnBALocalForKF = pKF->mnId;
        if(!pKFi->isBad())
            lLocalKeyFrames.push_back(pKFi);
    }

    // Local MapPoints seen in Local KeyFrames
    std::list<MapPoint*> lLocalMapPoints;
    for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin() , lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    {
        std::vector<MapPoint*> vpMPs = (*lit)->GetMapPointMatches();
        for(std::vector<MapPoint*>::iterator vit=vpMPs.begin(), vend=vpMPs.end(); vit!=vend; vit++)
        {
            MapPoint* pMP = *vit;
            if(pMP)
                if(!pMP->isBad())
                    if(pMP->mnBALocalForKF!=pKF->mnId)
                    {
                        lLocalMapPoints.push_back(pMP);
                        pMP->mnBALocalForKF=pKF->mnId;
                    }
        }
    }

    // Fixed Keyframes. Keyframes that see Local MapPoints but that are not Local Keyframes
    std::list<KeyFrame*> lFixedCameras;
    for(std::list<MapPoint*>::iterator lit=lLocalMapPoints.begin(), lend=lLocalMapPoints.end(); lit!=lend; lit++)
    {
        std::map<KeyFrame*,size_t> observations = (*lit)->GetObservations();
        for(std::map<KeyFrame*,size_t>::iterator mit=observations.begin(), mend=observations.end(); mit!=mend; mit++)
        {
            KeyFrame* pKFi = mit->first;

            if(pKFi->mnBALocalForKF!=pKF->mnId && pKFi->mnBAFixedForKF!=pKF->mnId)
            {                
                pKFi->mnBAFixedForKF=pKF->mnId;
                if(!pKFi->isBad())
                    lFixedCameras.push_back(pKFi);
            }
        }
    }

    // Setup optimizer
    ConstraintGraph graph;

    BundleAdjustmentOptions options;
    options.projection_loss_type = BundleAdjustmentOptions::LossFunctionType::HUBER;
    options.projection_loss_scale = std::sqrt(5.991);
    options.compute_errors = true;
    options.print_summary = false;
    options.solver_options.max_num_iterations = 5;

    StopFlagCheckerCallback callback(pbStopFlag);
    if (pbStopFlag)
        options.solver_options.callbacks.push_back(&callback);

    BundleAdjustmentConfig config;

    long unsigned int maxKFid = 0;

    // SET LOCAL KEYFRAME VERTICES
    for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin(), lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    {
        KeyFrame* pKFi = *lit;
        graph.AddPose(pKFi->mnId, Converter::toSE3Quat<float>(pKFi->GetPose()));
        if (pKFi->mnId == 0)
            config.SetConstantPose(pKFi->mnId);
        if(pKFi->mnId>maxKFid)
            maxKFid=pKFi->mnId;
    }

    // SET FIXED KEYFRAME VERTICES
    for(std::list<KeyFrame*>::iterator lit=lFixedCameras.begin(), lend=lFixedCameras.end(); lit!=lend; lit++)
    {
        KeyFrame* pKFi = *lit;
        graph.AddPose(pKFi->mnId, Converter::toSE3Quat<float>(pKFi->GetPose()));
        config.SetConstantPose(pKFi->mnId);
        if(pKFi->mnId>maxKFid)
            maxKFid=pKFi->mnId;
    }

    // SET MAP POINT VERTICES
    const int nExpectedSize = (lLocalKeyFrames.size()+lFixedCameras.size())*lLocalMapPoints.size();

    std::vector<pair_id_t> vpEdges;
    vpEdges.reserve(nExpectedSize);

    std::vector<KeyFrame*> vpEdgeKF;
    vpEdgeKF.reserve(nExpectedSize);

    std::vector<MapPoint*> vpMapPointEdge;
    vpMapPointEdge.reserve(nExpectedSize);

    for(std::list<MapPoint*>::iterator lit=lLocalMapPoints.begin(), lend=lLocalMapPoints.end(); lit!=lend; lit++)
    {
        MapPoint* pMP = *lit;

        int id = pMP->mnId+maxKFid+1;
        graph.AddPoint(id, Converter::toVector3d(pMP->GetWorldPos()));

        std::map<KeyFrame*,size_t> observations = pMP->GetObservations();

        //SET EDGES
        for(std::map<KeyFrame*,size_t>::iterator mit=observations.begin(), mend=observations.end(); mit!=mend; mit++)
        {
            KeyFrame* pKFi = mit->first;

            if(!pKFi->isBad())
            {
                Eigen::Matrix<double,2,1> obs;
                cv::KeyPoint kpUn = pKFi->GetKeyPointUn(mit->second);
                obs << kpUn.pt.x, kpUn.pt.y;

                ProjectionConstraint e(id, pKFi->mnId);

                e.SetMeasurement(obs);
                float invSigma = std::sqrt(pKFi->GetInvSigma2(kpUn.octave));
                e.SetWeight(Eigen::Vector2d(invSigma, invSigma));

                e.fx() = pKFi->fx;
                e.fy() = pKFi->fy;
                e.cx() = pKFi->cx;
                e.cy() = pKFi->cy;

                graph.AddProjectionConstraint(id, pKFi->mnId, e);
                vpEdges.push_back(std::make_pair(id, pKFi->mnId));
                vpEdgeKF.push_back(pKFi);
                vpMapPointEdge.push_back(pMP);
            }
        }
    }

    {
      BundleAdjuster ba(options, config);
      ba.Solve(&graph);
    }

    // Check inlier observations
    for(size_t i=0, iend=vpEdges.size(); i<iend;i++)
    {
        pair_id_t pair_id = vpEdges[i];
        ProjectionConstraint& e = graph.GetProjectionConstraint(pair_id);
        MapPoint* pMP = vpMapPointEdge[i];

        if(pMP->isBad())
            continue;

        id_t point_id = e.GetPointId();
        id_t pose_id = e.GetPoseId();
        CHECK(!colmap::IsNaN(e.Chi2())); // Debug
        if(e.Chi2() > 5.991 || !HasPointPositiveDepth(graph.GetPoint(point_id), graph.GetPose(pose_id)))
        {
            KeyFrame* pKFi = vpEdgeKF[i];
            pKFi->EraseMapPointMatch(pMP);
            pMP->EraseObservation(pKFi);

            graph.RemoveProjectionConstraint(pair_id);
        }
    }

    // Recover optimized data

    //Keyframes
    for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin(), lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    {
        KeyFrame* pKF = *lit;
        pKF->SetPose(Converter::toCvMat<float>(graph.GetPose(pKF->mnId)));
    }
    //Points
    for(std::list<MapPoint*>::iterator lit=lLocalMapPoints.begin(), lend=lLocalMapPoints.end(); lit!=lend; lit++)
    {
        MapPoint* pMP = *lit;
        pMP->SetWorldPos(Converter::toCvMat(graph.GetPoint(pMP->mnId+maxKFid+1)));
        pMP->UpdateNormalAndDepth();
    }

    // Optimize again without the outliers

    options.solver_options.max_num_iterations = 10;
    {
      BundleAdjuster ba(options, config);
      ba.Solve(&graph);
    }

    // Check inlier observations
    for(size_t i=0, iend=vpEdges.size(); i<iend;i++)
    {
        pair_id_t pair_id = vpEdges[i];
        if(!graph.HasProjectionConstraint(pair_id))
            continue;

        ProjectionConstraint& e = graph.GetProjectionConstraint(pair_id);

        MapPoint* pMP = vpMapPointEdge[i];

        if(pMP->isBad())
            continue;

        id_t point_id = e.GetPointId();
        id_t pose_id = e.GetPoseId();
        CHECK(!colmap::IsNaN(e.Chi2())); // Debug
        if(e.Chi2()>5.991 || !HasPointPositiveDepth(graph.GetPoint(point_id), graph.GetPose(pose_id)))
        {
            KeyFrame* pKF = vpEdgeKF[i];
            pKF->EraseMapPointMatch(pMP->GetIndexInKeyFrame(pKF));
            pMP->EraseObservation(pKF);
        }
    }

    // Recover optimized data

    //Keyframes
    for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin(), lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    {
        KeyFrame* pKF = *lit;
        pKF->SetPose(Converter::toCvMat<float>(graph.GetPose(pKF->mnId)));
    }

    //Points
    for(std::list<MapPoint*>::iterator lit=lLocalMapPoints.begin(), lend=lLocalMapPoints.end(); lit!=lend; lit++)
    {
        MapPoint* pMP = *lit;
        pMP->SetWorldPos(Converter::toCvMat(graph.GetPoint(pMP->mnId+maxKFid+1)));
        pMP->UpdateNormalAndDepth();
    }

    //Refinement(pKF, mpKFPose, nullptr);
}

void Optimizer::Refinement(KeyFrame *pKF, const std::map<KeyFrame*, SE3Quat> &mpKFPose, bool* pbStopFlag)
{
    // Local KeyFrames: First Breath Search from Current Keyframe
    std::list<KeyFrame*> lLocalKeyFrames;

    lLocalKeyFrames.push_back(pKF);
    pKF->mnBALocalForKF = pKF->mnId;

    std::vector<KeyFrame*> vNeighKFs = pKF->GetVectorCovisibleKeyFrames();
    for(int i=0, iend=vNeighKFs.size(); i<iend; i++)
    {
        KeyFrame* pKFi = vNeighKFs[i];
        pKFi->mnBALocalForKF = pKF->mnId;
        if(!pKFi->isBad())
            lLocalKeyFrames.push_back(pKFi);
    }

    // Local MapPoints seen in Local KeyFrames
    std::list<MapPoint*> lLocalMapPoints;
    for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin() , lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    {
        std::vector<MapPoint*> vpMPs = (*lit)->GetMapPointMatches();
        for(std::vector<MapPoint*>::iterator vit=vpMPs.begin(), vend=vpMPs.end(); vit!=vend; vit++)
        {
            MapPoint* pMP = *vit;
            if(pMP)
                if(!pMP->isBad())
                    if(pMP->mnBALocalForKF!=pKF->mnId)
                    {
                        lLocalMapPoints.push_back(pMP);
                        pMP->mnBALocalForKF=pKF->mnId;
                    }
        }
    }

    // Fixed Keyframes. Keyframes that see Local MapPoints but that are not Local Keyframes
    std::list<KeyFrame*> lFixedCameras;
    for(std::list<MapPoint*>::iterator lit=lLocalMapPoints.begin(), lend=lLocalMapPoints.end(); lit!=lend; lit++)
    {
        std::map<KeyFrame*,size_t> observations = (*lit)->GetObservations();
        for(std::map<KeyFrame*,size_t>::iterator mit=observations.begin(), mend=observations.end(); mit!=mend; mit++)
        {
            KeyFrame* pKFi = mit->first;

            if(pKFi->mnBALocalForKF!=pKF->mnId && pKFi->mnBAFixedForKF!=pKF->mnId)
            {                
                pKFi->mnBAFixedForKF=pKF->mnId;
                if(!pKFi->isBad())
                    lFixedCameras.push_back(pKFi);
            }
        }
    }

    std::set<long unsigned int> sFixedIds;
    std::vector<long unsigned int> vLocalIds;
    std::map<long unsigned int, KeyFrame*> mIdKF;
    for (KeyFrame* pKF : lFixedCameras) {
        sFixedIds.insert(pKF->mnId);
        vLocalIds.push_back(pKF->mnId);
        mIdKF[pKF->mnId] = pKF;
    }
    
    for (KeyFrame* pKF : lLocalKeyFrames) {
        if (pKF->mnId == 0) sFixedIds.insert(pKF->mnId);
        vLocalIds.push_back(pKF->mnId);
        mIdKF[pKF->mnId] = pKF;
    }

    std::sort(vLocalIds.begin(), vLocalIds.end());

    // Setup optimizer
    ConstraintGraph graph;

    BundleAdjustmentOptions options;
    options.compute_errors = false;
    options.print_summary = true;
    options.solver_options.max_num_iterations = 50;
    options.solver_options.minimizer_progress_to_stdout = true;

    StopFlagCheckerCallback callback(pbStopFlag);
    if (pbStopFlag)
        options.solver_options.callbacks.push_back(&callback);

    BundleAdjustmentConfig config;

    long unsigned int maxKFid = 0;

    // SET LOCAL KEYFRAME VERTICES
    for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin(), lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    {
        KeyFrame* pKFi = *lit;
        graph.AddPose(pKFi->mnId, Converter::toSE3Quat<double>(pKFi->GetPoseINS()));
        if (pKFi->mnId == 0) {
            config.SetConstantPose(pKFi->mnId);
            pKFi->SetRefined(true);
        }
        if(pKFi->mnId>maxKFid)
            maxKFid=pKFi->mnId;
    }

    // SET FIXED KEYFRAME VERTICES
    for(std::list<KeyFrame*>::iterator lit=lFixedCameras.begin(), lend=lFixedCameras.end(); lit!=lend; lit++)
    {
        KeyFrame* pKFi = *lit;
        graph.AddPose(pKFi->mnId, Converter::toSE3Quat<double>(pKFi->GetPoseINS()));
        config.SetConstantPose(pKFi->mnId);
        if(pKFi->mnId>maxKFid)
            maxKFid=pKFi->mnId;
    }

    // SET RELATIVE POSE CONSTRAINTS
    std::cout << "Fixed poses: " << sFixedIds.size() << std::endl;

    // Set fixed ids as roots
    std::vector<long unsigned int> vConnectedIds;
    for (long unsigned int nId : vLocalIds) {
        if (sFixedIds.find(nId) == sFixedIds.cend()) continue;

        KeyFrame* pKF = mIdKF.at(nId);
        if (mpKFPose.find(pKF) == mpKFPose.cend()) continue;

        vConnectedIds.push_back(nId);
    }

    if (vConnectedIds.empty()) {
        std::cout << "Setting latest keyfram fixed" << std::endl;
        long unsigned int nId = vLocalIds.front();
        config.SetConstantPose(nId);
        sFixedIds.insert(nId);
        vConnectedIds.push_back(nId);
    }
    std::cout << "Root poses: " << vConnectedIds.size() << std::endl;

    for (long unsigned int nId : vConnectedIds) {
        std::cout << nId << " ";
    }
    std::cout << std::endl;

    // Add connections
    unsigned int nConstraints = 0;
    for (long unsigned int nId : vLocalIds) {
        if (sFixedIds.find(nId) != sFixedIds.cend()) continue;

        KeyFrame* pKFb = mIdKF.at(nId);

        std::map<KeyFrame*, SE3Quat>::const_iterator mit = mpKFPose.find(pKFb);
        if (mit == mpKFPose.cend()) continue;

        std::vector<long unsigned int>::iterator it = std::lower_bound(vConnectedIds.begin(), vConnectedIds.end(), nId);
        if (it == vConnectedIds.begin()) {
            std::cout << "Varibale KFs should appear after fixed KFs!" << std::endl;
            continue;
        }

        long unsigned int nLastId = *std::prev(it);
        KeyFrame* pKFa = mIdKF.at(nLastId);

        std::map<KeyFrame*, SE3Quat>::const_iterator mita = mpKFPose.find(pKFa);
        if (mita == mpKFPose.cend()) continue;

        std::cout << nLastId << " -> " << nId << std::endl;

        if (nId == pKF->mnId) {
            // Initialize current KF
            SE3Quat lastPose = graph.GetPose(nLastId);
            graph.GetPose(nId) = lastPose;
        }

        SE3Quat pose_a = mita->second;
        SE3Quat pose_b = mit->second;

        SE3Quat p_ba = pose_b * pose_a.inverse();
        Eigen::Vector4d qba_vec(p_ba.q[0], p_ba.q[1], p_ba.q[2], p_ba.q[3]);
        Eigen::Vector3d tba_vec(p_ba.t[0], p_ba.t[1], p_ba.t[2]);

        PoseConstraint constraint(nLastId, nId);

        SE3Quat relative_pose(tba_vec, qba_vec);
        Eigen::Vector6d weight;
        weight << 1.0, 1.0, 1.0, 1.0, 1.0, 1.0;

        constraint.SetMeasurement(relative_pose);
        constraint.SetWeight(weight);

        graph.AddPoseConstraint(nLastId, nId, constraint);
        ++nConstraints;

        vConnectedIds.insert(it, nId);

        pKFb->SetRefined(true);
    }

    std::cout << "Relative constraints: " << nConstraints << std::endl;

    // Perform optimization
    if (nConstraints > 0) {
        BundleAdjuster ba(options, config);
        ba.Solve(&graph);
    }

    // Recover optimized data

    //Keyframes
    //for(std::list<KeyFrame*>::iterator lit=lLocalKeyFrames.begin(), lend=lLocalKeyFrames.end(); lit!=lend; lit++)
    //{
    //    KeyFrame* pKF = *lit;
    //    pKF->SetPoseINS(Converter::toCvMatd(graph.GetPose(pKF->mnId)));
    //}

    for (long unsigned int nId : vLocalIds) {
        KeyFrame* pKF = mIdKF.at(nId);
        if (!pKF->GetRefined()) continue;
        pKF->SetPoseINS(Converter::toCvMat<double>(graph.GetPose(pKF->mnId)));
    }
}
*/

/*
void Optimizer::OptimizeEssentialGraph(Map* pMap, KeyFrame* pLoopKF, KeyFrame* pCurKF, g2o::Sim3 &Scurw,
                                       LoopClosing::KeyFrameAndPose &NonCorrectedSim3,
                                       LoopClosing::KeyFrameAndPose &CorrectedSim3,
                                       map<KeyFrame *, set<KeyFrame *> > &LoopConnections)
{
    // Setup optimizer
    g2o::SparseOptimizer optimizer;
    optimizer.setVerbose(false);
    g2o::BlockSolver_7_3::LinearSolverType * linearSolver =
           new g2o::LinearSolverEigen<g2o::BlockSolver_7_3::PoseMatrixType>();
    g2o::BlockSolver_7_3 * solver_ptr= new g2o::BlockSolver_7_3(linearSolver);
    g2o::OptimizationAlgorithmLevenberg* solver = new g2o::OptimizationAlgorithmLevenberg(solver_ptr);

    solver->setUserLambdaInit(1e-16);
    optimizer.setAlgorithm(solver);

    vector<KeyFrame*> vpKFs = pMap->GetAllKeyFrames();
    vector<MapPoint*> vpMPs = pMap->GetAllMapPoints();

    unsigned int nMaxKFid = pMap->GetMaxKFid();


    vector<g2o::Sim3,Eigen::aligned_allocator<g2o::Sim3> > vScw(nMaxKFid+1);
    vector<g2o::Sim3,Eigen::aligned_allocator<g2o::Sim3> > vCorrectedSwc(nMaxKFid+1);
    vector<g2o::VertexSim3Expmap*> vpVertices(nMaxKFid+1);

    const int minFeat = 100;

    // SET KEYFRAME VERTICES
    for(size_t i=0, iend=vpKFs.size(); i<iend;i++)
    {
        KeyFrame* pKF = vpKFs[i];
        if(pKF->isBad())
            continue;
        g2o::VertexSim3Expmap* VSim3 = new g2o::VertexSim3Expmap();

        int nIDi = pKF->mnId;      

        if(CorrectedSim3.count(pKF))
        {
            vScw[nIDi] = CorrectedSim3[pKF];
            VSim3->setEstimate(CorrectedSim3[pKF]);
        }
        else
        {
            Eigen::Matrix<double,3,3> Rcw = Converter::toMatrix3d(pKF->GetRotation());
            Eigen::Matrix<double,3,1> tcw = Converter::toVector3d(pKF->GetTranslation());
            g2o::Sim3 Siw(Rcw,tcw,1.0);
            vScw[nIDi] = Siw;
            VSim3->setEstimate(Siw);
        }

        if(pKF==pLoopKF)
            VSim3->setFixed(true);

        VSim3->setId(nIDi);
        VSim3->setMarginalized(false);

        optimizer.addVertex(VSim3);

        vpVertices[nIDi]=VSim3;
    }


    set<pair<long unsigned int,long unsigned int> > sInsertedEdges;

    Eigen::Matrix<double,7,7> matLambda = Eigen::Matrix<double,7,7>::Identity();

    // SET LOOP EDGES
    for(map<KeyFrame *, set<KeyFrame *> >::iterator mit = LoopConnections.begin(), mend=LoopConnections.end(); mit!=mend; mit++)
    {
        KeyFrame* pKF = mit->first;
        const long unsigned int nIDi = pKF->mnId;
        set<KeyFrame*> &spConnections = mit->second;
        g2o::Sim3 Siw = vScw[nIDi];
        g2o::Sim3 Swi = Siw.inverse();

        for(set<KeyFrame*>::iterator sit=spConnections.begin(), send=spConnections.end(); sit!=send; sit++)
        {
            const long unsigned int nIDj = (*sit)->mnId;
            if((nIDi!=pCurKF->mnId || nIDj!=pLoopKF->mnId) && pKF->GetWeight(*sit)<minFeat)
                continue;

            g2o::Sim3 Sjw = vScw[nIDj];
            g2o::Sim3 Sji = Sjw * Swi;

            g2o::EdgeSim3* e = new g2o::EdgeSim3();
            e->setVertex(1, dynamic_cast<g2o::OptimizableGraph::Vertex*>(optimizer.vertex(nIDj)));
            e->setVertex(0, dynamic_cast<g2o::OptimizableGraph::Vertex*>(optimizer.vertex(nIDi)));
            e->setMeasurement(Sji);

            e->information() = matLambda;

            optimizer.addEdge(e);

            sInsertedEdges.insert(make_pair(min(nIDi,nIDj),max(nIDi,nIDj)));
        }
    }

    // SET NORMAL EDGES
    for(size_t i=0, iend=vpKFs.size(); i<iend; i++)
    {
        KeyFrame* pKF = vpKFs[i];

        const int nIDi = pKF->mnId;

        g2o::Sim3 Swi;
        if(NonCorrectedSim3.count(pKF))
            Swi = NonCorrectedSim3[pKF].inverse();
        else
            Swi = vScw[nIDi].inverse();

        KeyFrame* pParentKF = pKF->GetParent();

        // Spanning tree edge
        if(pParentKF)
        {
            int nIDj = pParentKF->mnId;

            g2o::Sim3 Sjw;

            if(NonCorrectedSim3.count(pParentKF))
                Sjw = NonCorrectedSim3[pParentKF];
            else
                Sjw = vScw[nIDj];

            g2o::Sim3 Sji = Sjw * Swi;

            g2o::EdgeSim3* e = new g2o::EdgeSim3();
            e->setVertex(1, dynamic_cast<g2o::OptimizableGraph::Vertex*>(optimizer.vertex(nIDj)));
            e->setVertex(0, dynamic_cast<g2o::OptimizableGraph::Vertex*>(optimizer.vertex(nIDi)));
            e->setMeasurement(Sji);

            e->information() = matLambda;
            optimizer.addEdge(e);
        }

        // Loop edges
        set<KeyFrame*> sLoopEdges = pKF->GetLoopEdges();
        for(set<KeyFrame*>::iterator sit=sLoopEdges.begin(), send=sLoopEdges.end(); sit!=send; sit++)
        {
            KeyFrame* pLKF = *sit;
            if(pLKF->mnId<pKF->mnId)
            {
                g2o::Sim3 Slw;
                if(NonCorrectedSim3.count(pLKF))
                    Slw = NonCorrectedSim3[pLKF];
                else
                    Slw = vScw[pLKF->mnId];

                g2o::Sim3 Sli = Slw * Swi;
                g2o::EdgeSim3* el = new g2o::EdgeSim3();
                el->setVertex(1, dynamic_cast<g2o::OptimizableGraph::Vertex*>(optimizer.vertex(pLKF->mnId)));
                el->setVertex(0, dynamic_cast<g2o::OptimizableGraph::Vertex*>(optimizer.vertex(nIDi)));
                el->setMeasurement(Sli);
                el->information() = matLambda;
                optimizer.addEdge(el);
            }
        }

        // Covisibility graph edges
        vector<KeyFrame*> vpConnectedKFs = pKF->GetCovisiblesByWeight(minFeat);
        for(vector<KeyFrame*>::iterator vit=vpConnectedKFs.begin(); vit!=vpConnectedKFs.end(); vit++)
        {
            KeyFrame* pKFn = *vit;
            if(pKFn && pKFn!=pParentKF && !pKF->hasChild(pKFn) && !sLoopEdges.count(pKFn))
            {
                if(!pKFn->isBad() && pKFn->mnId<pKF->mnId)
                {
                    if(sInsertedEdges.count(make_pair(min(pKF->mnId,pKFn->mnId),max(pKF->mnId,pKFn->mnId))))
                        continue;

                    g2o::Sim3 Snw;
                    if(NonCorrectedSim3.count(pKFn))
                        Snw = NonCorrectedSim3[pKFn];
                    else
                        Snw = vScw[pKFn->mnId];

                    g2o::Sim3 Sni = Snw * Swi;

                    g2o::EdgeSim3* en = new g2o::EdgeSim3();
                    en->setVertex(1, dynamic_cast<g2o::OptimizableGraph::Vertex*>(optimizer.vertex(pKFn->mnId)));
                    en->setVertex(0, dynamic_cast<g2o::OptimizableGraph::Vertex*>(optimizer.vertex(nIDi)));
                    en->setMeasurement(Sni);
                    en->information() = matLambda;
                    optimizer.addEdge(en);
                }
            }
        }
    }

    // OPTIMIZE

    optimizer.initializeOptimization();
    optimizer.optimize(20);

    // SE3 Pose Recovering. Sim3:[sR t;0 1] -> SE3:[R t/s;0 1]
    for(size_t i=0;i<vpKFs.size();i++)
    {
        KeyFrame* pKFi = vpKFs[i];

        const int nIDi = pKFi->mnId;

        g2o::VertexSim3Expmap* VSim3 = static_cast<g2o::VertexSim3Expmap*>(optimizer.vertex(nIDi));
        g2o::Sim3 CorrectedSiw =  VSim3->estimate();
        vCorrectedSwc[nIDi]=CorrectedSiw.inverse();
        Eigen::Matrix3d eigR = CorrectedSiw.rotation().toRotationMatrix();
        Eigen::Vector3d eigt = CorrectedSiw.translation();
        double s = CorrectedSiw.scale();

        eigt *=(1./s); //[R t/s;0 1]

        cv::Mat Tiw = Converter::toCvSE3(eigR,eigt);

        pKFi->SetPose(Tiw);
    }

    // Correct points. Transform to "non-optimized" reference keyframe pose and transform back with optimized pose
    for(size_t i=0, iend=vpMPs.size(); i<iend; i++)
    {
        MapPoint* pMP = vpMPs[i];

        if(pMP->isBad())
            continue;

        int nIDr;
        if(pMP->mnCorrectedByKF==pCurKF->mnId)
        {
            nIDr = pMP->mnCorrectedReference;
        }
        else
        {
            KeyFrame* pRefKF = pMP->GetReferenceKeyFrame();
            nIDr = pRefKF->mnId;
        }


        g2o::Sim3 Srw = vScw[nIDr];
        g2o::Sim3 correctedSwr = vCorrectedSwc[nIDr];

        cv::Mat P3Dw = pMP->GetWorldPos();
        Eigen::Matrix<double,3,1> eigP3Dw = Converter::toVector3d(P3Dw);
        Eigen::Matrix<double,3,1> eigCorrectedP3Dw = correctedSwr.map(Srw.map(eigP3Dw));

        cv::Mat cvCorrectedP3Dw = Converter::toCvMat(eigCorrectedP3Dw);
        pMP->SetWorldPos(cvCorrectedP3Dw);

        pMP->UpdateNormalAndDepth();
    }
}

int Optimizer::OptimizeSim3(KeyFrame *pKF1, KeyFrame *pKF2, vector<MapPoint *> &vpMatches1, g2o::Sim3 &g2oS12, float th2)
{
    g2o::SparseOptimizer optimizer;
    g2o::BlockSolverX::LinearSolverType * linearSolver;

    linearSolver = new g2o::LinearSolverDense<g2o::BlockSolverX::PoseMatrixType>();

    g2o::BlockSolverX * solver_ptr = new g2o::BlockSolverX(linearSolver);

    g2o::OptimizationAlgorithmLevenberg* solver = new g2o::OptimizationAlgorithmLevenberg(solver_ptr);
    optimizer.setAlgorithm(solver);

    // Calibration
    cv::Mat K1 = pKF1->GetCalibrationMatrix();
    cv::Mat K2 = pKF2->GetCalibrationMatrix();

    // Camera poses
    cv::Mat R1w = pKF1->GetRotation();
    cv::Mat t1w = pKF1->GetTranslation();
    cv::Mat R2w = pKF2->GetRotation();
    cv::Mat t2w = pKF2->GetTranslation();

    // SET SIMILARITY VERTEX
    g2o::VertexSim3Expmap * vSim3 = new g2o::VertexSim3Expmap();
    vSim3->setEstimate(g2oS12);
    vSim3->setId(0);
    vSim3->setFixed(false);
    vSim3->_principle_point1[0] = K1.at<float>(0,2);
    vSim3->_principle_point1[1] = K1.at<float>(1,2);
    vSim3->_focal_length1[0] = K1.at<float>(0,0);
    vSim3->_focal_length1[1] = K1.at<float>(1,1);
    vSim3->_principle_point2[0] = K2.at<float>(0,2);
    vSim3->_principle_point2[1] = K2.at<float>(1,2);
    vSim3->_focal_length2[0] = K2.at<float>(0,0);
    vSim3->_focal_length2[1] = K2.at<float>(1,1);
    optimizer.addVertex(vSim3);

    // SET MAP POINT VERTICES
    const int N = vpMatches1.size();
    vector<MapPoint*> vpMapPoints1 = pKF1->GetMapPointMatches();
    vector<g2o::EdgeSim3ProjectXYZ*> vpEdges12;
    vector<g2o::EdgeInverseSim3ProjectXYZ*> vpEdges21;
    vector<float> vSigmas12, vSigmas21;
    vector<size_t> vnIndexEdge;

    vnIndexEdge.reserve(2*N);
    vpEdges12.reserve(2*N);
    vpEdges21.reserve(2*N);

    const float deltaHuber = sqrt(th2);

    int nCorrespondences = 0;

    for(int i=0; i<N; i++)
    {
        if(!vpMatches1[i])
            continue;

        MapPoint* pMP1 = vpMapPoints1[i];
        MapPoint* pMP2 = vpMatches1[i];

        int id1 = 2*i+1;
        int id2 = 2*(i+1);

        int i2 = pMP2->GetIndexInKeyFrame(pKF2);

        if(pMP1 && pMP2)
        {
            if(!pMP1->isBad() && !pMP2->isBad() && i2>=0)
            {
                g2o::VertexSBAPointXYZ* vPoint1 = new g2o::VertexSBAPointXYZ();
                cv::Mat P3D1w = pMP1->GetWorldPos();
                cv::Mat P3D1c = R1w*P3D1w + t1w;
                vPoint1->setEstimate(Converter::toVector3d(P3D1c));
                vPoint1->setId(id1);
                vPoint1->setFixed(true);
                optimizer.addVertex(vPoint1);

                g2o::VertexSBAPointXYZ* vPoint2 = new g2o::VertexSBAPointXYZ();
                cv::Mat P3D2w = pMP2->GetWorldPos();
                cv::Mat P3D2c = R2w*P3D2w + t2w;
                vPoint2->setEstimate(Converter::toVector3d(P3D2c));
                vPoint2->setId(id2);
                vPoint2->setFixed(true);
                optimizer.addVertex(vPoint2);
            }
            else
                continue;
        }
        else
            continue;

        nCorrespondences++;

        // SET EDGE x1 = S12*X2
        Eigen::Matrix<double,2,1> obs1;
        cv::KeyPoint kpUn1 = pKF1->GetKeyPointUn(i);
        obs1 << kpUn1.pt.x, kpUn1.pt.y;

        g2o::EdgeSim3ProjectXYZ* e12 = new g2o::EdgeSim3ProjectXYZ();
        e12->setVertex(0, dynamic_cast<g2o::OptimizableGraph::Vertex*>(optimizer.vertex(id2)));
        e12->setVertex(1, dynamic_cast<g2o::OptimizableGraph::Vertex*>(optimizer.vertex(0)));
        e12->setMeasurement(obs1);
        float invSigmaSquare1 = pKF1->GetInvSigma2(kpUn1.octave);
        e12->setInformation(Eigen::Matrix2d::Identity()*invSigmaSquare1);

        g2o::RobustKernelHuber* rk1 = new g2o::RobustKernelHuber;
        e12->setRobustKernel(rk1);
        rk1->setDelta(deltaHuber);
        optimizer.addEdge(e12);

        // SET EDGE x2 = S21*X1
        Eigen::Matrix<double,2,1> obs2;
        cv::KeyPoint kpUn2 = pKF2->GetKeyPointUn(i2);
        obs2 << kpUn2.pt.x, kpUn2.pt.y;

        g2o::EdgeInverseSim3ProjectXYZ* e21 = new g2o::EdgeInverseSim3ProjectXYZ();

        e21->setVertex(0, dynamic_cast<g2o::OptimizableGraph::Vertex*>(optimizer.vertex(id1)));
        e21->setVertex(1, dynamic_cast<g2o::OptimizableGraph::Vertex*>(optimizer.vertex(0)));
        e21->setMeasurement(obs2);
        float invSigmaSquare2 = pKF2->GetSigma2(kpUn2.octave);
        e21->setInformation(Eigen::Matrix2d::Identity()*invSigmaSquare2);

        g2o::RobustKernelHuber* rk2 = new g2o::RobustKernelHuber;
        e21->setRobustKernel(rk2);
        rk2->setDelta(deltaHuber);
        optimizer.addEdge(e21);

        vpEdges12.push_back(e12);
        vpEdges21.push_back(e21);
        vnIndexEdge.push_back(i);
    }

    // Optimize

    optimizer.initializeOptimization();
    optimizer.optimize(5);

    // Check inliers
    int nBad=0;
    for(size_t i=0; i<vpEdges12.size();i++)
    {
        g2o::EdgeSim3ProjectXYZ* e12 = vpEdges12[i];
        g2o::EdgeInverseSim3ProjectXYZ* e21 = vpEdges21[i];
        if(!e12 || !e21)
            continue;

        if(e12->chi2()>th2 || e21->chi2()>th2)
        {
            size_t idx = vnIndexEdge[i];
            vpMatches1[idx]=NULL;
            optimizer.removeEdge(e12);
            optimizer.removeEdge(e21);
            vpEdges12[i]=NULL;
            vpEdges21[i]=NULL;
            nBad++;
        }
    }

    int nMoreIterations;
    if(nBad>0)
        nMoreIterations=10;
    else
        nMoreIterations=5;

    if(nCorrespondences-nBad<10)
        return 0;

    // Optimize again only with inliers

    optimizer.initializeOptimization();
    optimizer.optimize(nMoreIterations);

    int nIn = 0;
    for(size_t i=0; i<vpEdges12.size();i++)
    {
        g2o::EdgeSim3ProjectXYZ* e12 = vpEdges12[i];
        g2o::EdgeInverseSim3ProjectXYZ* e21 = vpEdges21[i];
        if(!e12 || !e21)
            continue;

        if(e12->chi2()>th2 || e21->chi2()>th2)
        {
            size_t idx = vnIndexEdge[i];
            vpMatches1[idx]=NULL;
        }
        else
            nIn++;
    }

    // Recover optimized Sim3
    g2o::VertexSim3Expmap* vSim3_recov = static_cast<g2o::VertexSim3Expmap*>(optimizer.vertex(0));
    g2oS12= vSim3_recov->estimate();

    return nIn;
}
*/

} //namespace ORB_SLAM
