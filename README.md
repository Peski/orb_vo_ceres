
# ORB-VO Monocular

ORB-VO is a simplified version of ORB-SLAM: a versatile and accurate Monocular SLAM solution able to compute in real-time the camera trajectory and a sparse 3D reconstruction of the scene in a wide variety of environments, ranging from small hand-held sequences to a car driven around several city blocks. It is able to close large loops and perform global relocalisation in real-time and from wide baselines.

See the original project webpage: http://webdiis.unizar.es/~raulmur/orbslam/

###Related Publications:

[1] Raúl Mur-Artal, J. M. M. Montiel and Juan D. Tardós. **ORB-SLAM: A Versatile and Accurate Monocular SLAM System**. *IEEE Transactions on Robotics,* vol. 31, no. 5, pp. 1147-1163, 2015. (2015 IEEE Transactions on Robotics **Best Paper Award**). **[PDF](http://webdiis.unizar.es/~raulmur/MurMontielTardosTRO15.pdf)**.

[2] Dorian Gálvez-López and Juan D. Tardós. **Bags of Binary Words for Fast Place Recognition in Image Sequences**. *IEEE Transactions on Robotics,* vol. 28, no. 5, pp.  1188-1197, 2012. **[PDF](http://doriangalvez.com/php/dl.php?dlp=GalvezTRO12.pdf)**.

#1. License

ORB-VO is released under a [GPLv3 license](License-gpl.txt). For a list of all code/library dependencies (and associated licenses), please see [Dependencies.md](https://github.com/raulmur/ORB_SLAM/blob/master/Dependencies.md).

If you use ORB-SLAM in an academic work, please cite:

    @article{murAcceptedTRO2015,
      title={{ORB-SLAM}: a Versatile and Accurate Monocular {SLAM} System},
      author={Mur-Artal, Ra\'ul, Montiel, J. M. M. and Tard\'os, Juan D.},
      journal={IEEE Transactions on Robotics},
      volume={31},
      number={5},
      pages={1147--1163},
      doi = {10.1109/TRO.2015.2463671},
      year={2015}
     }


#2. Prerequisites (dependencies)

##2.1 Boost

We use the Boost library to launch the different threads of our SLAM system.

	sudo apt-get install libboost-all-dev 

##2.2 ROS
We use ROS to receive images from the camera or from a recorded sequence (rosbag), and for visualization (rviz, image_view). 
**We have tested ORB-SLAM in Ubuntu 16.04 with ROS Kinetic**. 
If you do not have already installed ROS in your computer, we recommend you to install the Full-Desktop version of ROS Kinetic (http://wiki.ros.org/kinetic/Installation/Ubuntu).

##2.3 OpenCV
We use OpenCV (3.0.0 and above) to manipulate images and features. OpenCV is already included in the ROS distribution. **We tested OpenCV 3.3.1-dev**. Dowload and install instructions can be found at: http://opencv.org/

##2.4 ceres-solver
We use cere-solver (1.14.0) to perform optimizations. To install it, please see http://ceres-solver.org/

##2.4 DBoW2 (included in Thirdparty)
We make use of some components of the DBoW2 and DLib library (see original at https://github.com/dorian3d/DBoW2) for place recognition and feature matching. There are no additional dependencies to compile DBoW2.

##2.5 COLMAP (included in Thirdparty)
We make use of some components of the COLMAP library (see original at https://github.com/colmap/colmap) for the bundle adjustment.

#3. Installation

1. Make sure you have installed ROS and all library dependencies (boost, opencv, eigen3).

2. Clone the repository:

		git clone https://github.com/raulmur/ORB_SLAM.git ORB_SLAM

3. Build ORB_SLAM (and thirdparty dependencies) using the provided script. In the ORB_SLAM root execute:

		bash build.sh

#4. Usage

We provide a bash script file to easily get ORB-VO running:

1. Extract the `ORBvoc.txt` vocabulary file from `Data/ORBvoc.txt.tar.gz`.

2. Provide the intrinsic camera parameters in the `Data/Settings.yaml` configuration file.

3. Run ORB-VO:

		bash run.sh

#5. The Settings File

ORB_VO reads the camera calibration and setting parameters from a YAML file. We provide an example in `Data/Settings.yaml`, where you will find all parameters and their description. We use the camera calibration model of OpenCV.

Please make sure you write and call your own settings file for your camera (copy the example file and modify the calibration)

#6. Failure Modes

You should expect to achieve good results in sequences similar to those in which we show results in our paper [1], in terms of camera movement and texture in the environment. In general our Monocular SLAM solution is expected to have a bad time in the following situations:
- No translation at system initialization (or too much rotation).
- Pure rotations in exploration.
- Low texture environments.
- Many (or big) moving objects, especially if they move slowly.

The system is able to initialize from planar and non-planar scenes. In the case of planar scenes, depending on the camera movement relative to the plane, it is possible that the system refuses to initialize, see the paper [1] for details. 

