You should have received this colmap version along with ORB-SLAM (https://github.com/raulmur/ORB_SLAM).
See the original colmap library at: https://github.com/colmap/colmap/
All files included in this colmap version are BSD, see license-bsd.txt
